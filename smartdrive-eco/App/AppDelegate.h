//
//  AppDelegate.h
//  smartdrive-eco
//
//  Created by Koon~ on 3/25/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import <CoreData/CoreData.h>
#import "./ViewModel/CoreVM.h"
#import "./ViewModel/CoreVM.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

/* Properties */
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CoreVM *coreVM;

/* Object data model */
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end

