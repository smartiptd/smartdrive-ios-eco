//
//  AppDelegate.m
//  smartdrive-eco
//
//  Created by Koon~ on 3/25/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "AppDelegate.h"

static NSString * const STORYBOARD_IPAD = @"iPadMainStoryboard";
static NSString * const STORYBOARD_IPHONE = @"iPhoneMainStoryboard";


@implementation AppDelegate

#pragma mark - AppDelegate
- (BOOL)application:(UIApplication *)application
willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return YES;
    }
}

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    @try {
        self.coreVM = [CoreVM sharedInstance];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            // iPad
            if ([self.coreVM.sessionMgr getCurrentSession]) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:STORYBOARD_IPAD bundle:nil];
                self.window.rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            } else {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:STORYBOARD_IPAD bundle:nil];
                self.window.rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoginMethodVC"];
            }
        } else {
            // iPhone
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return YES;
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    BOOL isOpenURL = YES;
    
    @try {
        isOpenURL = [[GIDSignIn sharedInstance] handleURL:url
                                        sourceApplication:sourceApplication
                                               annotation:annotation];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return isOpenURL;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    @try {
        [self saveContext];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    
    UIInterfaceOrientationMask orientationMask;
    
    @try {
        /* Set support interface orientation */
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            // iPad
            orientationMask = UIInterfaceOrientationMaskLandscape;
        } else {
            // iPhone
            orientationMask = UIInterfaceOrientationMaskPortrait;
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return orientationMask;
    }
}

#pragma mark - Core Data stack
@synthesize managedObjectContext        = _managedObjectContext;
@synthesize managedObjectModel          = _managedObjectModel;
@synthesize persistentStoreCoordinator  = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    
    @try {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "th.co.scg.cbm.smartdrive_eco" in the application's documents directory.
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                       inDomains:NSUserDomainMask] lastObject];
    }
}

- (NSManagedObjectModel *)managedObjectModel {
    
    @try {
        // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
        if (_managedObjectModel != nil) {
            return _managedObjectModel;
        }
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"smartdrive_eco" withExtension:@"momd"];
        _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return _managedObjectModel;
    }
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    @try {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
        if (_persistentStoreCoordinator != nil) {
            return _persistentStoreCoordinator;
        }
        
        // Create the coordinator and store
        
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"smartdrive_eco.sqlite"];
        NSError *error = nil;
        NSString *failureReason = @"There was an error creating or loading the application's saved data.";
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                       configuration:nil
                                                                 URL:storeURL
                                                             options:nil
                                                               error:&error]) {
            // Report any error we got.
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
            dict[NSLocalizedFailureReasonErrorKey] = failureReason;
            dict[NSUnderlyingErrorKey] = error;
            error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return _persistentStoreCoordinator;
    }
}


- (NSManagedObjectContext *)managedObjectContext {
    
    @try {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
        if (_managedObjectContext != nil) {
            return _managedObjectContext;
        }
        
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if (!coordinator) {
            return nil;
        }
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return _managedObjectContext;
    }
}

#pragma mark - Core Data Saving support
- (void)saveContext {
    
    @try {
        NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
        if (managedObjectContext != nil) {
            
            NSError *error = nil;
            if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
