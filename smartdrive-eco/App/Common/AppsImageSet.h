//
//  AppsImageSet.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/28/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface AppsImageSet : NSObject

/* Image set */
+ (NSArray *)IPAD_WIND_IMAGE_SET;
+ (NSArray *)IPAD_HEAT_IMAGE_SET;
+ (NSArray *)IPAD_CV_IMAGE_SET;
+ (NSArray *)IPAD_POWER_SOURCE_IMAGE_SET;
+ (NSArray *)IPAD_POWER_SOURCE_BAR_IMAGE_SET;
+ (NSArray *)IPAD_FAN_IMAGE_SET;
+ (NSArray *)IPAD_MENU_SLIDE_SIGN_IMAGE_SET;
+ (NSArray *)IPAD_FAN_STATUS_IMAGE_SET;

@end
