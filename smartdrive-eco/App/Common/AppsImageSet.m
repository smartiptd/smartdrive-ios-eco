//
//  AppsImageSet.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/28/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "AppsImageSet.h"

@implementation AppsImageSet

#pragma - Animation set
+ (NSArray *)IPAD_WIND_IMAGE_SET {
    
    return [NSArray arrayWithObjects:[UIImage imageNamed:@"ipad_an_wind_0"],
            [UIImage imageNamed:@"ipad_an_wind_1"],
            [UIImage imageNamed:@"ipad_an_wind_2"],
            [UIImage imageNamed:@"ipad_an_wind_3"],
            [UIImage imageNamed:@"ipad_an_wind_4"],
            [UIImage imageNamed:@"ipad_an_wind_5"],
            [UIImage imageNamed:@"ipad_an_wind_6"], nil];
}

+ (NSArray *)IPAD_HEAT_IMAGE_SET {
    
    return [NSArray arrayWithObjects:[UIImage imageNamed:@"ipad_an_heat_0"],
            [UIImage imageNamed:@"ipad_an_heat_1"],
            [UIImage imageNamed:@"ipad_an_heat_2"],
            [UIImage imageNamed:@"ipad_an_heat_3"],
            [UIImage imageNamed:@"ipad_an_heat_4"],
            [UIImage imageNamed:@"ipad_an_heat_5"],
            [UIImage imageNamed:@"ipad_an_heat_6"], nil];
}

+ (NSArray *)IPAD_CV_IMAGE_SET {
    
    return [NSArray arrayWithObjects:[UIImage imageNamed:@"ipad_an_ceiling_0"],
            [UIImage imageNamed:@"ipad_an_ceiling_1"],
            [UIImage imageNamed:@"ipad_an_ceiling_2"],
            [UIImage imageNamed:@"ipad_an_ceiling_3"],
            [UIImage imageNamed:@"ipad_an_ceiling_4"],
            [UIImage imageNamed:@"ipad_an_ceiling_5"],
            [UIImage imageNamed:@"ipad_an_ceiling_6"], nil];
}

+ (NSArray *)IPAD_POWER_SOURCE_IMAGE_SET {
    
    return [NSArray arrayWithObjects:[UIImage imageNamed:@"ipad_source_power_source_grid"],
            [UIImage imageNamed:@"ipad_source_power_source_solar"],
            [UIImage imageNamed:@"ipad_source_power_source_grid"], nil];
}

+ (NSArray *)IPAD_POWER_SOURCE_BAR_IMAGE_SET {
    
    return [NSArray arrayWithObjects:[UIImage imageNamed:@"ipad_source_power_source_bar_1"],
            [UIImage imageNamed:@"ipad_source_power_source_bar_1"],
            [UIImage imageNamed:@"ipad_source_power_source_bar_2"],
            [UIImage imageNamed:@"ipad_source_power_source_bar_3"],
            [UIImage imageNamed:@"ipad_source_power_source_bar_4"],
            [UIImage imageNamed:@"ipad_source_power_source_bar_5"], nil];
}

+ (NSArray *)IPAD_FAN_IMAGE_SET {
    
    return [NSArray arrayWithObjects:[UIImage imageNamed:@"ipad_an_fan_1"],
            [UIImage imageNamed:@"ipad_an_fan_2"],
            [UIImage imageNamed:@"ipad_an_fan_3"],
            [UIImage imageNamed:@"ipad_an_fan_4"],
            [UIImage imageNamed:@"ipad_an_fan_5"],
            [UIImage imageNamed:@"ipad_an_fan_6"],
            [UIImage imageNamed:@"ipad_an_fan_7"],
            [UIImage imageNamed:@"ipad_an_fan_8"],
            [UIImage imageNamed:@"ipad_an_fan_9"],
            [UIImage imageNamed:@"ipad_an_fan_10"],
            [UIImage imageNamed:@"ipad_an_fan_11"],
            [UIImage imageNamed:@"ipad_an_fan_12"],
            [UIImage imageNamed:@"ipad_an_fan_13"],
            [UIImage imageNamed:@"ipad_an_fan_14"],
            [UIImage imageNamed:@"ipad_an_fan_15"], nil];
}

+ (NSArray *)IPAD_MENU_SLIDE_SIGN_IMAGE_SET {
    
    return [NSArray arrayWithObjects:[UIImage imageNamed:@"ipad_source_button_menu_close"],
            [UIImage imageNamed:@"ipad_source_button_menu_open"], nil];
}

+ (NSArray *)IPAD_FAN_STATUS_IMAGE_SET {
    
    return [NSArray arrayWithObjects:[UIImage imageNamed:@"ipad_source_button_status_off"],
            [UIImage imageNamed:@"ipad_source_button_status_on"],
            [UIImage imageNamed:@"ipad_source_button_status_auto"], nil];
}

@end
