//
//  Common.h
//  smartdrive-eco
//
//  Created by Koon~ on 5/4/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "./Constant.h"

@interface Common : NSObject

/* Common method */
+ (NSInteger)randomNumberBetween:(NSInteger)min maxNumber:(NSInteger)max;
+ (BOOL) isNumeric:(NSString *) str;
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (BOOL)validateEmailWithString:(NSString*)email;
+ (UIViewController *)getTopController;
+ (NSDictionary *)mqttRegexWithPattern:(NSString *)pattern
                                 Topic:(NSString *)topic;

@end
