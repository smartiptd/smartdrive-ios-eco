//
//  Common.m
//  smartdrive-eco
//
//  Created by Koon~ on 5/4/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "Common.h"

@implementation Common

+ (NSInteger)randomNumberBetween:(NSInteger)min maxNumber:(NSInteger)max {
    
    NSInteger randNum;
    
    @try {
        randNum = min + arc4random_uniform((unsigned int)max - (unsigned int)min + 1);
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return randNum;
    }
}

+ (BOOL) isNumeric:(NSString *) str {
    
    BOOL isNumeric = NO;
    
    @try {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        NSNumber *number = [formatter numberFromString:str];
        
        isNumeric = !!number;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return isNumeric;
    }
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    
    UIImage *image;
    
    @try {
        CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSetFillColorWithColor(context, [color CGColor]);
        CGContextFillRect(context, rect);
        
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        return image;
    }
}

+ (BOOL)validateEmailWithString:(NSString*)email {
    
    BOOL result;
    
    @try {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *evaluator = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        result = [evaluator evaluateWithObject:email];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return result;
    }
}

+ (UIViewController *)getTopController {
    
    UIViewController *topController;
    
    @try {
        topController = [UIApplication sharedApplication].keyWindow.rootViewController;
        while (topController.presentedViewController) {
            
            topController = topController.presentedViewController;
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return topController;
    }
}

+ (NSDictionary *)mqttRegexWithPattern:(NSString *)pattern
                                 Topic:(NSString *)topic {
    
    NSMutableDictionary *tmpDic = [[NSMutableDictionary alloc] init];
    
    @try {
        NSArray *separatedPattern = [pattern componentsSeparatedByString:@"/"];
        NSArray *separatedTopic = [topic componentsSeparatedByString:@"/"];
        
        if (separatedPattern && separatedTopic && ([separatedPattern count] <= [separatedTopic count])) {
            
            for (NSInteger index = 0; index < [separatedPattern count]; index++) {
                
                NSString *key = [separatedPattern objectAtIndex:index];
                if (key && ![key isEqualToString:BLANK]) {
                    
                    NSString *value = BLANK;
                    if ([key hasPrefix:@"+"]) {
                        
                        key = [key stringByReplacingOccurrencesOfString:@"+" withString:BLANK];
                        value = [separatedTopic objectAtIndex:index];
                        [tmpDic setObject:value forKey:key];
                        
                    } else if ([key hasPrefix:@"#"]) {
                        
                        key = [key stringByReplacingOccurrencesOfString:@"#" withString:BLANK];
                        for (NSInteger i = index; i < [separatedTopic count]; i++) {
                            value = [value stringByAppendingString:[NSString stringWithFormat:@"/%@", [separatedTopic objectAtIndex:i]]];
                        }
                        [tmpDic setObject:value forKey:key];
                        
                    } else {
                        
                        value = [separatedTopic objectAtIndex:index];
                        if (![key isEqualToString:value]) tmpDic = nil;
                        break;
                    }
                }
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return [NSDictionary dictionaryWithDictionary:tmpDic];
    }
}

@end
