//
//  CommunicationManager.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/27/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MQTTClient.h"
#import "./Common-Util.h"

@protocol CommunicationManagerDelegate

@optional
- (void)connectionDidUpdate:(MQTTSession *)conn;
- (void)connectionDidReceiveMessage:(NSDictionary *)messageDic
                           SubsysID:(NSString *)subsysID
                          Operation:(NSString *)operation
                               Path:(NSString *)path;

@end

@interface CommunicationManager : NSObject <MQTTSessionDelegate>

{ id delegate; }
@property (strong, nonatomic) MQTTSession *conn;

- (void)setDelegate:(id)aDelegate;
- (void)createMqttConnectionWithClientID:(NSString *)clientID
                                Username:(NSString *)username
                                Password:(NSString *)password;
- (void)subscribeToTopic:(NSString *)topic;
- (void)publishWithTopic:(NSString *)topic
                 Payload:(NSData *)payload;
- (void)destroyMqttConnection;

@end
