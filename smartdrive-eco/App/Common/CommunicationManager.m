//
//  CommunicationManager.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/27/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "CommunicationManager.h"

@implementation CommunicationManager

- (void)setDelegate:(id)aDelegate {
    
    @try {
        delegate = aDelegate;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)createMqttConnectionWithClientID:(NSString *)clientID
                                Username:(NSString *)username
                                Password:(NSString *)password {
    
    @try {
        /* Destroy old session */
        [self destroyMqttConnection];
        
        /* Prepare SSL communication */
        NSString *certificate = [[NSBundle bundleForClass:[MQTTSession class]] pathForResource:MQTT_CERTIFICATE ofType:@"cer"];
        if (certificate) {
            
            /* Initial MQTT connection */
            self.conn = [[MQTTSession alloc] initWithClientId:clientID
                                                     userName:username
                                                     password:password
                                                    keepAlive:10
                                                 cleanSession:NO
                                                         will:NO
                                                    willTopic:nil
                                                      willMsg:nil
                                                      willQoS:MQTTQosLevelAtMostOnce
                                               willRetainFlag:NO
                                                protocolLevel:4
                                                      runLoop:nil
                                                      forMode:nil];
            self.conn.delegate = self;
            
            self.conn.securityPolicy = [MQTTSSLSecurityPolicy policyWithPinningMode:MQTTSSLPinningModePublicKey];
            self.conn.securityPolicy.pinnedCertificates = @[[NSData dataWithContentsOfFile:certificate]];
            self.conn.securityPolicy.allowInvalidCertificates = YES;
            
            /* Connect to MQTT broker */
            [self.conn connectToHost:MQTT_HOST port:MQTT_PORT usingSSL:YES];
            
            [delegate connectionDidUpdate:self.conn];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)subscribeToTopic:(NSString *)topic {
    
    @try {
        if (self.conn) {
            
            [self.conn subscribeToTopic:topic atLevel:2 subscribeHandler:^(NSError *error, NSArray<NSNumber *> *qos){
                if (error) {
                    NSLog(@"[MQTT] Status: SUBSCRIPTION ERROR = %@", error.localizedDescription);
                } else {
                    NSLog(@"[MQTT] Status: SUBSCRIPTION SUCCESSFULL (%@)", topic);
                }
            }];
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)publishWithTopic:(NSString *)topic
                 Payload:(NSData *)payload {
    
    @try {
        if (self.conn) {
            [self.conn publishAndWaitData:payload
                                  onTopic:topic
                                   retain:NO
                                      qos:MQTTQosLevelAtLeastOnce];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)destroyMqttConnection {
    
    @try {
        if (self.conn) {
            
            [self.conn close];
            self.conn = nil;
            
            [delegate connectionDidUpdate:self.conn];
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - MQTTSession Delegate
- (void)connected:(MQTTSession *)session {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)connectionClosed:(MQTTSession *)session {
    
    @try {
        self.conn = nil;
        [delegate connectionDidUpdate:self.conn];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)connected:(MQTTSession *)session sessionPresent:(BOOL)sessionPresent {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)connectionRefused:(MQTTSession *)session error:(NSError *)error {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)connectionError:(MQTTSession *)session
                  error:(NSError *)error {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)protocolError:(MQTTSession *)session error:(NSError *)error {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)handleEvent:(MQTTSession *)session
              event:(MQTTSessionEvent)eventCode
              error:(NSError *)error {
    
    @try {
        switch (eventCode) {
            case MQTTSessionEventConnected:
                NSLog(@"[MQTT] Status: CONNECTED");
                break;
            case MQTTSessionEventConnectionClosed:
                NSLog(@"[MQTT] Status: CONNECTION CLOSED");
                break;
            case MQTTSessionEventConnectionRefused:
                NSLog(@"[MQTT] Status: CONNECTION REFUSED");
                break;
            case MQTTSessionEventConnectionError:
                NSLog(@"[MQTT] Status: CONNECTION ERROR = %@", error);
                break;
            case MQTTSessionEventProtocolError:
                NSLog(@"[MQTT] Status: PROTOCOL ERROR = %@", error);
                break;
            case MQTTSessionEventConnectionClosedByBroker:
                NSLog(@"[MQTT] Status: CLOSED BY BROKER");
                break;
            default:
                break;
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)newMessage:(MQTTSession *)session
              data:(NSData *)data
           onTopic:(NSString *)topic
               qos:(MQTTQosLevel)qos
          retained:(BOOL)retained
               mid:(unsigned int)mid {
    
    @try {
        NSError *jsonError;
        NSDictionary *payload = [NSJSONSerialization JSONObjectWithData:data
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&jsonError];
        
        if (payload && !jsonError) {
            
            NSDictionary *regexDic = [Common mqttRegexWithPattern:@"+subsysID/+operation/#path" Topic:topic];
            if (regexDic) {
                
                NSString *subsysID = [regexDic objectForKey:@"subsysID"];
                NSString *operation = [regexDic objectForKey:@"operation"];
                NSString *path = [regexDic objectForKey:@"path"];
                
                if (subsysID && operation && path) {
                    
                    [delegate connectionDidReceiveMessage:payload
                                                 SubsysID:subsysID
                                                Operation:operation
                                                     Path:path];
                }
            }
        }

    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
