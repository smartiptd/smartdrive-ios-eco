//
//  Constant.h
//  smartdrive-eco
//
//  Created by Koon~ on 4/28/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject

/**
 * Authentication 
 */
extern NSString * const OAUTH2_DOMAIN;
extern NSString * const OAUTH2_SIGNUP;
extern NSString * const OAUTH2_TOKEN_URL;
extern NSString * const OAUTH2_CLIENT_ID;
extern NSString * const OAUTH2_APP_SCOPE;
extern NSString * const OAUTH2_CLIENT_SECRET;
extern NSString * const OAUTH2_CERTIFICATE;
extern NSString * const OAUTH2_API_USERINFO;
extern NSString * const OAUTH2_API_SUBSCRIBE;
extern NSString * const OAUTH2_API_SUBSCRIPTIONINFO;

/**
 * Communication 
 */
extern UInt32     const MQTT_PORT;
extern NSString * const MQTT_HOST;
extern NSString * const MQTT_CERTIFICATE;

/**
 * Font
 */
extern NSString * const FONT_HELVETICA_REGULAR;
extern NSString * const FONT_HELVETICA_LIGHT;
extern NSString * const FONT_HELVETICA_OBLIQUE;
extern NSString * const FONT_HELVETICA57_CONDENSED;
extern NSString * const FONT_HELVETICA_NEUE_THIN;
extern NSString * const FONT_HELVETICA_NEUE_LIGHT;
extern NSString * const FONT_DB_HELVETICA_BD_COND;
extern NSString * const FONT_DB_HELVETICA_LI_COND;
extern NSString * const FONT_DB_HELVETICA_COND_IT;

/**
 * Graphic User Interface (GUI)
 */
extern NSString * const GUI_LOGO;
extern NSString * const GUI_LOGIN_LOGO;

/**
 * Common constant
 */
extern NSString * const BLANK;

/**
 * Entity (Data Model)
 */
extern NSString * const ENTITY_SUBSCRIPTION;

/**
 * Subsys
 */
extern NSString * const SUBSYS_AAF;

/**
 * Active AIRflow Enum
 */
typedef NS_ENUM(NSInteger, AAF_HOUSE_STATUS) {
    AAF_HOUSE_STATUS_OFF,
    AAF_HOUSE_STATUS_ON,
    AAF_HOUSE_STATUS_HOT,
    AAF_HOUSE_STATUS_COOL
};

@end
