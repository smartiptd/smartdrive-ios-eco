//
//  Constant.m
//  smartdrive-eco
//
//  Created by Koon~ on 4/28/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "Constant.h"

@implementation Constant

/**
 * Authentication 
 */
NSString * const OAUTH2_DOMAIN                  = @"https://183.90.171.97";
NSString * const OAUTH2_SIGNUP                  = @"/signup";
NSString * const OAUTH2_TOKEN_URL               = @"/api/oauth2/token";
NSString * const OAUTH2_CLIENT_ID               = @"577b33b9d515c7633896fa3b";
NSString * const OAUTH2_CLIENT_SECRET           = @"Vq5y7setp108";
NSString * const OAUTH2_APP_SCOPE               = @"user_info,mqtt_conn,mqtt_pub,mqtt_sub,subscription_info,subscribe_subsys,share_subscription";
NSString * const OAUTH2_CERTIFICATE             = @"smartdrive-directorybased-service";
NSString * const OAUTH2_API_USERINFO            = @"/api/user/info";
NSString * const OAUTH2_API_SUBSCRIBE           = @"/api/subscription/subscribe?access_token=";
NSString * const OAUTH2_API_SUBSCRIPTIONINFO    = @"/api/subscription/info";

/**
 * Communication 
 */
UInt32     const MQTT_PORT                      = 8883;
NSString * const MQTT_HOST                      = @"183.90.171.97";
NSString * const MQTT_CERTIFICATE               = @"smartdrive-mqtt-service";

/**
 * Font
 */
NSString * const  FONT_HELVETICA_REGULAR        = @"Helvetica-Normal";
NSString * const  FONT_HELVETICA_LIGHT          = @"Helvetica-Light";
NSString * const  FONT_HELVETICA_OBLIQUE        = @"Helvetica-Oblique";
NSString * const  FONT_HELVETICA57_CONDENSED    = @"Helvetica57-Condensed";
NSString * const  FONT_HELVETICA_NEUE_THIN      = @"HelveticaNeue-Thin";
NSString * const  FONT_HELVETICA_NEUE_LIGHT     = @"HelveticaNeue-Light";
NSString * const  FONT_DB_HELVETICA_BD_COND     = @"DBHelvethaicaX-77BdCond";
NSString * const  FONT_DB_HELVETICA_LI_COND     = @"DBHelvethaicaX-47LiCond";
NSString * const  FONT_DB_HELVETICA_COND_IT     = @"DBHelvethaicaX-58CondIt";

/**
 * Graphic User Interface (GUI)
 */
NSString * const GUI_LOGO                       = @"logo";
NSString * const GUI_LOGIN_LOGO                 = @"logo_login";

/**
 * Common constant
 */
NSString * const BLANK                          = @"";

/**
 * Entity (Data Model)
 */
NSString * const ENTITY_SUBSCRIPTION            = @"Subscription";

/**
 * Subsys
 */
NSString * const SUBSYS_AAF                     = @"aaf";

@end
