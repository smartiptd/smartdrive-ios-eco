//
//  SessionManager.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/7/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "AFOAuth2Manager.h"
#import "../DataModel/AppSession.h"
#import "../DataModel/AppCredential.h"
#import "../DataModel/AppUserInfo.h"
#import "../DataModel/Subscription.h"

typedef enum {
    REQUEST_ERROR,
    REQUEST_SUCCESS
} RequestStatus;

typedef enum {
    NETWORK_NONE,
    NETWORK_PROCESS
} NetworkStatus;

@protocol SessionManagerDelegate

@optional
- (void)sessionDidUpdate:(AppSession *)session;
- (void)subscriptionDidUpdate:(NSArray *)subscriptionList;
- (void)requestStatusDidUpdate:(RequestStatus)status;
- (void)networkStatusDidUpdate:(NetworkStatus)status;

@end

@interface SessionManager : NSObject

{ id delegate; }
@property (nonatomic, strong) AppSession *session;

- (void)setDelegate:(id)newDelegate;
- ( AppSession *)getCurrentSession;
- (void)requestSessionWithEmail:( NSString *)email
                       Password:(NSString *)password;
- (void)requestSessionWithGoogleEmail:(NSString *)email
                              TokenID:(NSString *)tokenID;
- (void)requestSubscriptionWithSubsysID:(NSString *)subsysID;
- (void)requestSubscriptionInfo;
- (void)destroySession;

@end
