//
//  SessionManager.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/7/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "SessionManager.h"

@implementation SessionManager

- (instancetype)init {
    
    @try {
        self = [super init];
        if (self) {
            self.session = [[AppSession alloc] init];
            self.session = [self readSessionFromFile];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)setDelegate:(id)aDelegate {
    
    @try {
        delegate = aDelegate;
        [delegate sessionDidUpdate:self.session];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (AppSession *)getCurrentSession {
    return self.session;
}

- (void)requestSessionWithEmail:(NSString *)email
                       Password:(NSString *)password {
    
    @try {
        [delegate networkStatusDidUpdate:NETWORK_PROCESS];
        [self authenticateOAuth2WithUsername:email
                                    Password:password
                                    Callback:^(AppCredential *credential) {
                                        
            if (credential) {
             
                [self getUserInfoWithAccessToken:credential.accessToken Callback:^(AppUserInfo *userInfo) {
                    
                    if (userInfo) {
                        
                        [self registerNewSessionWithOAuth2Credential:credential OAuth2UserInfo:userInfo];
                        [delegate networkStatusDidUpdate:NETWORK_NONE];
                        [delegate requestStatusDidUpdate:REQUEST_SUCCESS];
                        
                    } else {
                        [delegate networkStatusDidUpdate:NETWORK_NONE];
                        [delegate requestStatusDidUpdate:REQUEST_ERROR];
                    }
                }];
            } else {
                [delegate networkStatusDidUpdate:NETWORK_NONE];
                [delegate requestStatusDidUpdate:REQUEST_ERROR];
            }
        }];
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)requestSessionWithGoogleEmail:(NSString *)email
                              TokenID:(NSString *)tokenID {
    
    @try {
        [delegate networkStatusDidUpdate:NETWORK_PROCESS];
        [self authenticateOAuth2WithUsername:[NSString stringWithFormat:@"%@;google", email]
                                    Password:tokenID
                                    Callback:^(AppCredential *credential) {
        
            if (credential) {
                
                [self getUserInfoWithAccessToken:credential.accessToken Callback:^(AppUserInfo *userInfo) {
                                                
                    if (userInfo) {
                        
                        [self registerNewSessionWithOAuth2Credential:credential OAuth2UserInfo:userInfo];
                        [delegate networkStatusDidUpdate:NETWORK_NONE];
                        [delegate requestStatusDidUpdate:REQUEST_SUCCESS];
                        
                    } else {
                        [delegate networkStatusDidUpdate:NETWORK_NONE];
                        [delegate requestStatusDidUpdate:REQUEST_ERROR];
                    }
                }];
            } else {
                [delegate networkStatusDidUpdate:NETWORK_NONE];
                [delegate requestStatusDidUpdate:REQUEST_ERROR];
            }
        }];
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)requestSubscriptionWithSubsysID:(NSString *)subsysID {

    @try {
        [delegate networkStatusDidUpdate:NETWORK_PROCESS];
        [self subscribeSubsysWithSubsysID:subsysID Callback:^(BOOL result) {
            
            if (result) {
                [self requestSubscriptionInfo];
                [delegate networkStatusDidUpdate:NETWORK_NONE];
                [delegate requestStatusDidUpdate:REQUEST_SUCCESS];
            } else {
                [delegate networkStatusDidUpdate:NETWORK_NONE];
                [delegate requestStatusDidUpdate:REQUEST_ERROR];
            }
        }];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)requestSubscriptionInfo {
    
    @try {
        [delegate networkStatusDidUpdate:NETWORK_PROCESS];
        [self getSubscriptionInfoWithCallback:^(NSArray *subscriptionList) {
           
            [delegate subscriptionDidUpdate:subscriptionList];
            [delegate networkStatusDidUpdate:NETWORK_NONE];
        }];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)destroySession {
    
    @try {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *sessionFilePath = [[paths firstObject] stringByAppendingPathComponent:@"Session.plist"];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:sessionFilePath]) {
            
            NSError *error;
            if ([fileManager removeItemAtPath:sessionFilePath error:&error]) {
                
                self.session = nil;
                [delegate sessionDidUpdate:nil];
                [delegate subscriptionDidUpdate:nil];
            }
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - Private Method
- (AppSession *)readSessionFromFile {
    
    AppSession *curSession;
    
    @try {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *sessionFilePath = [[paths firstObject] stringByAppendingPathComponent:@"Session.plist"];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:sessionFilePath]) {
            
            NSMutableDictionary *sessionDic = [NSMutableDictionary dictionaryWithContentsOfFile:sessionFilePath];
            if (sessionDic) {
                
                curSession = [[AppSession alloc] init];
                curSession.accessToken = [sessionDic objectForKey:@"accessToken"];
                curSession.refreshToken = [sessionDic objectForKey:@"refreshToken"];
                curSession.tokenExpire = [sessionDic objectForKey:@"tokenExpire"];
                curSession.tokenType = [sessionDic objectForKey:@"tokenType"];
                curSession.userID = [sessionDic objectForKey:@"userID"];
                curSession.email = [sessionDic objectForKey:@"email"];
                curSession.givenName = [sessionDic objectForKey:@"givenName"];
                curSession.familyName = [sessionDic objectForKey:@"familyName"];
                curSession.photoURL = [sessionDic objectForKey:@"photoURL"];
            }
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return curSession;
    }
}

- (void)authenticateOAuth2WithUsername:(NSString *)username
                              Password:(NSString *)password
                              Callback:(void (^)(AppCredential *))callback {
    @try {
        NSString *certificate = [[NSBundle mainBundle] pathForResource:OAUTH2_CERTIFICATE ofType:@"cer"];
        if (certificate) {
            
            AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
            [securityPolicy setPinnedCertificates:@[[NSData dataWithContentsOfFile:certificate]]];
            [securityPolicy setValidatesDomainName:NO];
            [securityPolicy setAllowInvalidCertificates:YES];
            
            /* Setup OAuth 2.0 */
            AFOAuth2Manager *oauth2Manager = [[AFOAuth2Manager alloc] initWithBaseURL:[NSURL URLWithString:OAUTH2_DOMAIN]
                                                                             clientID:OAUTH2_CLIENT_ID
                                                                               secret:OAUTH2_CLIENT_SECRET];
            [oauth2Manager setSecurityPolicy:securityPolicy];
            
            /* Authenticate with OAuth2Manager */
            [oauth2Manager authenticateUsingOAuthWithURLString:OAUTH2_TOKEN_URL
                                                      username:username
                                                      password:password
                                                         scope:OAUTH2_APP_SCOPE
                                                       success:^(AFOAuthCredential *credential) {
                                                           
                                                           AppCredential *oauth2Credential = [[AppCredential alloc] init];
                                                           oauth2Credential.accessToken = credential.accessToken;
                                                           oauth2Credential.refreshToken = credential.refreshToken;
                                                           oauth2Credential.tokenExpire = credential.expiration;
                                                           oauth2Credential.tokenType = credential.tokenType;
                                                           callback((AppCredential *)oauth2Credential);
                                                       }
                                                       failure:^(NSError *error) {
                                                           callback((AppCredential *)nil);
                                                       }];
        } else {
            callback((AppCredential *)nil);
        }
    } @catch (NSException *exception) {
        @throw exception;
    }
}

- (void)getUserInfoWithAccessToken:(NSString *)userAccessToken
                          Callback:(void (^)(AppUserInfo *))callback {
    @try {
        NSString *certificate = [[NSBundle mainBundle] pathForResource:OAUTH2_CERTIFICATE ofType:@"cer"];
        if (certificate) {
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
            manager.securityPolicy = policy;
            manager.securityPolicy.pinnedCertificates = @[[NSData dataWithContentsOfFile:certificate]];
            manager.securityPolicy.allowInvalidCertificates = YES;
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            
            [manager GET:[OAUTH2_DOMAIN stringByAppendingString:OAUTH2_API_USERINFO]
              parameters:@{ @"access_token": userAccessToken }
                 success:^(AFHTTPRequestOperation *operation, id res) {
                     if (res && [[res objectForKey:@"success"] isEqual:[NSNumber numberWithBool:YES]]) {
                         
                         AppUserInfo *userInfo = [[AppUserInfo alloc] init];
                         userInfo.userID = [res objectForKey:@"userID"];
                         userInfo.email = [res objectForKey:@"email"];
                         userInfo.givenName = [res objectForKey:@"givenName"];
                         userInfo.familyName = [res objectForKey:@"familyName"];
                         userInfo.photoURL = [res objectForKey:@"photoPath"];
                         callback((AppUserInfo *)userInfo);
                         
                     } else {
                         callback((AppUserInfo *)nil);
                     }
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     callback((AppUserInfo *)nil);
                 }
             ];
        } else {
            callback((AppUserInfo *)nil);
        }
    } @catch (NSException *exception) {
        @throw exception;
    }
}

- (void)registerNewSessionWithOAuth2Credential:(AppCredential *)credential
                                OAuth2UserInfo:(AppUserInfo *)userInfo {
    @try {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *sessionFilePath = [[paths firstObject] stringByAppendingPathComponent:@"Session.plist"];
    
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath:sessionFilePath]) {
            NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"Session" ofType:@"plist"];
            if (bundlePath) {
                [fileManager copyItemAtPath:bundlePath toPath:sessionFilePath error:nil];
            }
        }
        
        NSMutableDictionary *sessionDic = [NSMutableDictionary dictionaryWithContentsOfFile:sessionFilePath];
        if (sessionDic) {
            
            [sessionDic setObject:credential.accessToken forKey:@"accessToken"];
            [sessionDic setObject:credential.refreshToken forKey:@"refreshToken"];
            [sessionDic setObject:credential.tokenExpire forKey:@"tokenExpire"];
            [sessionDic setObject:credential.tokenType forKey:@"tokenType"];
            [sessionDic setObject:userInfo.userID forKey:@"userID"];
            [sessionDic setObject:userInfo.email forKey:@"email"];
            [sessionDic setObject:userInfo.givenName forKey:@"givenName"];
            [sessionDic setObject:userInfo.familyName forKey:@"familyName"];
            if (userInfo.photoURL) {
                [sessionDic setObject:userInfo.photoURL forKey:@"photoURL"];
            }
            [sessionDic writeToFile:sessionFilePath atomically:YES];
            
            self.session = nil;
            self.session = [[AppSession alloc] init];
            self.session.accessToken = credential.accessToken;
            self.session.refreshToken = credential.refreshToken;
            self.session.tokenExpire = credential.tokenExpire;
            self.session.tokenType = credential.tokenType;
            self.session.userID = userInfo.userID;
            self.session.email = userInfo.email;
            self.session.givenName = userInfo.givenName;
            self.session.familyName = userInfo.familyName;
            self.session.photoURL = userInfo.photoURL;
            
            [delegate sessionDidUpdate:self.session];
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)subscribeSubsysWithSubsysID:(NSString *)subsysID
                           Callback:(void (^)(BOOL))callback {
    
    @try {
        NSString *accessToken = (self.session && self.session.accessToken) ? self.session.accessToken : nil;
        NSString *certificate = [[NSBundle mainBundle] pathForResource:OAUTH2_CERTIFICATE ofType:@"cer"];
        if (certificate && accessToken) {
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
            manager.securityPolicy = policy;
            manager.securityPolicy.pinnedCertificates = @[[NSData dataWithContentsOfFile:certificate]];
            manager.securityPolicy.allowInvalidCertificates = YES;
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            
            [manager POST:[[OAUTH2_DOMAIN stringByAppendingString:OAUTH2_API_SUBSCRIBE] stringByAppendingString:accessToken]
               parameters:@{ @"clientID": subsysID }
                  success:^(AFHTTPRequestOperation *operation, id res) {
                      if (res && [[res objectForKey:@"success"] isEqual:[NSNumber numberWithBool:YES]]) {
                          
                          callback(YES);
                          
                      } else {
                          
                          NSString *alertMsg = (res) ? [res objectForKey:@"message"] : BLANK;
                          UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Subscription failed"
                                                                                         message:alertMsg
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                          UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                                             style:UIAlertActionStyleDefault
                                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                                               callback(NO);
                                                                           }];
                          [alert addAction:okButton];
                          [[Common getTopController] presentViewController:alert animated:YES completion:nil];
                      }
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                      UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Subscription failed"
                                                                                     message:[error description]
                                                                              preferredStyle:UIAlertControllerStyleAlert];
                      UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                                         style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                                           callback(NO);
                                                                       }];
                      [alert addAction:okButton];
                      [[Common getTopController] presentViewController:alert animated:YES completion:nil];
                  }
             ];
        } else {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Subscription failed"
                                                                           message:@"Invalid certificate or access token"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 callback(NO);
                                                             }];
            [alert addAction:okButton];
            [[Common getTopController] presentViewController:alert animated:YES completion:nil];
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)getSubscriptionInfoWithCallback:(void (^)(NSArray *))callback {
    
    @try {
        NSString *accessToken = (self.session && self.session.accessToken) ? self.session.accessToken : nil;
        NSString *certificate = [[NSBundle mainBundle] pathForResource:OAUTH2_CERTIFICATE ofType:@"cer"];
        if (certificate && accessToken) {
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
            manager.securityPolicy = policy;
            manager.securityPolicy.pinnedCertificates = @[[NSData dataWithContentsOfFile:certificate]];
            manager.securityPolicy.allowInvalidCertificates = YES;
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            
            [manager GET:[OAUTH2_DOMAIN stringByAppendingString:OAUTH2_API_SUBSCRIPTIONINFO]
              parameters:@{ @"access_token": accessToken }
                 success:^(AFHTTPRequestOperation *operation, id res) {
                     if (res && [[res objectForKey:@"success"] isEqual:[NSNumber numberWithBool:YES]]) {
                         
                         NSArray *subscriptionArray = [res objectForKey:@"subscription"];
                         callback((NSArray *)subscriptionArray);
                         
                     } else {
                         callback((NSArray *)nil);
                     }
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    callback((NSArray *)nil);
                 }
             ];
        } else {
            callback((NSArray *)nil);
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
