//
//  UIButton+Apps.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/20/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "./Common-Util.h" 

@interface UIButton (Apps)

- (instancetype)initWithFrame:(CGRect)frame
                        Title:(NSString *)title
                         Font:(NSString *)font
                         Size:(CGFloat)size
                   TitleColor:(UIColor *)titleColor
            TitleHilightColor:(UIColor *)titleHilightColor
               UnderlineStyle:(NSUnderlineStyle)underlineStyle
                        Color:(UIColor *)color
                 HilightColor:(UIColor *)hilightColor
                  BorderColor:(UIColor *)borderColor
                  BorderWidth:(CGFloat)borderWidth
                 BorderRadius:(CGFloat)borderRadius;

@end
