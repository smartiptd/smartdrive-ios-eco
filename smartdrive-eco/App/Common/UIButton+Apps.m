//
//  UIButton+Apps.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/20/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "UIButton+Apps.h"

@implementation UIButton (Apps)

- (instancetype)initWithFrame:(CGRect)frame
                        Title:(NSString *)title
                         Font:(NSString *)font
                         Size:(CGFloat)size
                   TitleColor:(UIColor *)titleColor
            TitleHilightColor:(UIColor *)titleHilightColor
               UnderlineStyle:(NSUnderlineStyle)underlineStyle
                        Color:(UIColor *)color
                 HilightColor:(UIColor *)hilightColor
                  BorderColor:(UIColor *)borderColor
                  BorderWidth:(CGFloat)borderWidth
                 BorderRadius:(CGFloat)borderRadius {
    
    @try {
        self = [super initWithFrame:frame];
        if (self) {
            
            [self setEnabled:YES];
            [self.titleLabel setFont:[UIFont fontWithName:font size:size]];
            NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:title];
            [attrStr setAttributes:@{NSForegroundColorAttributeName:titleColor,
                                     NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:underlineStyle]}
                             range:NSMakeRange(0, [attrStr length])];
            NSMutableAttributedString *attrStrHi = [[NSMutableAttributedString alloc] initWithString:title];
            [attrStrHi setAttributes:@{NSForegroundColorAttributeName:titleHilightColor,
                                       NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:underlineStyle]}
                               range:NSMakeRange(0, [attrStr length])];
            [self setAttributedTitle:attrStr forState:UIControlStateNormal];
            [self setAttributedTitle:attrStrHi forState:UIControlStateHighlighted];
            [self setBackgroundImage:[Common imageWithColor:color] forState:UIControlStateNormal];
            [self setBackgroundImage:[Common imageWithColor:hilightColor] forState:UIControlStateHighlighted];
            [self.layer setBorderColor:borderColor.CGColor];
            [self.layer setBorderWidth:borderWidth];
            [self.layer setCornerRadius:borderRadius];
            [self.layer setMasksToBounds:YES];
            [self.layer setShadowOffset:CGSizeZero];
            [self.layer setShadowOpacity:0.0f];
        }
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        return self;
    }
}

@end
