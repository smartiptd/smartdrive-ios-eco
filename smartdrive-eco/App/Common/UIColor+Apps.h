//
//  UIColor+Apps.h
//  smartdrive-eco
//
//  Created by Koon~ on 5/3/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Apps)

/* Custom application color */
+ (UIColor *)BG_WHITE;
+ (UIColor *)BG_LIGHT_GRAY;
+ (UIColor *)BG_MEDIUM_GRAY;
+ (UIColor *)BG_DARK_GRAY;
+ (UIColor *)BG_MENU;

+ (UIColor *)SCREEN_SEPARATOR;
+ (UIColor *)TEXTFIELD_BORDER;
+ (UIColor *)INDICATOR_VIEW;

+ (UIColor *)BUTTON_FACEBOOK;
+ (UIColor *)BUTTON_FACEBOOK_HILIGHT;
+ (UIColor *)BUTTON_GOOGLE;
+ (UIColor *)BUTTON_GOOGLE_HILIGHT;
+ (UIColor *)LIVINGTECH_RED;
+ (UIColor *)LIVINGTECH_RED_HILIGHT;

+ (UIColor *)TEXT_WHITE;
+ (UIColor *)TEXT_GRAY;
+ (UIColor *)TEXT_LIGHT_GRAY;
+ (UIColor *)TEXT_MEDIUM_GRAY;
+ (UIColor *)TEXT_DARK_GRAY;
+ (UIColor *)TEXT_AAF_TEMP_IN_OVER_OUT;
+ (UIColor *)TEXT_AAF_TEMP_IN_EQUAL_OUT;
+ (UIColor *)TEXT_AAF_TEMP_OUT_OVER_IN;

+ (UIColor *)HOME_MENU_LIGHT_GRAY;
+ (UIColor *)HOME_MENU_MEDIUM_GRAY;
+ (UIColor *)HOME_MENU_DARK_GRAY;

+ (UIColor *)AAF_MENU_BUTTON_DARK_GRAY;
+ (UIColor *)AAF_MENU_BUTTON__DARK_GRAY_HILIGHT;
+ (UIColor *)AAF_MENU_MAIN_CONTROL_GRAY;
+ (UIColor *)AAF_MENU_CONTROL_GRAY;

@end
