//
//  UIColor+Apps.m
//  smartdrive-eco
//
//  Created by Koon~ on 5/3/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "UIColor+Apps.h"

@implementation UIColor (Apps)

/* Custom application color */
+ (UIColor *)BG_WHITE {
    return [UIColor whiteColor];
}

+ (UIColor *)BG_LIGHT_GRAY {
    return [UIColor colorWithRed:0.976 green:0.976 blue:0.976 alpha:1];
}

+ (UIColor *)BG_MEDIUM_GRAY {
    return [UIColor colorWithRed:0.345 green:0.345 blue:0.345 alpha:1];
}

+ (UIColor *)BG_DARK_GRAY {
    return [UIColor colorWithRed:0.137 green:0.121 blue:0.125 alpha:1];
}

+ (UIColor *)BG_MENU {
    return [UIColor colorWithRed:0.266 green:0.254 blue:0.258 alpha:1];
}

+ (UIColor *)SCREEN_SEPARATOR {
    return [UIColor colorWithRed:0.777 green:0.777 blue:0.796 alpha:1];
}

+ (UIColor *)TEXTFIELD_BORDER {
    return [UIColor colorWithRed:0.345 green:0.345 blue:0.345 alpha:1];
}

+ (UIColor *)INDICATOR_VIEW {
    return [UIColor colorWithRed:0.55 green:0.55 blue:0.55 alpha:0.50];
}

+ (UIColor *)BUTTON_FACEBOOK {
    return [UIColor colorWithRed:0.230 green:0.348 blue:0.594 alpha:1];
}

+ (UIColor *)BUTTON_FACEBOOK_HILIGHT {
    return [UIColor colorWithRed:0.130 green:0.248 blue:0.494 alpha:1];
}

+ (UIColor *)BUTTON_GOOGLE {
    return [UIColor colorWithRed:0.855 green:0.265 blue:0.214 alpha:1];
}

+ (UIColor *)BUTTON_GOOGLE_HILIGHT {
    return [UIColor colorWithRed:0.755 green:0.165 blue:0.114 alpha:1];
}

+ (UIColor *)LIVINGTECH_RED {
    return [UIColor colorWithRed:0.925 green:0.125 blue:0.125 alpha:1];
}

+ (UIColor *)LIVINGTECH_RED_HILIGHT {
    return [UIColor colorWithRed:0.725 green:0.025 blue:0.025 alpha:1];
}

+ (UIColor *)TEXT_WHITE {
    return [UIColor whiteColor];
}

+ (UIColor *)TEXT_GRAY {
    return [UIColor colorWithRed:0.345 green:0.345 blue:0.356 alpha:1];
}

+ (UIColor *)TEXT_LIGHT_GRAY {
    return [UIColor colorWithRed:0.345 green:0.345 blue:0.345 alpha:1];
}

+ (UIColor *)TEXT_MEDIUM_GRAY {
    return [UIColor colorWithRed:0.537 green:0.539 blue:0.544 alpha:1];
}

+ (UIColor *)TEXT_DARK_GRAY {
    return [UIColor colorWithRed:0.168 green:0.168 blue:0.168 alpha:1];
}

+ (UIColor *)TEXT_AAF_TEMP_IN_OVER_OUT {
    return [UIColor colorWithRed:0.925 green:0.125 blue:0.125 alpha:1];
}

+ (UIColor *)TEXT_AAF_TEMP_IN_EQUAL_OUT {
    return [UIColor colorWithRed:0.168 green:0.168 blue:0.168 alpha:1];
}

+ (UIColor *)TEXT_AAF_TEMP_OUT_OVER_IN {
    return [UIColor colorWithRed:0.192 green:0.733 blue:0.874 alpha:1];
}

+ (UIColor *)HOME_MENU_LIGHT_GRAY {
    return [UIColor colorWithRed:0.92 green:0.92 blue:0.92 alpha:1];
}

+ (UIColor *)HOME_MENU_MEDIUM_GRAY {
    return [UIColor colorWithRed:0.82 green:0.82 blue:0.82 alpha:1];
}

+ (UIColor *)HOME_MENU_DARK_GRAY {
    return [UIColor colorWithRed:0.72 green:0.72 blue:0.72 alpha:1];
}

+ (UIColor *)AAF_MENU_BUTTON_DARK_GRAY {
    return [UIColor colorWithRed:0.223 green:0.211 blue:0.211 alpha:1];
}

+ (UIColor *)AAF_MENU_BUTTON__DARK_GRAY_HILIGHT {
    return [UIColor colorWithRed:0.627 green:0.125 blue:0.125 alpha:1];
}

+ (UIColor *)AAF_MENU_MAIN_CONTROL_GRAY {
    return [UIColor colorWithRed:0.439 green:0.439 blue:0.439 alpha:1];
}

+ (UIColor *)AAF_MENU_CONTROL_GRAY {
    return [UIColor colorWithRed:0.588 green:0.588 blue:0.588 alpha:1];
}

@end
