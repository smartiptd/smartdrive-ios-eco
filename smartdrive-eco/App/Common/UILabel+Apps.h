//
//  UILabel+Apps.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/20/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "./Common-Util.h"

@interface UILabel (Apps)

- (instancetype)initWithFrame:(CGRect)frame
                         Font:(NSString *)font
                         Size:(CGFloat)size
                        Color:(UIColor *)color
                    Alignment:(NSTextAlignment)align
                         Text:(NSString *)text;

@end
