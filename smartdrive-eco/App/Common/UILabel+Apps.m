//
//  UILabel+Apps.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/20/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "UILabel+Apps.h"

@implementation UILabel (Apps)

- (instancetype)initWithFrame:(CGRect)frame
                         Font:(NSString *)font
                         Size:(CGFloat)size
                        Color:(UIColor *)color
                    Alignment:(NSTextAlignment)align
                         Text:(NSString *)text {
    
    @try {
        self = [super initWithFrame:frame];
        if (self) {
            
            [self setEnabled:YES];
            [self setFont:[UIFont fontWithName:font size:size]];
            [self setTextColor:color];
            [self setTextAlignment:align];
            [self setBackgroundColor:[UIColor clearColor]];
            [self setAdjustsFontSizeToFitWidth:YES];
            [self setNumberOfLines:1];
            [self setText:text];
            [self.layer setBorderColor:[UIColor clearColor].CGColor];
            [self.layer setBorderWidth:0];
            [self.layer setCornerRadius:0];
            [self.layer setMasksToBounds:NO];
            [self.layer setShadowOffset:CGSizeZero];
            [self.layer setShadowColor:[UIColor clearColor].CGColor];
            [self.layer setShadowOpacity:0];
        }
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        return self;
    }
}

@end
