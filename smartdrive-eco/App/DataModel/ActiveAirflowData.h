//
//  ActiveAirflowData.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 8/2/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../Common/Common-Util.h"

/* Active AIRflow Enum */
typedef NS_ENUM(NSInteger, AAF_CONN_STATUS) {
    AAF_CONN_STATUS_OFFLINE,
    AAF_CONN_STATUS_ONLINE
};

typedef NS_ENUM(NSInteger, AAF_POWER_STATUS) {
    AAF_POWER_STATUS_OFF,
    AAF_POWER_STATUS_ON
};

typedef NS_ENUM(NSInteger, AAF_SYSTEM_MODE) {
    AAF_SYSTEM_MODE_OFF,
    AAF_SYSTEM_MODE_ON,
    AAF_SYSTEM_MODE_ECO
};

typedef NS_ENUM(NSInteger, AAF_POWER_SOURCE) {
    AAF_POWER_SOURCE_OFF,
    AAF_POWER_SOURCE_SOLAR,
    AAF_POWER_SOURCE_GRID
};

typedef NS_ENUM(NSInteger, AAF_SOLAR_LEVEL) {
    AAF_SOLAR_LEVEL_1 = 1,
    AAF_SOLAR_LEVEL_2 = 2,
    AAF_SOLAR_LEVEL_3 = 3,
    AAF_SOLAR_LEVEL_4 = 4,
    AAF_SOLAR_LEVEL_5 = 5
};

@interface ActiveAirflowData : NSObject

/* Active Airflow properties */
@property (nonatomic, strong) NSString *subsysID;
@property (nonatomic) AAF_CONN_STATUS connStatus;

@property (nonatomic, strong) NSString *firmwareVersion;
@property (nonatomic, strong) NSString *systemClock;

@property (nonatomic) AAF_POWER_STATUS powerStatus;
@property (nonatomic) AAF_SYSTEM_MODE systemMode;

@property (nonatomic) AAF_POWER_SOURCE powerSource;
@property (nonatomic) AAF_SOLAR_LEVEL solarLevel;

@property (nonatomic, strong) NSMutableDictionary *temperatureDic;
@property (nonatomic, strong) NSMutableDictionary *sensorStatusDic;
@property (nonatomic, strong) NSMutableDictionary *srtvStatusDic;
@property (nonatomic, strong) NSMutableDictionary *cvStatusDic;

- (instancetype)initWithSubsysID:(NSString *)subsysID;

@end
