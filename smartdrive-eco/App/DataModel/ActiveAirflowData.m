//
//  ActiveAirflowData.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 8/2/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ActiveAirflowData.h"

@implementation ActiveAirflowData

- (instancetype)initWithSubsysID:(NSString *)subsysID {
    
    @try {
        self = [super init];
        if (self) {
            
            self.subsysID = subsysID;
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
    
    @try {
        self.connStatus = AAF_CONN_STATUS_OFFLINE;
        
        self.firmwareVersion = BLANK;
        self.systemClock = BLANK;
        
        self.powerStatus = AAF_POWER_STATUS_OFF;
        self.systemMode = AAF_SYSTEM_MODE_OFF;
        
        self.powerSource = AAF_POWER_SOURCE_GRID;
        self.solarLevel = AAF_SOLAR_LEVEL_1;
        
        self.temperatureDic = [[NSMutableDictionary alloc] init];
        self.sensorStatusDic = [[NSMutableDictionary alloc] init];
        self.srtvStatusDic = [[NSMutableDictionary alloc] init];
        self.cvStatusDic = [[NSMutableDictionary alloc] init];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
