//
//  OAuth2Credential.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/8/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppCredential : NSObject

@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *refreshToken;
@property (nonatomic, strong) NSDate *tokenExpire;
@property (nonatomic, strong) NSString *tokenType;

@end
