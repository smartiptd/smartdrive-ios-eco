//
//  OAuth2Credential.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/8/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "AppCredential.h"

@implementation AppCredential

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.accessToken = nil;
    self.refreshToken = nil;
    self.tokenExpire = nil;
    self.tokenType = nil;
}

@end
