//
//  AppSession.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/10/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../Common/Common-Util.h"

@interface AppSession : NSObject

@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *refreshToken;
@property (nonatomic, strong) NSDate *tokenExpire;
@property (nonatomic, strong) NSString *tokenType;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *givenName;
@property (nonatomic, strong) NSString *familyName;
@property (nonatomic, strong) NSString *photoURL;

@end
