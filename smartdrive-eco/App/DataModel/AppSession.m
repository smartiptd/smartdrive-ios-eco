//
//  AppSession.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/10/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "AppSession.h"

@implementation AppSession

- (instancetype)init {
    
    @try {
        self = [super init];
        if (self) {
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
    
    @try {
        self.accessToken = BLANK;
        self.refreshToken = BLANK;
        self.tokenExpire = NULL;
        self.tokenType = BLANK;
        self.userID = BLANK;
        self.email = BLANK;
        self.givenName = BLANK;
        self.familyName = BLANK;
        self.photoURL = BLANK;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
