//
//  OAuth2UserInfo.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/8/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "AppUserInfo.h"

@implementation AppUserInfo

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.userID = nil;
    self.email = nil;
    self.givenName = nil;
    self.familyName = nil;
    self.photoURL = nil;
}

@end
