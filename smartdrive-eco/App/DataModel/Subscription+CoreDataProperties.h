//
//  Subscription+CoreDataProperties.h
//  
//
//  Created by Sukrit  Chuen-im on 7/22/2559 BE.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Subscription.h"

NS_ASSUME_NONNULL_BEGIN

@interface Subscription (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *subscriptionID;
@property (nullable, nonatomic, retain) NSString *subsysID;
@property (nullable, nonatomic, retain) NSString *subsysType;
@property (nullable, nonatomic, retain) NSString *subscribeUser;
@property (nullable, nonatomic, retain) NSString *grantUser;
@property (nullable, nonatomic, retain) NSNumber *grantFlag;
@property (nullable, nonatomic, retain) NSString *refName;

@end

NS_ASSUME_NONNULL_END
