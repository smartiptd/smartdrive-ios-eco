//
//  Subscription+CoreDataProperties.m
//  
//
//  Created by Sukrit  Chuen-im on 7/22/2559 BE.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Subscription+CoreDataProperties.h"

@implementation Subscription (CoreDataProperties)

@dynamic subscriptionID;
@dynamic subsysID;
@dynamic subsysType;
@dynamic subscribeUser;
@dynamic grantUser;
@dynamic grantFlag;
@dynamic refName;

@end
