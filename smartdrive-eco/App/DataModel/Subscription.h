//
//  Subscription.h
//  
//
//  Created by Sukrit  Chuen-im on 7/22/2559 BE.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Subscription : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Subscription+CoreDataProperties.h"
