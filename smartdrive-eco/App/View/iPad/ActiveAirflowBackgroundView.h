//
//  ActiveAirflowBgView.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/27/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../DataModel/ActiveAirflowData.h"
#import "../../Common/Common-Util.h"
#import "../../Common/AppsImageSet.h"

@interface ActiveAirflowBackgroundView : UIView

@property (strong, nonatomic) UIImageView *imvBgLandscape;
@property (strong, nonatomic) UIImageView *imvBgHouse;
@property (strong, nonatomic) UIImageView *imvAnWind;
@property (strong, nonatomic) UIImageView *imvAnHeat;
@property (strong, nonatomic) UIImageView *imvAnCeiling;
@property (strong, nonatomic) UIView *stripView;

@property (strong, nonatomic) UILabel *lblIndoor;
@property (strong, nonatomic) UILabel *lblOutdoor;
@property (strong, nonatomic) UILabel *lblLivingRoom;
@property (strong, nonatomic) UILabel *lblHall;
@property (strong, nonatomic) UILabel *lblAttic;
@property (strong, nonatomic) UILabel *lblIndoorTempValue;
@property (strong, nonatomic) UILabel *lblOutdoorTempValue;
@property (strong, nonatomic) UILabel *lblLivingRoomTempValue;
@property (strong, nonatomic) UILabel *lblHallTempValue;
@property (strong, nonatomic) UILabel *lblAtticTempValue;
@property (strong, nonatomic) UIImageView *imvPowerSource;
@property (strong, nonatomic) UIImageView *imvPowerSourceBar;

@end

@interface ActiveAirflowBackgroundView (ActiveAirflowBgViewEX)

- (void)displayBgHouseWithHouseStatus:(AAF_HOUSE_STATUS)houseStatus;
- (void)displayBgLandscapeWithFade:(BOOL)fadeFlag;
- (void)displayAnWindWithAnimation:(BOOL)animationFlag;
- (void)displayAnCeilingWithAnimation:(BOOL)animationFlag;
- (void)displayAnHeat;

- (void)setIndoorTempValue:(NSNumber *)indoorTempValue;
- (void)setOutdoorTempValue:(NSNumber *)outdoorTempValue;
- (void)setLivingRoomTempValue:(NSNumber *)livingRoomTempValue;
- (void)setHallTempValue:(NSNumber *)hallTempValue;
- (void)setAtticTempValue:(NSNumber *)atticTempValue;
- (void)setPowerSource:(AAF_POWER_SOURCE)source;
- (void)setPowerSourceBar:(AAF_SOLAR_LEVEL)level;

@end