//
//  ActiveAirflowBgView.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/27/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ActiveAirflowBackgroundView.h"

#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_8_0
#define GregorianCalendar NSCalendarIdentifierGregorian
#define HourCalendarUnit NSCalendarUnitHour
#else
#define GregorianCalendar NSGregorianCalendar
#define HourCalendarUnit NSHourCalendarUnit
#endif

static NSString * const PIC_BG_LANDSCAPE            = @"ipad-bg-landscape.png";
static NSString * const PIC_BG_LANDSCAPE_FADE       = @"ipad-bg-landscape-fade.png";
static NSString * const PIC_BG_HOUSE_ON             = @"ipad-bg-house-on.png";
static NSString * const PIC_BG_HOUSE_OFF            = @"ipad-bg-house-off.png";
static NSString * const PIC_BG_HOUSE_COOL           = @"ipad-bg-house-cool.png";

@interface ActiveAirflowBackgroundView()

@property (nonatomic, strong) NSTimer *heatAnTimer;

@end

@implementation ActiveAirflowBackgroundView

- (instancetype)initWithFrame:(CGRect)frame {
    
    @try {
        self = [super initWithFrame:frame];
        if (self) {
            
            [self initSubview];
            [self initContent];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initSubview {
    
    CGFloat frameW = self.frame.size.width;
    CGFloat frameH = self.frame.size.height;
    
    CGFloat stripH;
    
    @try {
        /* Component calibration */
        stripH = frameH * 0.0403f;
        
        /* Initial UI component */
        self.imvBgLandscape = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frameW, frameH)];
        
        self.imvBgHouse = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frameW, frameH)];
        
        self.imvAnWind= [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frameW, frameH)];
        [self.imvAnWind setAnimationImages:[AppsImageSet IPAD_WIND_IMAGE_SET]];
        [self.imvAnWind setAnimationDuration:5];
        [self.imvAnWind setAnimationRepeatCount:0];
        
        self.imvAnHeat = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frameW, frameH)];
        [self.imvAnHeat setAnimationImages:[AppsImageSet IPAD_HEAT_IMAGE_SET]];
        [self.imvAnHeat setAnimationDuration:5];
        [self.imvAnHeat setAnimationRepeatCount:0];
        self.heatAnTimer = [NSTimer scheduledTimerWithTimeInterval:60
                                                            target:self
                                                          selector:@selector(displayAnHeat)
                                                          userInfo:nil
                                                           repeats:YES];
        
        self.imvAnCeiling = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frameW, frameH)];
        [self.imvAnCeiling setAnimationImages:[AppsImageSet IPAD_CV_IMAGE_SET]];
        [self.imvAnCeiling setAnimationDuration:0.6];
        [self.imvAnCeiling setAnimationRepeatCount:0];
        
        self.stripView = [[UIView alloc] initWithFrame:CGRectMake(0, frameH - stripH, frameW, stripH)];
        [self.stripView setBackgroundColor:[UIColor LIVINGTECH_RED]];
        
        [self displayBgHouseWithHouseStatus:AAF_HOUSE_STATUS_OFF];
        [self displayBgLandscapeWithFade:NO];
        [self displayAnWindWithAnimation:NO];
        [self displayAnCeilingWithAnimation:NO];
        
        /* Add sub view */
        [self addSubview:self.imvBgLandscape];
        [self addSubview:self.imvBgHouse];
        [self addSubview:self.imvAnWind];
        [self addSubview:self.imvAnHeat];
        [self addSubview:self.imvAnCeiling];
        [self addSubview:self.stripView];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initContent {
    
    CGFloat frameW = self.frame.size.width;
    CGFloat frameH = self.frame.size.height;
    
    CGFloat lblRoomW, lblRoomH, lblRoomTempW, lblRoomTempH;
    CGFloat lblIndoorX, lblIndoorY, lblIndoorW, lblIndoorH;
    CGFloat lblOutdooorX, lblOutdooorY, lblOutdooorW, lblOutdooorH;
    CGFloat lblLivingRoomX,lblLivingRoomY, lblLivingRoomW, lblLivingRoomH;
    CGFloat lblHallX, lblHallY, lblHallW, lblHallH;
    CGFloat lblAtticX, lblAtticY, lblAtticW, lblAtticH;
    CGFloat lblIndoorTempValueX, lblIndoorTempValueY, lblIndoorTempValueW, lblIndoorTempValueH;
    CGFloat lblOutdoorTempValueX, lblOutdoorTempValueY, lblOutdoorTempValueW, lblOutdoorTempValueH;
    CGFloat lblLivingRoomTempValueX, lblLivingRoomTempValueY, lblLivingRoomTempValueW, lblLivingRoomTempValueH;
    CGFloat lblHallTempValueX, lblHallTempValueY, lblHallTempValueW, lblHallTempValueH;
    CGFloat lblAtticTempValueX, lblAtticTempValueY, lblAtticTempValueW, lblAtticTempValueH;
    CGFloat imvPowerSourceX, imvPowerSourceY, imvPowerSourceW, imvPowerSourceH;
    CGFloat imvPowerSourceBarX, imvPowerSourceBarY, imvPowerSourceBarW, imvPowerSourceBarH;
    
    @try {
        /* Component calibration */
        lblIndoorW = frameW * 0.1611f;
        lblIndoorH = frameH * 0.0390f;
        lblIndoorX = frameW * 0.5332f;
        lblIndoorY = frameH * 0.7682f;
        
        lblOutdooorW = lblIndoorW;
        lblOutdooorH = lblIndoorH;
        lblOutdooorX = frameW * 0.6943f;
        lblOutdooorY = lblIndoorY;
        
        lblIndoorTempValueW = lblIndoorW;
        lblIndoorTempValueH = frameH * 0.0690f;
        lblIndoorTempValueX = lblIndoorX;
        lblIndoorTempValueY = frameH * 0.8072f;
        
        lblOutdoorTempValueW = lblIndoorW;
        lblOutdoorTempValueH = lblIndoorTempValueH;
        lblOutdoorTempValueX = lblOutdooorX;
        lblOutdoorTempValueY = lblIndoorTempValueY;
        
        lblRoomW = frameW * 0.1464f;
        lblRoomH = frameH * 0.0208f;
        
        lblRoomTempW = lblRoomW;
        lblRoomTempH = frameH * 0.0325f;
        
        lblLivingRoomW = lblRoomW;
        lblLivingRoomH = lblRoomH;
        lblLivingRoomX = frameW * 0.3662f;
        lblLivingRoomY = frameH * 0.5924f;
        
        lblLivingRoomTempValueW = lblRoomTempW;
        lblLivingRoomTempValueH = lblRoomTempH;
        lblLivingRoomTempValueX = lblLivingRoomX;
        lblLivingRoomTempValueY = frameH * 0.6119f;
        
        lblHallW = lblRoomW;
        lblHallH = lblRoomH;
        lblHallX = frameW * 0.4833f;
        lblHallY = frameH * 0.4687f;
        
        lblHallTempValueW = lblRoomTempW;
        lblHallTempValueH = lblRoomTempH;
        lblHallTempValueX = lblHallX;
        lblHallTempValueY = frameH * 0.4882f;
        
        lblAtticW = lblRoomW;
        lblAtticH = lblRoomH;
        lblAtticX = (frameW * 0.5273f) - (lblRoomW / 2);
        lblAtticY = frameH * 0.2734f;
        
        lblAtticTempValueW = lblRoomTempW;
        lblAtticTempValueH = lblRoomTempH;
        lblAtticTempValueX = lblAtticX;
        lblAtticTempValueY = frameH * 0.2929f;
        
        imvPowerSourceW = frameW * 0.1953f;
        imvPowerSourceH = frameH * 0.0390f;
        imvPowerSourceX = frameW * 0.2353f;
        imvPowerSourceY = frameH * 0.7656f;
        
        imvPowerSourceBarW = imvPowerSourceW;
        imvPowerSourceBarH = frameH * 0.0234f;
        imvPowerSourceBarX = imvPowerSourceX;
        imvPowerSourceBarY = frameH * 0.8203f;
        
        /* Initial contents */
        self.lblIndoor = [[UILabel alloc] initWithFrame:CGRectMake(lblIndoorX, lblIndoorY, lblIndoorW, lblIndoorH)
                                                   Font:FONT_DB_HELVETICA_BD_COND
                                                   Size:30
                                                  Color:[UIColor TEXT_DARK_GRAY]
                                              Alignment:NSTextAlignmentLeft
                                                   Text:@"INDOOR"];
        
        self.lblOutdoor = [[UILabel alloc] initWithFrame:CGRectMake(lblOutdooorX, lblOutdooorY, lblOutdooorW, lblOutdooorH)
                                                    Font:FONT_DB_HELVETICA_BD_COND
                                                    Size:30
                                                   Color:[UIColor TEXT_DARK_GRAY]
                                               Alignment:NSTextAlignmentLeft
                                                    Text:@"OUTDOOR"];
        
        self.lblIndoorTempValue = [[UILabel alloc] initWithFrame:CGRectMake(lblIndoorTempValueX, lblIndoorTempValueY, lblIndoorTempValueW, lblIndoorTempValueH)
                                                            Font:FONT_HELVETICA_NEUE_THIN
                                                            Size:53
                                                           Color:[UIColor TEXT_DARK_GRAY]
                                                       Alignment:NSTextAlignmentLeft
                                                            Text:@"°C"];
        
        self.lblOutdoorTempValue = [[UILabel alloc] initWithFrame:CGRectMake(lblOutdoorTempValueX, lblOutdoorTempValueY, lblOutdoorTempValueW, lblOutdoorTempValueH)
                                                             Font:FONT_HELVETICA_NEUE_THIN
                                                             Size:53
                                                            Color:[UIColor TEXT_DARK_GRAY]
                                                        Alignment:NSTextAlignmentLeft
                                                             Text:@"°C"];
        
        self.lblLivingRoom = [[UILabel alloc] initWithFrame:CGRectMake(lblLivingRoomX, lblLivingRoomY, lblLivingRoomW, lblLivingRoomH)
                                                       Font:FONT_DB_HELVETICA_LI_COND
                                                       Size:16
                                                      Color:[UIColor TEXT_LIGHT_GRAY]
                                                  Alignment:NSTextAlignmentLeft
                                                       Text:@"LIVING ROOM"];
        
        self.lblLivingRoomTempValue = [[UILabel alloc] initWithFrame:CGRectMake(lblLivingRoomTempValueX, lblLivingRoomTempValueY, lblLivingRoomTempValueW, lblLivingRoomTempValueH)
                                                                Font:FONT_HELVETICA_NEUE_LIGHT
                                                                Size:25
                                                               Color:[UIColor TEXT_GRAY]
                                                           Alignment:NSTextAlignmentLeft
                                                                Text:BLANK];
        
        self.lblHall = [[UILabel alloc] initWithFrame:CGRectMake(lblHallX, lblHallY, lblHallW, lblHallH)
                                                 Font:FONT_DB_HELVETICA_LI_COND
                                                 Size:16
                                                Color:[UIColor TEXT_LIGHT_GRAY]
                                            Alignment:NSTextAlignmentLeft
                                                 Text:@"HALL"];
        
        self.lblHallTempValue = [[UILabel alloc] initWithFrame:CGRectMake(lblHallTempValueX, lblHallTempValueY, lblHallTempValueW, lblHallTempValueH)
                                                          Font:FONT_HELVETICA_NEUE_LIGHT
                                                          Size:25
                                                         Color:[UIColor TEXT_GRAY]
                                                     Alignment:NSTextAlignmentLeft
                                                          Text:BLANK];
        
        self.lblAttic = [[UILabel alloc] initWithFrame:CGRectMake(lblAtticX, lblAtticY, lblAtticW, lblAtticH)
                                                  Font:FONT_DB_HELVETICA_LI_COND
                                                  Size:16
                                                 Color:[UIColor TEXT_LIGHT_GRAY]
                                             Alignment:NSTextAlignmentCenter
                                                  Text:@"ATTIC"];
        
        self.lblAtticTempValue = [[UILabel alloc] initWithFrame:CGRectMake(lblAtticTempValueX, lblAtticTempValueY, lblAtticTempValueW, lblAtticTempValueH)
                                                           Font:FONT_HELVETICA_NEUE_LIGHT
                                                           Size:25
                                                          Color:[UIColor TEXT_GRAY]
                                                      Alignment:NSTextAlignmentCenter
                                                           Text:BLANK];
        
        self.imvPowerSource = [[UIImageView alloc] initWithFrame:CGRectMake(imvPowerSourceX, imvPowerSourceY, imvPowerSourceW, imvPowerSourceH)];
        [self.imvPowerSource setImage:[[AppsImageSet IPAD_POWER_SOURCE_IMAGE_SET] objectAtIndex:AAF_POWER_SOURCE_GRID]];
        
        self.imvPowerSourceBar = [[UIImageView alloc] initWithFrame:CGRectMake(imvPowerSourceBarX, imvPowerSourceBarY, imvPowerSourceBarW, imvPowerSourceBarH)];
        [self.imvPowerSourceBar setImage:[[AppsImageSet IPAD_POWER_SOURCE_BAR_IMAGE_SET] objectAtIndex:AAF_SOLAR_LEVEL_1]];
        
        /* Add subviews */
        [self addSubview:self.lblIndoor];
        [self addSubview:self.lblOutdoor];
        [self addSubview:self.lblIndoorTempValue];
        [self addSubview:self.lblOutdoorTempValue];
        
        [self addSubview:self.lblLivingRoom];
        [self addSubview:self.lblLivingRoomTempValue];
        [self addSubview:self.lblHall];
        [self addSubview:self.lblHallTempValue];
        [self addSubview:self.lblAttic];
        [self addSubview:self.lblAtticTempValue];
        
        [self addSubview:self.imvPowerSource];
        [self addSubview:self.imvPowerSourceBar];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end

@implementation ActiveAirflowBackgroundView (ActiveAirflowBgViewEX)

- (void)displayBgHouseWithHouseStatus:(AAF_HOUSE_STATUS)houseStatus {
    
    @try {
        if (houseStatus == AAF_HOUSE_STATUS_OFF) {
            [self.imvBgHouse setImage:[UIImage imageNamed:PIC_BG_HOUSE_OFF]];
        } else if (houseStatus == AAF_HOUSE_STATUS_COOL) {
            [self.imvBgHouse setImage:[UIImage imageNamed:PIC_BG_HOUSE_COOL]];
        } else {
            [self.imvBgHouse setImage:[UIImage imageNamed:PIC_BG_HOUSE_ON]];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)displayBgLandscapeWithFade:(BOOL)fadeFlag {
    
    @try {
        if (fadeFlag) {
            [self.imvBgLandscape setImage:[UIImage imageNamed:PIC_BG_LANDSCAPE_FADE]];
        } else {
            [self.imvBgLandscape setImage:[UIImage imageNamed:PIC_BG_LANDSCAPE]];
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)displayAnWindWithAnimation:(BOOL)animationFlag {
    
    @try {
        if (animationFlag) {
            if (![self.imvAnWind isAnimating]) [self.imvAnWind startAnimating];
            [self.imvAnWind setHidden:NO];
        } else {
            if ([self.imvAnWind isAnimating]) [self.imvAnWind stopAnimating];
            [self.imvAnWind setHidden:YES];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)displayAnCeilingWithAnimation:(BOOL)animationFlag {
    
    @try {
        if (animationFlag) {
            [self.imvAnCeiling startAnimating];
        } else {
            [self.imvAnCeiling stopAnimating];
            [self.imvAnCeiling setImage:[[AppsImageSet IPAD_CV_IMAGE_SET] objectAtIndex:1]];
        }
        [self.imvAnCeiling setHidden:NO];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)displayAnHeat {
    
    @try {
        NSDate *now = [NSDate date];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:GregorianCalendar];
        NSCalendarUnit units = HourCalendarUnit;
        NSDateComponents *components = [calendar components:units fromDate:now];
        
        NSInteger currentHour = [components hour];
        if (currentHour > 5 && currentHour < 17) {
            if (![self.imvAnHeat isAnimating]) [self.imvAnHeat startAnimating];
            [self.imvAnHeat setHidden:NO];
        } else {
            if ([self.imvAnHeat isAnimating]) [self.imvAnHeat stopAnimating];
            [self.imvAnHeat setHidden:YES];
        }
        
    } @finally {
        return;
    }
}

- (void)displayTemperatureColor {
    
    @try {
        NSString *indoorTempValueStr = [self.lblIndoorTempValue.text stringByReplacingOccurrencesOfString:@"°C" withString:BLANK];
        NSString *outdoorTempValueStr = [self.lblOutdoorTempValue.text stringByReplacingOccurrencesOfString:@"°C" withString:BLANK];
        
        if ([Common isNumeric:indoorTempValueStr] && [Common isNumeric:outdoorTempValueStr]) {
            
            NSInteger indoorTemp = [indoorTempValueStr integerValue];
            NSInteger outdoorTemp = [outdoorTempValueStr integerValue];
            
            if (indoorTemp < outdoorTemp) {
                [self.lblIndoorTempValue setTextColor:[UIColor TEXT_AAF_TEMP_OUT_OVER_IN]];
                [self displayBgHouseWithHouseStatus:AAF_HOUSE_STATUS_COOL];
            } else if (indoorTemp == outdoorTemp) {
                [self.lblIndoorTempValue setTextColor:[UIColor TEXT_AAF_TEMP_IN_EQUAL_OUT]];
                [self displayBgHouseWithHouseStatus:AAF_HOUSE_STATUS_ON];
            } else {
                [self.lblIndoorTempValue setTextColor:[UIColor TEXT_AAF_TEMP_IN_OVER_OUT]];
                [self displayBgHouseWithHouseStatus:AAF_HOUSE_STATUS_HOT];
            }
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)setIndoorTempValue:(NSNumber *)indoorTempValue {
    
    @try {
        [self.lblIndoorTempValue setText:[NSString stringWithFormat:@"%d°C", [indoorTempValue integerValue]]];
        [self displayTemperatureColor];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)setOutdoorTempValue:(NSNumber *)outdoorTempValue {
    
    @try {
        [self.lblOutdoorTempValue setText:[NSString stringWithFormat:@"%d°C", [outdoorTempValue integerValue]]];
        [self displayTemperatureColor];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)setLivingRoomTempValue:(NSNumber *)livingRoomTempValue {
    
    @try {
        [self.lblLivingRoomTempValue setText:[NSString stringWithFormat:@"%d°C", [livingRoomTempValue integerValue]]];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)setHallTempValue:(NSNumber *)hallTempValue {
    
    @try {
        [self.lblHallTempValue setText:[NSString stringWithFormat:@"%d°C", [hallTempValue integerValue]]];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)setAtticTempValue:(NSNumber *)atticTempValue {
 
    @try {
        [self.lblAtticTempValue setText:[NSString stringWithFormat:@"%d°C", [atticTempValue integerValue]]];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)setPowerSource:(AAF_POWER_SOURCE)source {
    
    @try {
        [self.imvPowerSource setImage:[[AppsImageSet IPAD_POWER_SOURCE_IMAGE_SET] objectAtIndex:source]];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)setPowerSourceBar:(AAF_SOLAR_LEVEL)level {
    
    @try {
        [self.imvPowerSourceBar setImage:[[AppsImageSet IPAD_POWER_SOURCE_BAR_IMAGE_SET] objectAtIndex:level]];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
