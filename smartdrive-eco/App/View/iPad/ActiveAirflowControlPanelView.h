//
//  ActiveAirflowSlidePanelView.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/28/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../Common/Common-Util.h"
#import "../../Common/AppsImageSet.h"
#import "./ActiveAirflowMenuView.h"

@interface ActiveAirflowControlPanelView : UIView

@property (nonatomic, strong) NSString *subsysID;

@property (nonatomic, strong) ActiveAirflowMenuView *aafMenuView;
@property (nonatomic, strong) UIButton *btnSlider;

@end

@interface ActiveAirflowControlPanelView (ActiveAirflowControlPanelViewEX)

typedef NS_ENUM(NSInteger, SLIDER_STATUS) {
    SLIDER_CLOSE,
    SLIDER_OPEN
};

- (void)flipSliderWithStatus:(SLIDER_STATUS)status;

@end