//
//  ActiveAirflowSlidePanelView.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/28/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ActiveAirflowControlPanelView.h"

@implementation ActiveAirflowControlPanelView

- (instancetype)initWithFrame:(CGRect)frame {
    
    @try {
        self = [super initWithFrame:frame];
        if (self) {
            
            [self initSubview];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initSubview {
    
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat frameW = self.frame.size.width;
    CGFloat frameH = self.frame.size.height;
    CGFloat statusBarH = [UIApplication sharedApplication].statusBarFrame.size.height;
    
    CGFloat aafMenuX, aafMenuY, aafMenuW, aafMenuH;
    CGFloat btnSliderX, btnSliderY, btnSliderW, btnSliderH;
    
    @try {
        /* Set background color */
        [self setBackgroundColor:[UIColor colorWithRed:0.137 green:0.121 blue:0.125 alpha:0.1]];
        
        /* Component calibration */
        btnSliderW = screenW * 0.0302f;
        btnSliderH = frameH - statusBarH;
        btnSliderX = frameW - btnSliderW;
        btnSliderY = statusBarH;
        
        aafMenuW = frameW - btnSliderW;
        aafMenuH = btnSliderH;
        aafMenuX = 0;
        aafMenuY = statusBarH;
        
        /* Initial component */
        self.aafMenuView = [[ActiveAirflowMenuView alloc] initWithFrame:CGRectMake(aafMenuX, aafMenuY, aafMenuW, aafMenuH)];
        [self.aafMenuView setSubsysID:self.subsysID];
        
        self.btnSlider = [[UIButton alloc] initWithFrame:CGRectMake(btnSliderX, btnSliderY, btnSliderW, btnSliderH)];
        [self.btnSlider setImage:[[AppsImageSet IPAD_MENU_SLIDE_SIGN_IMAGE_SET] objectAtIndex:1] forState:UIControlStateNormal];
        [self.btnSlider setBackgroundColor:[UIColor BG_MENU]];
        
        /* Add subview */
        [self addSubview:self.aafMenuView];
        [self addSubview:self.btnSlider];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end

@implementation ActiveAirflowControlPanelView (ActiveAirflowControlPanelViewEX)

- (void)flipSliderWithStatus:(SLIDER_STATUS)status {
    
    @try {
        if (status == SLIDER_CLOSE) {
            [self.btnSlider setImage:[[AppsImageSet IPAD_MENU_SLIDE_SIGN_IMAGE_SET] objectAtIndex:SLIDER_CLOSE]
                            forState:UIControlStateNormal];
        } else {
            [self.btnSlider setImage:[[AppsImageSet IPAD_MENU_SLIDE_SIGN_IMAGE_SET] objectAtIndex:SLIDER_OPEN]
                            forState:UIControlStateNormal];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end