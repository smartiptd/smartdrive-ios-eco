//
//  ActiveAirflowMenuControlCell.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/29/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../Common/Common-Util.h"
#import "../../Common/AppsImageSet.h"

typedef NS_ENUM(NSInteger, AAF_FAN_STATUS) {
    AAF_FAN_STATUS_OFF,
    AAF_FAN_STATUS_ON
};

@interface ActiveAirflowMenuControlCell : UITableViewCell

@property (nonatomic, strong) NSString *subsysID;
@property (nonatomic) AAF_FAN_STATUS currentStatus;

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *imvFan;
@property (nonatomic, strong) UILabel *lblName;
@property (nonatomic, strong) UILabel *lblTemp;
@property (nonatomic, strong) UIButton *btnStatus;

@end

@interface ActiveAirflowMenuControlCell (ActiveAirflowMenuControlCellEX)

- (void)displayCellWithFanName:(NSString *)name
                        Status:(AAF_FAN_STATUS)status
                        Hidden:(BOOL)hiddenFlag;
- (IBAction)performChangeStatus:(id)sender;

@end
