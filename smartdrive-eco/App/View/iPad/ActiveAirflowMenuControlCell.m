//
//  ActiveAirflowMenuControlCell.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/29/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ActiveAirflowMenuControlCell.h"

@implementation ActiveAirflowMenuControlCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    @try {
        self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
        if (self) {
            [self initSubview];
            [self displayCellWithFanName:@"ventilator" Status:AAF_FAN_STATUS_OFF Hidden:YES];
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initSubview {
    
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    
    CGFloat cellW, cellH;
    CGFloat bgViewH;
    CGFloat imvFanX, imvFanY, imvFanW, imvFanH;
    CGFloat lblNameX, lblNameY, lblNameW, lblNameH;
    CGFloat lblTempX, lblTempY, lblTempW, lblTempH;
    CGFloat btnStatusX, btnStatusY, btnStatusW, btnStatusH;
    
    @try {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        /* Set background color */
        [self setBackgroundColor:[UIColor clearColor]];
        
        /* Component calibration */
        cellW = screenW * 0.2587f;
        cellH = screenH * 0.1380f;
        
        bgViewH = cellH - (screenH * 0.0016f);
        
        imvFanW = screenW * 0.0576f;
        imvFanH = screenH * 0.0768f;
        imvFanX = screenW * 0.0146f;
        imvFanY = (bgViewH - imvFanH) / 2;
        
        lblNameW = screenW * 0.1328f;
        lblNameH = screenH * 0.0260f;
        lblNameX = screenW * 0.0871f;
        lblNameY = screenH * 0.0390f;
        
        lblTempW = lblNameW;
        lblTempH = screenH * 0.0260f;
        lblTempX = lblNameX;
        lblTempY = lblNameY + lblNameH;
        
        btnStatusW = screenW * 0.0361f;
        btnStatusH = screenH * 0.0481f;
        btnStatusX = screenW * 0.2099f;
        btnStatusY = screenH * 0.0292f;
        
        /* Initial component */
        self.bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cellW, bgViewH)];
        [self.bgView setBackgroundColor:[UIColor AAF_MENU_CONTROL_GRAY]];
        
        self.imvFan = [[UIImageView alloc] initWithFrame:CGRectMake(imvFanX, imvFanY, imvFanW, imvFanH)];
        [self.imvFan setAnimationImages:[AppsImageSet IPAD_FAN_IMAGE_SET]];
        [self.imvFan setAnimationDuration:0.6];
        [self.imvFan setAnimationRepeatCount:0];
        [self.imvFan stopAnimating];
        [self.imvFan setImage:[[AppsImageSet IPAD_FAN_IMAGE_SET] objectAtIndex:1]];
        
        self.lblName = [[UILabel alloc] initWithFrame:CGRectMake(lblNameX, lblNameY, lblNameW, lblNameH)
                                                 Font:FONT_DB_HELVETICA_BD_COND
                                                 Size:30
                                                Color:[UIColor TEXT_WHITE]
                                            Alignment:NSTextAlignmentLeft
                                                 Text:@"VENTILATOR"];
        
        self.lblTemp = [[UILabel alloc] initWithFrame:CGRectMake(lblTempX, lblTempY, lblTempW, lblTempH)
                                                 Font:FONT_HELVETICA_NEUE_LIGHT
                                                 Size:16
                                                Color:[UIColor TEXT_WHITE]
                                            Alignment:NSTextAlignmentLeft
                                                 Text:BLANK];
        
        self.btnStatus = [[UIButton alloc] initWithFrame:CGRectMake(btnStatusX, btnStatusY, btnStatusW, btnStatusH)];
        [self.btnStatus setImage:[[AppsImageSet IPAD_FAN_STATUS_IMAGE_SET] objectAtIndex:0]
                        forState:UIControlStateNormal];
        [self.btnStatus setHidden:NO];
        [self.btnStatus addTarget:self action:@selector(performChangeStatus:) forControlEvents:UIControlEventTouchUpInside];
        
        /* Add subviews */
        [self addSubview:self.bgView];
        [self addSubview:self.imvFan];
        [self addSubview:self.lblName];
        [self addSubview:self.lblTemp];
        [self addSubview:self.btnStatus];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end

@implementation ActiveAirflowMenuControlCell (ActiveAirflowMenuControlCellEX)

- (void)displayCellWithFanName:(NSString *)name
                        Status:(AAF_FAN_STATUS)status
                        Hidden:(BOOL)hiddenFlag {
    
    @try {
        self.currentStatus = status;
        [self.lblName setText:[name uppercaseString]];
        
        if (status == AAF_FAN_STATUS_OFF) {
            
            [self.imvFan stopAnimating];
            [self.imvFan setImage:[[AppsImageSet IPAD_FAN_IMAGE_SET] objectAtIndex:1]];
            
            [self.btnStatus setImage:[[AppsImageSet IPAD_FAN_STATUS_IMAGE_SET] objectAtIndex:0]
                            forState:UIControlStateNormal];
            
        } else if (status == AAF_FAN_STATUS_ON) {
            
            [self.imvFan startAnimating];
            
            [self.btnStatus setImage:[[AppsImageSet IPAD_FAN_STATUS_IMAGE_SET] objectAtIndex:1]
                            forState:UIControlStateNormal];
        }
        
        [self.btnStatus setHidden:hiddenFlag];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performChangeStatus:(id)sender {
    
    @try {
        self.currentStatus = (self.currentStatus == AAF_FAN_STATUS_OFF) ? AAF_FAN_STATUS_ON : AAF_FAN_STATUS_OFF;
        [self displayCellWithFanName:self.lblName.text Status:self.currentStatus Hidden:[self.btnStatus isHidden]];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
