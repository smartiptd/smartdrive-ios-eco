//
//  ActiveAirflowMenuHomeCell.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/29/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../Common/Common-Util.h"

@interface ActiveAirflowMenuHomeCell : UITableViewCell

@property (nonatomic, strong) UIButton *btnBack;

@end

@interface ActiveAirflowMenuHomeCell (ActiveAirflowMenuHomeCellEX)

- (IBAction)performBack:(id)sender;

@end
