//
//  ActiveAirflowMenuHomeCell.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/29/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ActiveAirflowMenuHomeCell.h"

static NSString * const SEGUE_HOME = @"segueHome";

@implementation ActiveAirflowMenuHomeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    @try {
        self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
        if (self) {
            [self initSubview];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initSubview {
    
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    
    CGFloat btnBackW, btnBackH;
    
    @try {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        /* Set background color */
        [self setBackgroundColor:[UIColor clearColor]];
        
        /* Component calibration */
        btnBackW = screenW * 0.2587f;
        btnBackH = screenH * 0.0690f;
        
        /* Initial component */
        self.btnBack = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnBackW, btnBackH)];
        [self.btnBack setTitle:@"BACK" forState:UIControlStateNormal];
        [self.btnBack.titleLabel setFont:[UIFont fontWithName:FONT_DB_HELVETICA_BD_COND size:30]];
        [self.btnBack.titleLabel setTextColor:[UIColor TEXT_WHITE]];
        [self.btnBack setBackgroundColor:[UIColor AAF_MENU_BUTTON_DARK_GRAY]];
        [self.btnBack setBackgroundImage:[Common imageWithColor:[UIColor AAF_MENU_BUTTON__DARK_GRAY_HILIGHT]]
                                forState:UIControlStateHighlighted];
        [self.btnBack addTarget:self action:@selector(performBack:) forControlEvents:UIControlEventTouchUpInside];
        
        /* Add subviews */
        [self addSubview:self.btnBack];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end

@implementation ActiveAirflowMenuHomeCell (ActiveAirflowMenuHomeCellEX)

- (IBAction)performBack:(id)sender {
    
    @try {
        [[Common getTopController] performSegueWithIdentifier:SEGUE_HOME sender:self];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
