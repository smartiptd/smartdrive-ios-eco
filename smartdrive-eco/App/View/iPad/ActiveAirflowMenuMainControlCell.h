//
//  ActiveAirflowMenuControlHeaderCell.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/29/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../Common/Common-Util.h"
#import "../../Common/AppsImageSet.h"
#import "../../ViewModel/iPad/ActiveAirflowMenuMainControlCellVM.h"

typedef NS_ENUM(NSInteger, AAF_MANUAL_STATUS) {
    AAF_MANUAL_STATUS_OFF,
    AAF_MANUAL_STATUS_ON
};

@interface ActiveAirflowMenuMainControlCell : UITableViewCell

@property (nonatomic, strong) NSString *subsysID;
@property (nonatomic) AAF_MANUAL_STATUS currentStatus;
@property (nonatomic, strong) ActiveAirflowMenuMainControlCellVM *viewModel;

@property (nonatomic, strong) UILabel *lblSystemName;
@property (nonatomic, strong) UIButton *btnStatus;

@end

@interface ActiveAirflowMenuMainControlCell (ActiveAirflowMenuMainControlCellEX)

- (void)displayMainSystemWithStatus:(AAF_MANUAL_STATUS)status
                             Hidden:(BOOL) hiddenFlag;
- (IBAction)performChangeStatus:(id)sender;

@end
