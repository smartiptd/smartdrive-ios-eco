//
//  ActiveAirflowMenuControlHeaderCell.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/29/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ActiveAirflowMenuMainControlCell.h"

@implementation ActiveAirflowMenuMainControlCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    @try {
        self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
        if (self) {
            [self initViewModel];
            [self initSubview];
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initViewModel {
    
    @try {
        self.viewModel = [[ActiveAirflowMenuMainControlCellVM alloc] init];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initSubview {
    
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    
    CGFloat cellW, cellH;
    CGFloat lblSystemNameX, lblSystemNameW;
    CGFloat btnStatusX, btnStatusY, btnStatusW, btnStatusH;
    
    @try {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        /* Set background color */
        [self setBackgroundColor:[UIColor AAF_MENU_MAIN_CONTROL_GRAY]];
        
        /* Component calibration */
        cellW = screenW * 0.2587f;
        cellH = screenH * 0.0690f;
        
        lblSystemNameW = cellW - (screenW * 0.0976f);
        lblSystemNameX = screenW * 0.0292f;
        
        btnStatusW = screenW * 0.0361f;
        btnStatusH = screenH * 0.0481f;
        btnStatusX = screenW * 0.2099f;
        btnStatusY = (cellH - btnStatusH) / 2;
        
        /* Initial subview */
        self.lblSystemName = [[UILabel alloc] initWithFrame:CGRectMake(lblSystemNameX, 0, lblSystemNameW, cellH)
                                                       Font:FONT_DB_HELVETICA_BD_COND
                                                       Size:30 Color:[UIColor TEXT_WHITE]
                                                  Alignment:NSTextAlignmentLeft
                                                       Text:@"MAIN SYSTEM"];
        
        self.btnStatus = [[UIButton alloc] initWithFrame:CGRectMake(btnStatusX, btnStatusY, btnStatusW, btnStatusH)];
        [self.btnStatus setImage:[[AppsImageSet IPAD_FAN_STATUS_IMAGE_SET] objectAtIndex:0]
                        forState:UIControlStateNormal];
        [self.btnStatus addTarget:self action:@selector(performChangeStatus:) forControlEvents:UIControlEventTouchUpInside];
        
        /* Add subviews */
        [self addSubview:self.lblSystemName];
        [self addSubview:self.btnStatus];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end

@implementation ActiveAirflowMenuMainControlCell (ActiveAirflowMenuMainControlCellEX)

- (void)displayMainSystemWithStatus:(AAF_MANUAL_STATUS)status
                             Hidden:(BOOL) hiddenFlag {
    
    @try {
        self.currentStatus = status;
        if (self.currentStatus == AAF_MANUAL_STATUS_OFF) {
            
            [self.btnStatus setImage:[[AppsImageSet IPAD_FAN_STATUS_IMAGE_SET] objectAtIndex:AAF_MANUAL_STATUS_OFF]
                            forState:UIControlStateNormal];
            
        } else if (self.currentStatus == AAF_MANUAL_STATUS_ON) {
            
            [self.btnStatus setImage:[[AppsImageSet IPAD_FAN_STATUS_IMAGE_SET] objectAtIndex:AAF_MANUAL_STATUS_ON]
                            forState:UIControlStateNormal];
        }
        
        [self.btnStatus setHidden:hiddenFlag];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performChangeStatus:(id)sender {
    
    @try {
        self.currentStatus = (self.currentStatus == AAF_MANUAL_STATUS_OFF) ? AAF_MANUAL_STATUS_ON : AAF_MANUAL_STATUS_OFF;
        [self displayMainSystemWithStatus:self.currentStatus Hidden:[self.btnStatus isHidden]];
        
        /* Publish data */
        NSDictionary *payloadDic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.currentStatus], @"system_mode",nil];
        NSData *payloadData = [NSJSONSerialization dataWithJSONObject:payloadDic
                                                              options:NSJSONWritingPrettyPrinted
                                                                error:nil];
        
        NSString *subsysID = [NSString stringWithFormat:@"%@/control/system", self.viewModel.coreVM.selectedSubsysID];
        [self.viewModel.coreVM.communicationMgr publishWithTopic:subsysID
                                                         Payload:payloadData];
        
        if (self.viewModel.coreVM.subscriptionDataDic) {
            
            ActiveAirflowData *aafData = [self.viewModel.coreVM.subscriptionDataDic objectForKey:self.viewModel.coreVM.selectedSubsysID];
            if (aafData) {
                
                aafData.systemMode = (NSInteger)self.currentStatus;
                [self.viewModel.coreVM.subscriptionDataDic setObject:aafData forKey:self.viewModel.coreVM.selectedSubsysID];
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
