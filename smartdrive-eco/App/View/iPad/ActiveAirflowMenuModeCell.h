//
//  ActiveAirflowMenuModeCell.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/29/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../Common/Common-Util.h"
#import "../../DataModel/ActiveAirflowData.h"
#import "../../ViewModel/iPad/ActiveAirflowMenuModeCellVM.h"

typedef NS_ENUM(NSInteger, AAF_SELECTED_MODE) {
    AAF_SELECTED_MODE_MANUAL,
    AAF_SELECTED_MODE_ECO
};

@interface ActiveAirflowMenuModeCell : UITableViewCell

@property (nonatomic, strong) NSString *subsysID;
@property (nonatomic, strong) ActiveAirflowMenuModeCellVM *viewModel;

@property (nonatomic, strong) UIButton *btnManual;
@property (nonatomic, strong) UIButton *btnEco;

@end

@interface ActiveAirflowMenuModeCell (ActiveAirflowMenuModeCellEX)

- (void)displaySelectedMode:(AAF_SELECTED_MODE)mode;
- (IBAction)performManualMode:(id)sender;
- (IBAction)performEcoMode:(id)sender;

@end
