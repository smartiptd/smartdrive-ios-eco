//
//  ActiveAirflowMenuModeCell.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/29/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ActiveAirflowMenuModeCell.h"

@implementation ActiveAirflowMenuModeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    @try {
        self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
        if (self) {
            [self initViewModel];
            [self initSubview];
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initViewModel {
    
    @try {
        self.viewModel = [[ActiveAirflowMenuModeCellVM alloc] init];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initSubview {
    
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    
    CGFloat cellW, cellH, btnW, btnManualX, btnEcoX;
    
    @try {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        /* Set background color */
        [self setBackgroundColor:[UIColor clearColor]];
        
        /* Component calibration */
        cellW = screenW * 0.2587f;
        cellH = screenH * 0.1145f;
        
        btnW = (cellW - (screenW * 0.0048f)) / 2;
        btnManualX = 0;
        btnEcoX = cellW - btnW;
        
        /* Initial component */
        self.btnManual = [[UIButton alloc] initWithFrame:CGRectMake(btnManualX, 0, btnW, cellH)];
        [self.btnManual setTitle:@"MANUAL" forState:UIControlStateNormal];
        [self.btnManual.titleLabel setFont:[UIFont fontWithName:FONT_DB_HELVETICA_BD_COND size:30]];
        [self.btnManual.titleLabel setTextColor:[UIColor TEXT_WHITE]];
        [self.btnManual setBackgroundImage:[Common imageWithColor:[UIColor AAF_MENU_BUTTON_DARK_GRAY]]
                                  forState:UIControlStateNormal];
        [self.btnManual setBackgroundImage:[Common imageWithColor:[UIColor AAF_MENU_BUTTON__DARK_GRAY_HILIGHT]]
                                  forState:UIControlStateHighlighted];
        [self.btnManual addTarget:self action:@selector(performManualMode:) forControlEvents:UIControlEventTouchUpInside];
        
        self.btnEco = [[UIButton alloc] initWithFrame:CGRectMake(btnEcoX, 0, btnW, cellH)];
        [self.btnEco setTitle:@"ECO" forState:UIControlStateNormal];
        [self.btnEco.titleLabel setFont:[UIFont fontWithName:FONT_DB_HELVETICA_BD_COND size:30]];
        [self.btnEco.titleLabel setTextColor:[UIColor TEXT_WHITE]];
        [self.btnEco setBackgroundImage:[Common imageWithColor:[UIColor AAF_MENU_BUTTON_DARK_GRAY]]
                               forState:UIControlStateNormal];
        [self.btnEco setBackgroundImage:[Common imageWithColor:[UIColor AAF_MENU_BUTTON__DARK_GRAY_HILIGHT]]
                               forState:UIControlStateHighlighted];
        [self.btnEco addTarget:self action:@selector(performEcoMode:) forControlEvents:UIControlEventTouchUpInside];
        
        /* Add subview */
        [self addSubview:self.btnManual];
        [self addSubview:self.btnEco];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end

@implementation ActiveAirflowMenuModeCell (ActiveAirflowMenuModeCellEX)

- (void)displaySelectedMode:(AAF_SELECTED_MODE)mode {
    
    @try {
        if (mode == AAF_SELECTED_MODE_MANUAL) {
            
            [self.btnManual setBackgroundImage:[Common imageWithColor:[UIColor LIVINGTECH_RED]] forState:UIControlStateNormal];
            [self.btnEco setBackgroundImage:[Common imageWithColor:[UIColor AAF_MENU_BUTTON_DARK_GRAY]] forState:UIControlStateNormal];
            
        } else if (mode == AAF_SELECTED_MODE_ECO) {
            
            [self.btnManual setBackgroundImage:[Common imageWithColor:[UIColor AAF_MENU_BUTTON_DARK_GRAY]] forState:UIControlStateNormal];
            [self.btnEco setBackgroundImage:[Common imageWithColor:[UIColor LIVINGTECH_RED]] forState:UIControlStateNormal];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performManualMode:(id)sender {
    
    @try {
        [self displaySelectedMode:AAF_SELECTED_MODE_MANUAL];
        
        /* Publish data */
        NSDictionary *payloadDic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:1], @"system_mode",nil];
        NSData *payloadData = [NSJSONSerialization dataWithJSONObject:payloadDic
                                                              options:NSJSONWritingPrettyPrinted
                                                                error:nil];
        
        NSString *subsysID = [NSString stringWithFormat:@"%@/control/system", self.viewModel.coreVM.selectedSubsysID];
        [self.viewModel.coreVM.communicationMgr publishWithTopic:subsysID
                                                         Payload:payloadData];
        
        if (self.viewModel.coreVM.subscriptionDataDic) {
            
            ActiveAirflowData *aafData = [self.viewModel.coreVM.subscriptionDataDic objectForKey:self.viewModel.coreVM.selectedSubsysID];
            if (aafData) {
                
                aafData.systemMode = AAF_SYSTEM_MODE_ON;
                [self.viewModel.coreVM.subscriptionDataDic setObject:aafData forKey:self.viewModel.coreVM.selectedSubsysID];
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performEcoMode:(id)sender {
    
    @try {
        [self displaySelectedMode:AAF_SELECTED_MODE_ECO];
        
        /* Publish data */
        NSDictionary *payloadDic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:2], @"system_mode",nil];
        NSData *payloadData = [NSJSONSerialization dataWithJSONObject:payloadDic
                                                              options:NSJSONWritingPrettyPrinted
                                                                error:nil];
        
        NSString *subsysID = [NSString stringWithFormat:@"%@/control/system", self.viewModel.coreVM.selectedSubsysID];
        [self.viewModel.coreVM.communicationMgr publishWithTopic:subsysID
                                                             Payload:payloadData];
        
        if (self.viewModel.coreVM.subscriptionDataDic) {
            
            ActiveAirflowData *aafData = [self.viewModel.coreVM.subscriptionDataDic objectForKey:self.viewModel.coreVM.selectedSubsysID];
            if (aafData) {
                
                aafData.systemMode = AAF_SYSTEM_MODE_ECO;
                [self.viewModel.coreVM.subscriptionDataDic setObject:aafData forKey:self.viewModel.coreVM.selectedSubsysID];
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
