//
//  ActiveAirflowMenuView.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/29/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "./ActiveAirflowMenuModeCell.h"
#import "./ActiveAirflowMenuMainControlCell.h"
#import "./ActiveAirflowMenuControlCell.h"
#import "./ActiveAirflowMenuHomeCell.h"
#import "../../Common/Common-Util.h"
#import "../../DataModel/ActiveAirflowData.h"

@interface ActiveAirflowMenuView : UITableView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSString *subsysID;

@end

@interface ActiveAirflowMenuView (ActiveAirflowMenuViewEX)

- (void)setMenuWithSystemMode:(AAF_SYSTEM_MODE)mode;

@end
