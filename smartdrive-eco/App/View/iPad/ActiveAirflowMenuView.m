//
//  ActiveAirflowMenuView.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/29/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ActiveAirflowMenuView.h"

static NSString * const ID_MENU_MODE_CELL           = @"menuModeCell";
static NSString * const ID_MENU_MAIN_CONTROL_CELL   = @"menuMainControlCell";
static NSString * const ID_MENU_CONTROL_CELL        = @"menuControlCell";
static NSString * const ID_MENU_HOME_CELL           = @"menuHomeCell";

@interface ActiveAirflowMenuView ()

typedef NS_ENUM(NSInteger, AAF_SECTION_MENU) {
    AAF_SECTION_MENU_MODE,
    AAF_SECTION_MENU_CONTROL,
    AAF_SECTION_MENU_HOME,
    AAF_SECTION_MENU_SIZE
};

typedef NS_ENUM(NSInteger, AAF_ROW_MENU_SVT) {
    AAF_ROW_MENU_SVT_HEADER,
    AAF_ROW_MENU_SVT_CONTROL
};

@property (nonatomic) AAF_SYSTEM_MODE currentSystemMode;

@end

@implementation ActiveAirflowMenuView

- (instancetype)initWithFrame:(CGRect)frame {
    
    @try {
        self = [super initWithFrame:frame];
        if (self) {
            
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
    
    @try {
        /* Set background color */
        [self setBackgroundColor:[UIColor BG_MENU]];
        
        /* Initial UITableView */
        self.delegate = self;
        self.dataSource = self;
        
        [self setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self setTableFooterView:[UIView new]];
        [self setScrollEnabled:YES];
        
        /* Register cell identifier */
        [self registerClass:[ActiveAirflowMenuModeCell class] forCellReuseIdentifier:ID_MENU_MODE_CELL];
        [self registerClass:[ActiveAirflowMenuMainControlCell class] forCellReuseIdentifier:ID_MENU_MAIN_CONTROL_CELL];
        [self registerClass:[ActiveAirflowMenuControlCell class] forCellReuseIdentifier:ID_MENU_CONTROL_CELL];
        [self registerClass:[ActiveAirflowMenuHomeCell class] forCellReuseIdentifier:ID_MENU_HOME_CELL];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger numOfSec = 0;
    
    @try {
        numOfSec = AAF_SECTION_MENU_SIZE;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return numOfSec;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numOfRow = 0;
    
    @try {
        if (section == AAF_SECTION_MENU_MODE) {
            numOfRow = 1;
        } else if (section == AAF_SECTION_MENU_CONTROL) {
            if (self.currentSystemMode == AAF_SYSTEM_MODE_OFF) {
                numOfRow = 1;
            } else {
                numOfRow = 3;
            }
        } else if (section == AAF_SECTION_MENU_HOME) {
            numOfRow = 1;
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return numOfRow;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    @try {
        if (indexPath.section == AAF_SECTION_MENU_MODE) {
            
            ActiveAirflowMenuModeCell *menuModeCell = (ActiveAirflowMenuModeCell *)[self dequeueReusableCellWithIdentifier:ID_MENU_MODE_CELL];
            [menuModeCell setSubsysID:self.subsysID];
            cell = menuModeCell;
            
        } else if (indexPath.section == AAF_SECTION_MENU_CONTROL) {
            
            if (indexPath.row == AAF_ROW_MENU_SVT_HEADER) {
                
                ActiveAirflowMenuMainControlCell *menuMainControlCell = (ActiveAirflowMenuMainControlCell *)[self dequeueReusableCellWithIdentifier:ID_MENU_MAIN_CONTROL_CELL];
                [menuMainControlCell setSubsysID:self.subsysID];
                cell = menuMainControlCell;
                
            } else {
                
                ActiveAirflowMenuControlCell *menuControlCell = (ActiveAirflowMenuControlCell *)[self dequeueReusableCellWithIdentifier:ID_MENU_CONTROL_CELL];
                [menuControlCell setSubsysID:self.subsysID];
                
                if (indexPath.row == 1) {
                    [menuControlCell displayCellWithFanName:@"ATTIC" Status:AAF_FAN_STATUS_ON Hidden:YES];
                } else if (indexPath.row == 2) {
                    [menuControlCell displayCellWithFanName:@"CEILING" Status:AAF_FAN_STATUS_ON Hidden:YES];
                }
                
                cell = menuControlCell;
            }
            
        } else if (indexPath.section == AAF_SECTION_MENU_HOME) {
            
            cell = (ActiveAirflowMenuHomeCell *)[self dequeueReusableCellWithIdentifier:ID_MENU_HOME_CELL];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return cell;
    }
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    CGFloat height = 0;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    @try {
        height = screenH * 0.0065f;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return height;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = 0;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    @try {
        if (indexPath.section == AAF_SECTION_MENU_MODE) {
            
            height = screenH * 0.1145f;
            
        } else if (indexPath.section == AAF_SECTION_MENU_CONTROL) {
            
            if (indexPath.row == AAF_ROW_MENU_SVT_HEADER) {
                
                height = screenH * 0.0690f;
                
            } else {
                
                height = screenH * 0.1380f;
            }
            
        } else if (indexPath.section == AAF_SECTION_MENU_HOME) {
            
            height = screenH * 0.0690f;
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return height;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    @try {
        // Do nothing
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return [[UIView alloc] init];
    }
}

@end

@implementation ActiveAirflowMenuView (ActiveAirflowMenuViewEX)

- (void)setMenuWithSystemMode:(AAF_SYSTEM_MODE)mode {
    
    @try {
        self.currentSystemMode = mode;
        
        /* Update system mode cell */
        id modeCell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:AAF_SECTION_MENU_MODE]];
        if ([modeCell isKindOfClass:[ActiveAirflowMenuModeCell class]]) {
            modeCell = (ActiveAirflowMenuModeCell *)modeCell;
            if (mode == AAF_SYSTEM_MODE_OFF || mode == AAF_SYSTEM_MODE_ON) {
                [modeCell displaySelectedMode:AAF_SELECTED_MODE_MANUAL];
            } else if (mode == AAF_SYSTEM_MODE_ECO) {
                [modeCell displaySelectedMode:AAF_SELECTED_MODE_ECO];
            }
        }
        
        /* Update main control cell */
        id mainControlCell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:AAF_SECTION_MENU_CONTROL]];
        if ([mainControlCell isKindOfClass:[ActiveAirflowMenuMainControlCell class]]) {
            mainControlCell = (ActiveAirflowMenuMainControlCell *)mainControlCell;
            if (mode == AAF_SYSTEM_MODE_OFF) {
                [mainControlCell displayMainSystemWithStatus:AAF_MANUAL_STATUS_OFF Hidden:NO];
            } else if (mode == AAF_SYSTEM_MODE_ECO) {
                [mainControlCell displayMainSystemWithStatus:AAF_MANUAL_STATUS_OFF Hidden:YES];
            } else if (mode == AAF_SYSTEM_MODE_ON) {
                [mainControlCell displayMainSystemWithStatus:AAF_MANUAL_STATUS_ON Hidden:NO];
            }
        }
        
        [self reloadData];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
