//
//  ActiveAirflowVC.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/27/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../Common/Common-Util.h"
#import "../../ViewModel/iPad/ActiveAirflowVM.h"
#import "../../DataModel/ActiveAirflowData.h"
#import "./ActiveAirflowBackgroundView.h"
#import "./ActiveAirflowControlPanelView.h"

@interface ActiveAirflowVC : UIViewController

@property (nonatomic, strong) NSString *subsysID;
@property (nonatomic, strong) ActiveAirflowVM *viewModel;

@property (nonatomic, strong) UIView *slideControlView;
@property (nonatomic, strong) UIImageView *imvLogo;
@property (nonatomic, strong) ActiveAirflowBackgroundView *aafBgView;
@property (nonatomic, strong) ActiveAirflowControlPanelView *aafCpView;

@end

@interface ActiveAirflowVC (ActiveAirflowVCEX)

- (void)performSlideControlPanel;
- (void)performFlipSliderMenu;

@end