//
//  ActiveAirflowVC.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/27/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ActiveAirflowVC.h"

static NSString * const PIC_LOGO = @"logo.png";

@interface ActiveAirflowVC()

@property (nonatomic) CGFloat controlPanelW, controlPanelH;
@property (nonatomic) CGFloat beginTouch, endTouch, beginPoint, endPoint, currentX;
@property (nonatomic) CGFloat slideAreaWidth, slideBgWidth;

@end

@implementation ActiveAirflowVC

- (void)viewDidLoad {
    
    @try {
        [super viewDidLoad];
        [self initViewModel];
        [self initSubview];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)didReceiveMemoryWarning {
    
    @try {
        [super didReceiveMemoryWarning];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initViewModel {
    
    @try {
        self.viewModel = [[ActiveAirflowVM alloc] init];
        
        [self.viewModel.coreVM addObserver:self
                                forKeyPath:@"subscriptionDataDic"
                                   options:(NSKeyValueObservingOptionNew)
                                   context:NULL];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initSubview {
    
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    CGFloat statusBarH = [UIApplication sharedApplication].statusBarFrame.size.height;
    
    CGFloat imvLogoX, imvLogoY, imvLogoW, imvLogoH;
    
    @try {
        /* Initialize */
        _currentX = -1;
        _slideAreaWidth = screenW * 0.2587f;
        _slideBgWidth = screenW * 0.1142f;
        
        /* Set background color */
        [self.view setBackgroundColor:[UIColor BG_LIGHT_GRAY]];
        
        /* Component calibation */
        _controlPanelW = screenW * 0.2890f;
        _controlPanelH = screenH;
        
        imvLogoW = screenW * 0.1494f;
        imvLogoH = screenH * 0.1744f;
        imvLogoX = screenW - imvLogoW;
        imvLogoY = statusBarH;
        
        /* Initial UI component */
        self.slideControlView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenW, screenH)];
        
        self.aafBgView = [[ActiveAirflowBackgroundView alloc] initWithFrame:CGRectMake(0, 0, screenW, screenH)];
        
        self.aafCpView = [[ActiveAirflowControlPanelView alloc] initWithFrame:CGRectMake(-_controlPanelW + (screenW * 0.0302f), 0, _controlPanelW, _controlPanelH)];
        [self.aafCpView setSubsysID:self.subsysID];
        [self.viewModel.coreVM setSelectedSubsysID:self.subsysID];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(performFlipSliderMenu)];
        [self.aafCpView.btnSlider addGestureRecognizer:tap];
        
        self.imvLogo = [[UIImageView alloc] initWithFrame:CGRectMake(imvLogoX, imvLogoY, imvLogoW, imvLogoH)];
        [self.imvLogo setImage:[UIImage imageNamed:PIC_LOGO]];
        
        /* Add sub view */
        [self.view addSubview:self.slideControlView];
        [self.view addSubview:self.aafBgView];
        [self.view addSubview:self.aafCpView];
        [self.view addSubview:self.imvLogo];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - Event Methods
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    @try {
        UITouch *touch = [touches anyObject];
        CGPoint touchLocation = [touch locationInView:self.slideControlView];
        
        _beginTouch = touchLocation.x;
        _beginPoint = touchLocation.x;
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        return;
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    @try {
        UITouch *touch = [touches anyObject];
        CGPoint touchLocation = [touch locationInView:self.slideControlView];
        
        _endPoint = touchLocation.x;
        
        if(_endPoint > _beginPoint) {
            
            /* Move right */
            _currentX += (_endPoint - _beginPoint);
            if (_currentX > _slideAreaWidth) {
                _currentX = _slideAreaWidth;
            }
            
        } else {
            
            /* Move left */
            _currentX -= (_beginPoint - _endPoint);
            if (_currentX < 0) {
                _currentX = 0;
            }
        }
        
        [self performSelectorOnMainThread:@selector(performSlideControlPanel) withObject:nil waitUntilDone:NO];
        _beginPoint = _endPoint;
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        return;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    @try {
        UITouch *touch = [touches anyObject];
        CGPoint touchLocation = [touch locationInView:self.slideControlView];
        
        _endTouch = touchLocation.x;
        
        if (_endTouch > _beginTouch) {
            
            /* Move right */
            if ((_endTouch - _beginTouch) > (_slideAreaWidth / 3)) {
                _currentX = _slideAreaWidth;
            } else {
                _currentX = 0;
            }
            
        } else {
            
            /* Move left */
            if ((_beginTouch - _endTouch) > (_slideAreaWidth / 3)) {
                _currentX = 0;
            } else {
                _currentX = _slideAreaWidth;
            }
        }
        
        [self performSelectorOnMainThread:@selector(performSlideControlPanel) withObject:nil waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        return;
    }
}

@end

@implementation ActiveAirflowVC (ActiveAirflowVCEX)

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    @try {
        if ([keyPath isEqualToString:@"subscriptionDataDic"]) {
            if ([change objectForKey:@"new"]) {
                
                NSMutableDictionary *subscriptionDataDic = [change objectForKey:@"new"];
                if (subscriptionDataDic) {
                    
                    id aafData = [subscriptionDataDic objectForKey:self.subsysID];
                    if (aafData) {
                        
                        [self updateActiveAirflowViewWithData:aafData];
                    }
                }
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)performSlideControlPanel {
    
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    @try {
        [UIView animateWithDuration:0.2 animations:^{
            
            [self.aafCpView setFrame:CGRectMake(-_controlPanelW + _currentX + (screenW * 0.0302f), 0, _controlPanelW, _controlPanelH)];
            [self.aafBgView setFrame:CGRectMake((_currentX * _slideBgWidth) / _slideAreaWidth, 0, screenW, screenH)];
            
            [self performSelectorOnMainThread:@selector(updateSliderMenu) withObject:nil waitUntilDone:YES];
            [self performSelectorOnMainThread:@selector(updateBackgroundFading) withObject:nil waitUntilDone:YES];
        }];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)performFlipSliderMenu {
    
    @try {
        _currentX = (_currentX > 0) ? 0 : _slideAreaWidth;
        [self performSelectorOnMainThread:@selector(performSlideControlPanel) withObject:nil waitUntilDone:NO];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - Private Methods
- (void)updateSliderMenu {
    
    @try {
        if (_currentX > 0) {
            [self.aafCpView flipSliderWithStatus:SLIDER_CLOSE];
        } else {
            [self.aafCpView flipSliderWithStatus:SLIDER_OPEN];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)updateBackgroundFading {
    
    @try {
        if (_currentX > 0) {
            [self.aafBgView displayBgLandscapeWithFade:YES];
        } else {
            [self.aafBgView displayBgLandscapeWithFade:NO];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)updateActiveAirflowViewWithData:(ActiveAirflowData *)aafData {
    
    @try {
        if (aafData && self.aafBgView && self.aafCpView) {
            
            /* Update solar cell value */
            [self.aafBgView setPowerSource:aafData.powerSource];
            [self.aafBgView setPowerSourceBar:aafData.solarLevel];
            
            /* Update temp value */
            for (NSString *tempName in aafData.temperatureDic) {
                NSNumber *tempVal = [aafData.temperatureDic objectForKey:tempName];
                if ([tempName isEqualToString:@"ambient"]) {
                    [self.aafBgView setOutdoorTempValue:tempVal];
                } else if ([tempName isEqualToString:@"attic"]) {
                    [self.aafBgView setAtticTempValue:tempVal];
                } else if ([tempName isEqualToString:@"room"]) {
                    [self.aafBgView setIndoorTempValue:tempVal];
                    [self.aafBgView setLivingRoomTempValue:tempVal];
                }
            }
            
            /* Update system value */
            [self.aafCpView.aafMenuView setMenuWithSystemMode:aafData.systemMode];
            
            /* Update background status */
            if (aafData.systemMode == AAF_SYSTEM_MODE_OFF) {
                [self.aafBgView displayBgHouseWithHouseStatus:AAF_HOUSE_STATUS_OFF];
                [self.aafBgView displayAnWindWithAnimation:NO];
                [self.aafBgView displayAnCeilingWithAnimation:NO];
            } else {
                [self.aafBgView displayBgHouseWithHouseStatus:AAF_HOUSE_STATUS_ON];
                [self.aafBgView displayAnWindWithAnimation:YES];
                [self.aafBgView displayAnCeilingWithAnimation:YES];
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
