//
//  HomeProfileView.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/17/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "FontAwesomeKit.h"
#import "./HomeVC.h"
#import "../../Common/Common-Util.h"
#import "../../DataModel/AppSession.h"
#import "../../ViewModel/iPad/HomeProfileVM.h"

@interface HomeProfileView : UIView

@property (nonatomic, strong) HomeProfileVM *viewModel;

@property (nonatomic, strong) UIImageView *imvPhoto;
@property (nonatomic, strong) UILabel *lblEmail;
@property (nonatomic, strong) UILabel *lblFullname;
@property (nonatomic, strong) UIButton *btnLogout;

@end

@interface HomeProfileView (HomeProfileViewEX)

- (void)displayProfileWithEmail:(NSString *)email
                      GivenName:(NSString *)givenName
                     FamilyName:(NSString *)familyName
                       PhotoURL:(NSString *)photoURL;
- (IBAction)performLogout:(id)sender;

@end
