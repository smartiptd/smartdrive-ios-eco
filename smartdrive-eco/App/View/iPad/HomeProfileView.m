//
//  HomeProfileView.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/17/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "HomeProfileView.h"

static NSString * const DEFAULT_PHOTO_NAME = @"default-profile-photo.png";

@implementation HomeProfileView

- (instancetype)initWithFrame:(CGRect)frame
{
    @try {
        self = [super initWithFrame:frame];
        if (self) {
            [self initViewModel];
            [self initSubview];
        }
    
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initViewModel {
    
    @try {
        self.viewModel = [[HomeProfileVM alloc] init];
        
        [self.viewModel.coreVM addObserver:self
                                forKeyPath:@"session"
                                   options:(NSKeyValueObservingOptionNew)
                                   context:NULL];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initSubview {
    
    CGFloat frameW = self.frame.size.width;
    CGFloat frameH = self.frame.size.height;
    
    CGFloat imvPhotoX, imvPhotoY, imvPhotoW, imvPhotoH = 0;
    CGFloat lblEmailX, lblEmailY, lblEmailW, lblEmailH = 0;
    CGFloat lblFullnameX, lblFullnameY, lblFullnameW, lblFullnameH = 0;
    CGFloat btnLogoutX, btnLogoutY, btnLogoutW, btnLogoutH = 0;
    
    @try {
        /* Set background color */
        [self setBackgroundColor:[UIColor BG_MEDIUM_GRAY]];
        
        /* Component calibration */
        imvPhotoH = imvPhotoW = (frameH * 7) / 12;
        imvPhotoX = (frameW - imvPhotoW) / 2;
        imvPhotoY = frameH / 6;
        
        lblEmailW = lblFullnameW = frameW / 2;
        lblEmailH = lblFullnameH = frameH / 8;
        lblEmailX = lblFullnameX = frameW / 4;
        lblEmailY = imvPhotoY + imvPhotoH;
        lblFullnameY = lblEmailY + ((lblEmailH * 4) / 5);
        
        btnLogoutW = frameW / 14.7f;
        btnLogoutH = frameH / 6.4f;
        btnLogoutX = frameW - btnLogoutW;
        btnLogoutY = frameH - btnLogoutH;
        
        /* Initial UI component */
        self.imvPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(imvPhotoX, imvPhotoY, imvPhotoW, imvPhotoH)];
        [self.imvPhoto.layer setCornerRadius:imvPhotoW / 2.0f];
        [self.imvPhoto.layer setMasksToBounds:YES];
        [self.imvPhoto.layer setBorderWidth:1.0f];
        [self.imvPhoto.layer setBorderColor:[[UIColor BG_MEDIUM_GRAY] CGColor]];
        [self.imvPhoto setBackgroundColor:[UIColor BG_MEDIUM_GRAY]];
        
        self.lblEmail = [[UILabel alloc] initWithFrame:CGRectMake(lblEmailX, lblEmailY, lblEmailW, lblEmailH)
                                                  Font:FONT_HELVETICA_NEUE_LIGHT
                                                  Size:18
                                                 Color:[UIColor whiteColor]
                                             Alignment:NSTextAlignmentCenter
                                                  Text:BLANK];
        
        self.lblFullname = [[UILabel alloc] initWithFrame:CGRectMake(lblFullnameX, lblFullnameY, lblFullnameW, lblFullnameH)
                                                     Font:FONT_HELVETICA_REGULAR
                                                     Size:20
                                                    Color:[UIColor whiteColor]
                                                Alignment:NSTextAlignmentCenter
                                                     Text:BLANK];
        
        self.btnLogout = [[UIButton alloc] initWithFrame:CGRectMake(btnLogoutX, btnLogoutY, btnLogoutW, btnLogoutH)
                                                   Title:@"Logout"
                                                    Font:FONT_HELVETICA_REGULAR
                                                    Size:16
                                              TitleColor:[UIColor whiteColor]
                                       TitleHilightColor:[UIColor BG_DARK_GRAY]
                                          UnderlineStyle:NSUnderlineStyleSingle
                                                   Color:[UIColor clearColor]
                                            HilightColor:[UIColor clearColor]
                                             BorderColor:[UIColor clearColor]
                                             BorderWidth:0
                                            BorderRadius:0];
        [self.btnLogout addTarget:self action:@selector(performLogout:) forControlEvents:UIControlEventTouchUpInside];
        
        /* Add subview */
        [self addSubview:self.imvPhoto];
        [self addSubview:self.lblEmail];
        [self addSubview:self.lblFullname];
        [self addSubview:self.btnLogout];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)drawRect:(CGRect)rect {
    
    @try {
        AppSession *curSession = [self.viewModel.coreVM.sessionMgr getCurrentSession];
        if (curSession) {
            
            [self displayProfileWithEmail:curSession.email
                                GivenName:curSession.givenName
                               FamilyName:curSession.familyName
                                 PhotoURL:curSession.photoURL];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end

@implementation HomeProfileView (HomeProfileViewEX)

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    @try {
        if ([keyPath isEqualToString:@"session"]) {
            
            if ([change objectForKey:@"new"] && ![[change objectForKey:@"new"] isEqual:[NSNull null]]) {
                
                AppSession *newSession = (AppSession *)[change objectForKey:@"new"];
                [self displayProfileWithEmail:newSession.email
                                    GivenName:newSession.givenName
                                   FamilyName:newSession.familyName
                                     PhotoURL:newSession.photoURL];
            } else {
                
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)displayProfileWithEmail:(NSString *)email
                      GivenName:(NSString *)givenName
                     FamilyName:(NSString *)familyName
                       PhotoURL:(NSString *)photoURL {
    
    @try {
        /* Set display profile photo */
        NSData *photoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:photoURL]];
        if (photoData) {
            self.imvPhoto.image = [UIImage imageWithData:photoData];
        } else {
            self.imvPhoto.image = [UIImage imageNamed:DEFAULT_PHOTO_NAME];
        }
        
        /* Set display proflie email & name */
        if (email && givenName && familyName) {
            
            [email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [givenName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [familyName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            [self.lblEmail setText:[email lowercaseString]];
            [self.lblFullname setText:[[NSString stringWithFormat:@"%@ %@", givenName, familyName] capitalizedString]];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performLogout:(id)sender {
    
    @try {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Logout"
                                                                       message:@"Are you sure you want to logout?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *yesButton = [UIAlertAction actionWithTitle:@"YES"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        [self.viewModel logout];
                                    }];
        UIAlertAction *noButton = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [[Common getTopController] presentViewController:alert animated:YES completion:nil];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
