//
//  HomeSubsysCell.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/17/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "../../Common/Common-Util.h"

@interface HomeSubsysCell : UICollectionViewCell

typedef NS_ENUM(NSInteger, HOME_MENU_CELL_TYPE) {
    HOME_MENU_CELL_TYPE_NONE,
    HOME_MENU_CELL_TYPE_ADD,
    HOME_MENU_CELL_TYPE_AAF
};

@property (nonatomic, strong) NSString *subsysID;
@property (nonatomic, strong) NSString *subsysType;
@property (nonatomic, strong) NSString *subsysDescription;

@property (nonatomic, strong) UIImageView *imvAdd;
@property (nonatomic, strong) UIImageView *imvIcon;
@property (nonatomic, strong) UILabel *lblSystemName;
@property (nonatomic, strong) UILabel *lblDescription;

@end

@interface HomeSubsysCell (HomeSubsysCellEX)

- (void)displayAddSubsysCell;
- (void)displayActiveAirflowCellWithSubsysID:(NSString *)subsysID
                                  SubsysType:(NSString *)subsysType
                           SubsysDescription:(NSString *)description;
- (void)displayComponentWithCellType:(HOME_MENU_CELL_TYPE)cellType;

@end
