//
//  HomeSubsysCell.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/17/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "HomeSubsysCell.h"

static NSString * const PIC_ADD_SUBSYS_ICON = @"ipad-add-subsys-icon.png";
static NSString * const PIC_AAF_ICON        = @"ipad-aaf-icon.png";

@implementation HomeSubsysCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    @try {
        self = [super initWithFrame:frame];
        if (self) {
            [self initSubview];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)drawRect:(CGRect)rect {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initSubview {
    
    CGFloat cellW = self.bounds.size.width;
    CGFloat cellH = self.bounds.size.height;
    
    CGFloat imvAddX, imvAddY, imvAddW, imvAddH;
    CGFloat imvIconX, imvIconY, imvIconW, imvIconH;
    CGFloat lblSystemNameX, lblSystemNameY, lblSystemNameW, lblSystemNameH;
    CGFloat lblDescriptionX, lblDescriptionY, lblDescriptionW, lblDescriptionH;
    
    @try {
        /* Setup cell */
        [self setBackgroundColor:[UIColor HOME_MENU_LIGHT_GRAY]];
        [self.layer setBorderColor:[UIColor HOME_MENU_MEDIUM_GRAY].CGColor];
        [self.layer setBorderWidth:1.0f];
        [self.layer setCornerRadius:0];
        [self.layer setMasksToBounds:NO];
        [self.layer setShadowOffset:CGSizeZero];
        [self.layer setShadowColor:[UIColor HOME_MENU_DARK_GRAY].CGColor];
        [self.layer setShadowOpacity:0.5f];
        
        /* Component calibration */
        imvIconW = cellW / 3;
        imvIconH = cellH;
        imvIconX = imvIconY = 0;
        
        imvAddH = imvAddW = (cellH * 5) / 6;
        imvAddX = (cellW - imvAddW) / 2;
        imvAddY = (cellH - imvAddH) / 2;
        
        lblSystemNameW = ((cellW * 2) / 3) * 0.9f;
        lblSystemNameH = cellH / 8;
        lblSystemNameX = imvIconW + (((cellW * 2) / 3) - lblSystemNameW) / 2;
        lblSystemNameY = lblSystemNameH;
        
        lblDescriptionW = lblSystemNameW;
        lblDescriptionH = lblSystemNameH;
        lblDescriptionX = lblSystemNameX;
        lblDescriptionY = lblSystemNameH * 2;
        
        /* Initial UI component */
        self.imvIcon = [[UIImageView alloc] initWithFrame:CGRectMake(imvIconX, imvIconY, imvIconW, imvIconH)];
        
        self.imvAdd = [[UIImageView alloc] initWithFrame:CGRectMake(imvAddX, imvAddY, imvAddW, imvAddH)];

        self.lblSystemName = [[UILabel alloc] initWithFrame:CGRectMake(lblSystemNameX, lblSystemNameY, lblSystemNameW, lblSystemNameH)
                                                       Font:FONT_HELVETICA_LIGHT
                                                       Size:18
                                                      Color:[UIColor TEXT_MEDIUM_GRAY]
                                                  Alignment:NSTextAlignmentLeft
                                                       Text:BLANK];
        
        self.lblDescription = [[UILabel alloc] initWithFrame:CGRectMake(lblDescriptionX, lblDescriptionY, lblDescriptionW, lblDescriptionH)
                                                        Font:FONT_HELVETICA_LIGHT
                                                        Size:14
                                                       Color:[UIColor HOME_MENU_DARK_GRAY]
                                                   Alignment:NSTextAlignmentLeft
                                                        Text:BLANK];
        
        [self addSubview:self.imvAdd];
        [self addSubview:self.imvIcon];
        [self addSubview:self.lblSystemName];
        [self addSubview:self.lblDescription];
        
        [self displayComponentWithCellType:HOME_MENU_CELL_TYPE_NONE];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end

@implementation HomeSubsysCell (HomeSubsysCellEX)

- (void)displayAddSubsysCell {
    
    @try {
        self.subsysID = BLANK;
        self.subsysType = BLANK;
        self.subsysDescription = BLANK;
        
        [self.imvAdd setImage:[UIImage imageNamed:PIC_ADD_SUBSYS_ICON]];
        [self.imvAdd setImage:[self.imvAdd.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        [self.imvAdd setTintColor:[UIColor HOME_MENU_MEDIUM_GRAY]];
        
        [self displayComponentWithCellType:HOME_MENU_CELL_TYPE_ADD];

    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)displayActiveAirflowCellWithSubsysID:(NSString *)subsysID
                                  SubsysType:(NSString *)subsysType
                           SubsysDescription:(NSString *)subysyDescription {
    
    @try {
        self.subsysID = subsysID;
        self.subsysType = subsysType;
        self.subsysDescription = subysyDescription;
        
        [self.imvIcon setImage:[UIImage imageNamed:PIC_AAF_ICON]];
        [self.lblSystemName setText:@"Active AIRflow™ System"];
        [self.lblDescription setText:[NSString stringWithFormat:@"(%@)", subysyDescription]];
        
        [self displayComponentWithCellType:HOME_MENU_CELL_TYPE_AAF];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)displayComponentWithCellType:(HOME_MENU_CELL_TYPE)cellType {
    
    @try {
        if (cellType == HOME_MENU_CELL_TYPE_NONE) {
            
            [self.imvAdd setHidden:YES];
            [self.imvIcon setHidden:YES];
            [self.lblSystemName setHidden:YES];
            [self.lblDescription setHidden:YES];
            
        } else if (cellType == HOME_MENU_CELL_TYPE_ADD) {
            
            [self.imvAdd setHidden:NO];
            [self.imvIcon setHidden:YES];
            [self.lblSystemName setHidden:YES];
            [self.lblDescription setHidden:YES];
            
        } else if (cellType == HOME_MENU_CELL_TYPE_AAF) {
            
            [self.imvAdd setHidden:YES];
            [self.imvIcon setHidden:NO];
            [self.lblSystemName setHidden:NO];
            [self.lblDescription setHidden:NO];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
