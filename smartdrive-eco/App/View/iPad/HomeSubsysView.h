//
//  HomeSubsysView.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/17/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../Common/Common-Util.h"
#import "./HomeSubsysViewLayout.h"
#import "./HomeSubsysCell.h"
#import "./ActiveAirflowVC.h"
#import "../../ViewModel/iPad/HomeSubsysVM.h"

@interface HomeSubsysView : UICollectionView <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) HomeSubsysVM *viewModel;
@property (nonatomic, strong) UIActivityIndicatorView *aivNetworking;

@property (nonatomic, strong) NSString *selectedSubsysID;

@end

@interface HomeSubsysView (HomeSubsysViewEX)

- (void)displayScanSubsysVC;
- (void)displayActiveAirflowVC;

@end
