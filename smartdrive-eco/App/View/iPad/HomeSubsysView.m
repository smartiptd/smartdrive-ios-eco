//
//  HomeSubsysView.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/17/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "HomeSubsysView.h"

static NSString * const ID_SUBSYS_CELL          = @"subsysCell";

static NSString * const SEGUE_SCAN_SUBSYS       = @"segueScanSubsys";
static NSString * const SEGUE_ACTIVE_AIRFLOW    = @"segueActiveAirflow";

@interface HomeSubsysView()

@property (nonatomic, strong) NSArray *currentSubscriptionList;
@property (nonatomic) CGFloat minOffset;

@end

@implementation HomeSubsysView

- (instancetype)initWithFrame:(CGRect)frame collectionViewLayout:(nonnull UICollectionViewLayout *)layout
{
    @try {
        self = [super initWithFrame:frame collectionViewLayout:layout];
        if (self) {
            [self initViewModel];
            [self initSubview];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initViewModel {
    
    @try {
        self.viewModel = [[HomeSubsysVM alloc] init];
        
        self.currentSubscriptionList = [self.viewModel getOldSubscriptionList];
        
        [self.viewModel.coreVM addObserver:self
                                forKeyPath:@"networkStatus"
                                   options:(NSKeyValueObservingOptionNew)
                                   context:NULL];
        
        [self.viewModel.coreVM addObserver:self
                                forKeyPath:@"subscriptionList"
                                   options:(NSKeyValueObservingOptionNew)
                                   context:NULL];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initSubview {
    
    CGFloat frameW = self.frame.size.width;
    CGFloat frameH = self.frame.size.height;
    
    @try {
        [self setBackgroundColor:[UIColor BG_LIGHT_GRAY]];
        
        self.dataSource = self;
        self.delegate = self;
        [self setAlwaysBounceVertical:YES];
        
        [self registerClass:[HomeSubsysCell class] forCellWithReuseIdentifier:ID_SUBSYS_CELL];
        
        /* Initial UI component */
        self.aivNetworking = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, frameW, frameH)];
        [self.aivNetworking setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self.aivNetworking setCenter:[self center]];
        [self.aivNetworking setBackgroundColor:[UIColor INDICATOR_VIEW]];
        [self.aivNetworking setHidesWhenStopped:YES];
        [self.aivNetworking stopAnimating];
        
        /* Add subview */
        [self addSubview:self.aivNetworking];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)drawRect:(CGRect)rect {
    
    @try {
        
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - UICollectionView DataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    NSInteger numOfSec = 1;
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return numOfSec;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger numOfItem = 1;
    
    @try {
        if (self.currentSubscriptionList) {
            numOfItem += [self.currentSubscriptionList count];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return numOfItem;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeSubsysCell *cell;
    
    @try {
        cell = (HomeSubsysCell *)[collectionView dequeueReusableCellWithReuseIdentifier:ID_SUBSYS_CELL forIndexPath:indexPath];
        [[[cell contentView] subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        if (indexPath.item == [self.currentSubscriptionList count]) {
            
            /* Display add sub system cell */
            [cell displayAddSubsysCell];
            
        } else {
            
            NSManagedObject *subscription = [self.currentSubscriptionList objectAtIndex:indexPath.item];
            if (subscription) {
                
                NSString *subsysID = [subscription valueForKey:@"subsysID"];
                NSString *subsysType = [subscription valueForKey:@"subsysType"];
                NSString *refName = [subscription valueForKey:@"refName"];
                
                if ([subsysType isEqualToString:SUBSYS_AAF]) {
                    
                    /* Display add sub system cell */
                    [cell displayActiveAirflowCellWithSubsysID:subsysID
                                                    SubsysType:subsysType
                                             SubsysDescription:(refName && ![refName isEqualToString:BLANK]) ? refName : subsysID];
                    
                } else {
                    
                    /* Display add sub system cell */
                    [cell displayAddSubsysCell];
                }
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return cell;
    }
}

#pragma mark - UICollectionView Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    @try {
        if (indexPath.item == [self.currentSubscriptionList count]) {
            /* Display add subsys view */
            [self displayScanSubsysVC];
            
        } else {
            
            HomeSubsysCell *cell = (HomeSubsysCell *)[collectionView cellForItemAtIndexPath:indexPath];
            if (cell) {
                
                self.selectedSubsysID = cell.subsysID;
                if ([cell.subsysType isEqualToString:SUBSYS_AAF]) {
                    
                    /* Display Active Airflow view */
                    [self displayActiveAirflowVC];
                }
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}


- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    @try {
        
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    @try {
        HomeSubsysCell *cell = (HomeSubsysCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [cell setBackgroundColor:[UIColor HOME_MENU_DARK_GRAY]];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    @try {
        HomeSubsysCell *cell = (HomeSubsysCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [cell setBackgroundColor:[UIColor HOME_MENU_LIGHT_GRAY]];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;  
    }
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    @try {
        if (scrollView.contentOffset.y < self.minOffset) {
            
            self.minOffset = scrollView.contentOffset.y;
            
        } else if (scrollView.contentOffset.y == 0) {
            
            if (self.minOffset <= -120.0f) {
                [self.viewModel.coreVM requestSubscriptionInfo];
            }
            self.minOffset = 0;
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end

@implementation HomeSubsysView (HomeSubsysViewEX)

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    @try {
        if ([keyPath isEqualToString:@"networkStatus"]) {
            
            if ([[change objectForKey:@"new"] isEqual:[NSNumber numberWithInteger:NETWORK_PROCESS]]) {
                [self.aivNetworking startAnimating];
            } else {
                [self.aivNetworking stopAnimating];
            }
        }
        
        if ([keyPath isEqualToString:@"subscriptionList"]) {
            
            if ([change objectForKey:@"new"]) {
                
                NSArray *tmpSubscriptionList = ([[change objectForKey:@"new"] isEqual:[NSNull null]]) ? nil : [change objectForKey:@"new"];
                self.currentSubscriptionList = [self.viewModel updateSubscriptionWithSubscriptionList:tmpSubscriptionList];
                
                [self reloadData];
                [self layoutIfNeeded];
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)displayScanSubsysVC {
    
    @try {
        [[Common getTopController] performSegueWithIdentifier:SEGUE_SCAN_SUBSYS sender:self];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)displayActiveAirflowVC {
    
    @try {
        [[Common getTopController] performSegueWithIdentifier:SEGUE_ACTIVE_AIRFLOW sender:self];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
