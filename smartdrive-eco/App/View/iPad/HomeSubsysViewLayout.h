//
//  HomeSystemViewLayout.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/18/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeSubsysViewLayout : UICollectionViewLayout

@property (nonatomic) UIEdgeInsets itemInset;
@property (nonatomic) CGSize itemSize;
@property (nonatomic) CGFloat interItemSpaceY;
@property (nonatomic) NSInteger numOfCol;

@end
