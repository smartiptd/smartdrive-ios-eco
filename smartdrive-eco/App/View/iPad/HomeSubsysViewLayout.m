//
//  HomeSystemViewLayout.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/18/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "HomeSubsysViewLayout.h"

static NSString * const ID_SUBSYS_CELL = @"subsysCell";

@interface HomeSubsysViewLayout()

@property (nonatomic, strong) NSDictionary *layoutInfo;

@end

@implementation HomeSubsysViewLayout

- (instancetype)init
{
    @try {
        self = [super init];
        if (self) {
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
    
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    @try {
        self.itemInset = UIEdgeInsetsMake(20.0f, 20.0f, 20.0f, 20.0f);
        self.itemSize = CGSizeMake((screenW - (4.0f * 20.0f)) / 3.0f, ((screenH * 2.0f) / 3.0f - (4.0f * 20.0f)) / 3.0f);
        self.interItemSpaceY = 20.0f;
        self.numOfCol = 3;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - Layout
- (void)prepareLayout {
    
    NSMutableDictionary *newLayoutInfo = [NSMutableDictionary dictionary];
    NSMutableDictionary *cellLayoutInfo = [NSMutableDictionary dictionary];
    
    @try {
        NSInteger sectionCount = [self.collectionView numberOfSections];
        for (NSInteger section = 0; section < sectionCount; section++) {
            
            NSInteger itemCount = [self.collectionView numberOfItemsInSection:section];
            for (NSInteger item = 0; item < itemCount; item++) {
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
                UICollectionViewLayoutAttributes *itemAttributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
                itemAttributes.frame = [self frameForItemAtIndexPath:indexPath];
                cellLayoutInfo[indexPath] = itemAttributes;
            }
        }
        
        newLayoutInfo[ID_SUBSYS_CELL] = cellLayoutInfo;
        self.layoutInfo = newLayoutInfo;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - Private
- (CGRect)frameForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGRect itemFrame = CGRectZero;
    NSInteger row = 0;
    
    @try {
        NSInteger sectionCount = [self.collectionView numberOfSections];
        for (NSInteger section = 0; section < sectionCount; section++) {
            
            if (section == indexPath.section) {
                row += indexPath.item / self.numOfCol;
                break;
            } else {
                NSInteger itemCount = [self.collectionView numberOfItemsInSection:section];
                NSInteger rowCount = itemCount / self.numOfCol;
                row += (itemCount % self.numOfCol) ? ++rowCount : rowCount;
            }
        }

        NSInteger column = indexPath.item % self.numOfCol;
        
        CGFloat spacingX = self.collectionView.bounds.size.width -
                            self.itemInset.left -
                            self.itemInset.right -
                            (self.numOfCol * self.itemSize.width);
        
        if (self.numOfCol > 1) spacingX = spacingX / (self.numOfCol - 1);
        
        CGFloat originX = floorf(self.itemInset.left + (self.itemSize.width + spacingX) * column);
        CGFloat originY = floor(self.itemInset.top + (self.itemSize.height + self.interItemSpaceY) * row);
        
        itemFrame = CGRectMake(originX, originY, self.itemSize.width, self.itemSize.height);
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return itemFrame;
    }
}

#pragma mark - UICollectionViewLayout Delegate
- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSMutableArray *allAttributes;
    
    @try {
        allAttributes = [NSMutableArray arrayWithCapacity:self.layoutInfo.count];
        
        [self.layoutInfo enumerateKeysAndObjectsUsingBlock:^(NSString *elementIdentifier, NSDictionary *elementsInfo, BOOL *stop) {
            
            [elementsInfo enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *indexPath, UICollectionViewLayoutAttributes *attributes, BOOL *innerStop) {
                
                if (CGRectIntersectsRect(rect, attributes.frame)) {
                    
                    [allAttributes addObject:attributes];
                }
            }];
        }];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return allAttributes;
    }
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewLayoutAttributes *itemLayoutAttribute;
    
    @try {
        itemLayoutAttribute = self.layoutInfo[ID_SUBSYS_CELL][indexPath];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return itemLayoutAttribute;
    }
}

- (CGSize)collectionViewContentSize {
    
    CGSize contentSize = CGSizeZero;
    CGFloat height = 0.0f;
    
    @try {
        NSInteger sectionCount = [self.collectionView numberOfSections];
        for (NSInteger section = 0; section < sectionCount; section++) {
            
            NSInteger itemCount = [self.collectionView numberOfItemsInSection:section];
            NSInteger rowCount = itemCount / self.numOfCol;
            if (itemCount % self.numOfCol) rowCount++;
            
            height += self.itemInset.top +
                        rowCount * self.itemSize.height +
                        (rowCount - 1) * self.interItemSpaceY +
                        ((section == (sectionCount - 1)) ? self.itemInset.bottom : 0);
        }
        
        contentSize = CGSizeMake(self.collectionView.bounds.size.width, height);
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return contentSize;
    }
}

@end
