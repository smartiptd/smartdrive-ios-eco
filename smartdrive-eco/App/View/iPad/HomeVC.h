//
//  HomeVC.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/13/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../Common/Common-Util.h"
#import "../../ViewModel/iPad/HomeVM.h"
#import "./HomeProfileView.h"
#import "./HomeSubsysView.h"
#import "./HomeSubsysViewLayout.h"

@interface HomeVC : UITableViewController

@property (nonatomic, strong) HomeVM *viewModel;

@property (nonatomic, strong) HomeSubsysView *subsysView;

@end

@interface HomeVC (HomeVCEX)

@end