//
//  HomeVC.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/13/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "HomeVC.h"

static NSString * const SEGUE_LOGIN_METHOD              = @"segueLoginMethod";
static NSString * const SEGUE_LOGIN_METHOD_NO_ANIMATE   = @"segueLoginMethodNoAnimate";
static NSString * const SEGUE_ACTIVE_AIRFLOW            = @"segueActiveAirflow";

@interface HomeVC ()

typedef NS_ENUM (NSInteger, TableSection) {
    SECTION_HOME,
    SECTION_SIZE
};

@end

@implementation HomeVC

- (void)viewDidLoad {
    
    @try {
        [super viewDidLoad];
        [self initViewModel];
        [self initSubview];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    @try {
        if (![self.viewModel.coreVM.sessionMgr getCurrentSession]) {
            [self performSegueWithIdentifier:SEGUE_LOGIN_METHOD_NO_ANIMATE sender:self];
        } else {
            if (self.viewModel) {
                [self.viewModel performSelectorInBackground:@selector(requestSubscriptionInfo) withObject:nil];
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)didReceiveMemoryWarning {
    
    @try {
        [super didReceiveMemoryWarning];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)initViewModel {
    
    @try {
        self.viewModel = [[HomeVM alloc] init];
        
        [self.viewModel.coreVM addObserver:self
                                forKeyPath:@"session"
                                   options:(NSKeyValueObservingOptionNew)
                                   context:NULL];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initSubview {
    
    @try {
        /* Set background color */
        [self.tableView setBackgroundColor:[UIColor BG_LIGHT_GRAY]];
        
        /* Setup UITableView */
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.tableView setScrollEnabled:NO];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    @try {
        if ([[segue identifier] isEqualToString:SEGUE_ACTIVE_AIRFLOW]) {
            
            ActiveAirflowVC *vc = (ActiveAirflowVC *)[segue destinationViewController];
            vc.subsysID = self.subsysView.selectedSubsysID;
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger numOfSec = 0;
    
    @try {
        numOfSec = 1;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return numOfSec;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numOfRow = 0;
    
    @try {
        numOfRow = 0;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return numOfRow;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return nil;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    CGFloat headerHeight = 0;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    @try {
        if (section == SECTION_HOME) {
            headerHeight = screenH / 3;
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return headerHeight;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    CGFloat footerHeight = 0;
    CGFloat screenH = self.view.bounds.size.height;
    
    @try {
        if (section == SECTION_HOME) {
            footerHeight = (screenH * 2) / 3;
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return footerHeight;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    HomeProfileView *profileView;
    
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    @try {
        profileView = [[HomeProfileView alloc] initWithFrame:CGRectMake(0, 0, screenW, screenH / 3)];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return profileView;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    @try {
        self.subsysView = [[HomeSubsysView alloc] initWithFrame:CGRectMake(0, 0, screenW, (screenH * 2) / 3)
                                      collectionViewLayout:[[HomeSubsysViewLayout alloc] init]];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self.subsysView;
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

@end

@implementation HomeVC (HomeVCEX)

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    @try {
        if ([keyPath isEqualToString:@"session"]) {
            
            if ([[change objectForKey:@"new"] isEqual:[NSNull null]]) {
                
                [self performSegueWithIdentifier:SEGUE_LOGIN_METHOD sender:self];
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
