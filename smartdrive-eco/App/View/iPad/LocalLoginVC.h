//
//  LocalLoginVC.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/12/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../Common/Common-Util.h"
#import "../../ViewModel/iPad/LocalLoginVM.h"

@interface LocalLoginVC : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) LocalLoginVM *viewModel;

@property (nonatomic, strong) UIImageView *imvLogo;
@property (nonatomic, strong) UIView *viewLoginBox;
@property (nonatomic, strong) UIView *viewBoxSeparator;
@property (nonatomic, strong) UITextField *txtUsername;
@property (nonatomic, strong) UITextField *txtPassword;
@property (nonatomic, strong) UIButton *btnLocalLogin;
@property (nonatomic, strong) UIButton *btnForgetPassword;
@property (nonatomic, strong) UIButton *btnBack;
@property (nonatomic, strong) UIActivityIndicatorView *aivNetworking;

@end

@interface LocalLoginVC (LocalLoginVCEX)

- (IBAction)fieldDidChange:(id)sender;
- (IBAction)performLocalLogin:(id)sender;
- (IBAction)performBack:(id)sender;
- (void)enableLocalLoginButton:(BOOL)enabled;

@end
