//
//  LocalLoginVC.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/12/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "LocalLoginVC.h"

static NSString * const SEGUE_HOME = @"segueHome";
static NSString * const SEGUE_LOGIN_METHOD = @"segueLoginMethod";

@interface LocalLoginVC ()

@end

@implementation LocalLoginVC

- (void)viewDidLoad {
    
    @try {
        [super viewDidLoad];
        [self initViewModel];
        [self initSubview];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)didReceiveMemoryWarning {
    
    @try {
        [super didReceiveMemoryWarning];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initViewModel {
    
    @try {
        self.viewModel = [[LocalLoginVM alloc] init];
        
        [self.viewModel.coreVM addObserver:self
                                forKeyPath:@"networkStatus"
                                   options:(NSKeyValueObservingOptionNew)
                                   context:NULL];
        
        [self.viewModel.coreVM addObserver:self
                                forKeyPath:@"requestStatus"
                                   options:(NSKeyValueObservingOptionNew)
                                   context:NULL];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initSubview {
    
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    CGFloat imvLogoW = 0;
    CGFloat imvLogoH = 0;
    CGFloat imvLogoX = 0;
    CGFloat imvLogoY = 0;
    
    CGFloat viewLoginBoxW = 0;
    CGFloat viewLoginBoxH = 0;
    CGFloat viewLoginBoxX = 0;
    CGFloat viewLoginBoxY = 0;
    
    CGFloat viewBoxSeparatorW = 0;
    CGFloat viewBoxSeparatorH = 0;
    CGFloat viewBoxSeparatorX = 0;
    CGFloat viewBoxSeparatorY = 0;
    
    CGFloat txtUsernameW = 0;
    CGFloat txtUsernameH = 0;
    CGFloat txtUsernameX = 0;
    CGFloat txtUsernameY = 0;
    
    CGFloat txtPasswordW = 0;
    CGFloat txtPasswordH = 0;
    CGFloat txtPasswordX = 0;
    CGFloat txtPasswordY = 0;
    
    CGFloat btnLocalLoginW = 0;
    CGFloat btnLocalLoginH = 0;
    CGFloat btnLocalLoginX = 0;
    CGFloat btnLocalLoginY = 0;
    
    CGFloat btnForgetPassW = 0;
    CGFloat btnForgetPassH = 0;
    CGFloat btnForgetPassX = 0;
    CGFloat btnForgetPassY = 0;
    
    CGFloat btnBackX, btnBackY, btnBackW, btnBackH = 0;
    
    @try {
        /* Set background color */
        [self.view setBackgroundColor:[UIColor BG_LIGHT_GRAY]];
        
        /* Component calibration */
        imvLogoW = (screenW * 180) / 1024;
        imvLogoH = (screenH * 153) / 768;
        imvLogoX = (screenW - imvLogoW) / 2;
        imvLogoY = (screenH / 3) - (imvLogoH * 2) / 3;
        
        viewLoginBoxW = btnLocalLoginW = btnForgetPassW = (screenW * 4) / 9;
        txtUsernameW = txtPasswordW = viewBoxSeparatorW = viewLoginBoxW - (screenW * 50) / 1024;
        viewLoginBoxH = screenH / 6;
        btnLocalLoginH = viewLoginBoxH / 2.5;
        btnForgetPassH = btnLocalLoginH / 2.5;
        txtUsernameH = txtPasswordH = viewLoginBoxH / 2;
        viewBoxSeparatorH = (screenH * 1) / 768;
        
        viewLoginBoxX = btnLocalLoginX = btnForgetPassX = (screenW - viewLoginBoxW) / 2;
        txtUsernameX = txtPasswordX = viewBoxSeparatorX = (viewLoginBoxW - txtUsernameW) / 2;
        viewLoginBoxY = imvLogoY + imvLogoH + (screenH * 10) / 768;
        txtUsernameY = 0;
        txtPasswordY = viewBoxSeparatorY = viewLoginBoxH / 2;
        btnLocalLoginY = viewLoginBoxY + viewLoginBoxH + (screenH * 20) / 768;
        btnForgetPassY = btnLocalLoginY + btnLocalLoginH + (screenH * 20) / 768;
        
        btnBackW = screenW / 14.7f;
        btnBackH = screenH / 19.2f;
        btnBackX = (screenW - btnBackW) / 2;
        btnBackY = screenH - btnBackH;
        
        /* Initial UI component */
        self.imvLogo = [[UIImageView alloc] initWithFrame:CGRectMake(imvLogoX, imvLogoY, imvLogoW, imvLogoH)];
        self.viewLoginBox = [[UIView alloc] initWithFrame:CGRectMake(viewLoginBoxX, viewLoginBoxY, viewLoginBoxW, viewLoginBoxH)];
        self.txtUsername = [[UITextField alloc] initWithFrame:CGRectMake(txtUsernameX, txtUsernameY, txtUsernameW, txtUsernameH)];
        self.txtPassword = [[UITextField alloc] initWithFrame:CGRectMake(txtPasswordX, txtPasswordY, txtPasswordW, txtPasswordH)];
        self.viewBoxSeparator = [[UIView alloc] initWithFrame:CGRectMake(viewBoxSeparatorX, viewBoxSeparatorY, viewBoxSeparatorW, viewBoxSeparatorH)];
        self.btnLocalLogin = [[UIButton alloc] initWithFrame:CGRectMake(btnLocalLoginX, btnLocalLoginY, btnLocalLoginW, btnLocalLoginH)];
        self.aivNetworking = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, screenW, screenH)];
        
        /* Initial UI property */
        [self.imvLogo setImage:[UIImage imageNamed:GUI_LOGIN_LOGO]];
        
        [self.viewLoginBox setBackgroundColor:[UIColor BG_WHITE]];
        [self.viewLoginBox.layer setBorderColor:[UIColor TEXTFIELD_BORDER].CGColor];
        [self.viewLoginBox.layer setBorderWidth:1];
        [self.viewLoginBox.layer setCornerRadius:0];
        [self.viewLoginBox.layer setMasksToBounds:NO];
        [self.viewLoginBox.layer setShadowOffset:CGSizeZero];
        [self.viewLoginBox.layer setShadowColor:[UIColor clearColor].CGColor];
        [self.viewLoginBox.layer setShadowOpacity:0];
        
        [self.txtUsername setPlaceholder:@"Email address"];
        [self.txtUsername setFont:[UIFont fontWithName:FONT_DB_HELVETICA_LI_COND size:28]];
        [self.txtUsername setBackgroundColor:[UIColor BG_WHITE]];
        [self.txtUsername setKeyboardType:UIKeyboardTypeEmailAddress];
        [self.txtUsername setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [self.txtUsername setDelegate:self];
        [self.txtUsername addTarget:self action:@selector(fieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        
        [self.txtPassword setPlaceholder:@"Password"];
        [self.txtPassword setSecureTextEntry:YES];
        [self.txtPassword setFont:[UIFont fontWithName:FONT_DB_HELVETICA_LI_COND size:28]];
        [self.txtPassword setBackgroundColor:[UIColor BG_WHITE]];
        [self.txtPassword setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [self.txtPassword setDelegate:self];
        [self.txtPassword addTarget:self action:@selector(fieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        
        [self.viewBoxSeparator setBackgroundColor:[UIColor TEXTFIELD_BORDER]];
        
        [self.btnLocalLogin setTitle:@"LOG IN" forState:UIControlStateNormal];
        [self.btnLocalLogin.titleLabel setFont:[UIFont fontWithName:FONT_DB_HELVETICA_BD_COND size:24]];
        [self.btnLocalLogin setBackgroundImage:[Common imageWithColor:[UIColor LIVINGTECH_RED]] forState:UIControlStateNormal];
        [self.btnLocalLogin setBackgroundImage:[Common imageWithColor:[UIColor LIVINGTECH_RED_HILIGHT]] forState:UIControlStateHighlighted];
        [self.btnLocalLogin addTarget:self action:@selector(performLocalLogin:) forControlEvents:UIControlEventTouchUpInside];
        
        self.btnForgetPassword = [[UIButton alloc] initWithFrame:CGRectMake(btnForgetPassX, btnForgetPassY, btnForgetPassW, btnForgetPassH)
                                                 Title:@"Forget password?"
                                                  Font:FONT_DB_HELVETICA_LI_COND
                                                  Size:24
                                            TitleColor:[UIColor TEXT_LIGHT_GRAY]
                                     TitleHilightColor:[UIColor TEXT_DARK_GRAY]
                                        UnderlineStyle:NSUnderlineStyleSingle
                                                 Color:[UIColor clearColor]
                                          HilightColor:[UIColor clearColor]
                                           BorderColor:[UIColor clearColor]
                                           BorderWidth:0
                                          BorderRadius:0];
        
        self.btnBack = [[UIButton alloc] initWithFrame:CGRectMake(btnBackX, btnBackY, btnBackW, btnBackH)
                                                 Title:@"Back"
                                                  Font:FONT_DB_HELVETICA_LI_COND
                                                  Size:24
                                            TitleColor:[UIColor TEXT_LIGHT_GRAY]
                                     TitleHilightColor:[UIColor TEXT_DARK_GRAY]
                                        UnderlineStyle:NSUnderlineStyleSingle
                                                 Color:[UIColor clearColor]
                                          HilightColor:[UIColor clearColor]
                                           BorderColor:[UIColor clearColor]
                                           BorderWidth:0
                                          BorderRadius:0];
        [self.btnBack addTarget:self action:@selector(performBack:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.aivNetworking setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self.aivNetworking setCenter:[self.view center]];
        [self.aivNetworking setBackgroundColor:[UIColor INDICATOR_VIEW]];
        [self.aivNetworking setHidesWhenStopped:YES];
        [self.aivNetworking stopAnimating];
        
        /* Add UI component into the view */
        [self.viewLoginBox addSubview:self.txtUsername];
        [self.viewLoginBox addSubview:self.txtPassword];
        [self.viewLoginBox addSubview:self.viewBoxSeparator];
        
        [self.view addSubview:self.imvLogo];
        [self.view addSubview:self.viewLoginBox];
        [self.view addSubview:self.btnLocalLogin];
        [self.view addSubview:self.btnForgetPassword];
        [self.view addSubview:self.btnBack];
        [self.view addSubview:self.aivNetworking];
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, screenH / 5.5, screenW, screenH)];
        [bgView setBackgroundColor:[UIColor BG_LIGHT_GRAY]];
        [self.view addSubview:bgView];
        [self.view sendSubviewToBack:bgView];
        
        [self enableLocalLoginButton:NO];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma - UITextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    @try {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidShow:)
                                                     name:UIKeyboardDidShowNotification
                                                   object:nil];
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        return YES;
    }
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    @try {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidHide:)
                                                     name:UIKeyboardDidHideNotification
                                                   object:nil];
        
        [self.view endEditing:YES];
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        return YES;
    }
}

- (void)keyboardDidShow:(NSNotification *)notification {
    
    @try {
        /* Move screen up */
        [UIView animateWithDuration:0.2 animations:^{
            CGFloat screenW = self.view.bounds.size.width;
            CGFloat screenH = self.view.bounds.size.height;
            [self.view setFrame:CGRectMake(0, -(screenH / 5.5), screenW, screenH)];
        }];
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        return;
    }
}

- (void)keyboardDidHide:(NSNotification *)notification
{
    @try {
        /* Move screen down */
        [UIView animateWithDuration:0.1 animations:^{
            CGFloat screenW = self.view.bounds.size.width;
            CGFloat screenH = self.view.bounds.size.height;
            [self.view setFrame:CGRectMake(0, 0, screenW, screenH)];
        }];
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        return;
    }
}

@end

@implementation LocalLoginVC (LocalLoginVCEX)

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    @try {
        if ([keyPath isEqualToString:@"networkStatus"] && [self isEqual:[Common getTopController]]) {
            
            if ([[change objectForKey:@"new"] isEqual:[NSNumber numberWithInteger:NETWORK_PROCESS]]) {
                [self.aivNetworking startAnimating];
            } else {
                [self.aivNetworking stopAnimating];
            }
        }
        
        if ([keyPath isEqualToString:@"requestStatus"] && [self isEqual:[Common getTopController]]) {
            
            if ([[change objectForKey:@"new"] isEqual:[NSNumber numberWithInteger:REQUEST_SUCCESS]]) {
                
                if ([self.viewModel.coreVM.sessionMgr getCurrentSession]) {
                     [self performSegueWithIdentifier:SEGUE_HOME sender:self];
                }
            } else {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login failed"
                                                                               message:@"Invalid email or password"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:okButton];
                [[Common getTopController] presentViewController:alert animated:YES completion:nil];
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)enableLocalLoginButton:(BOOL)enabled {
    
    @try {
        [self.btnLocalLogin setEnabled:(enabled) ? YES : NO];
        [self.btnLocalLogin setAlpha:(enabled) ? 1.0 : 0.5];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)fieldDidChange:(id)sender {
    
    @try {
        [self enableLocalLoginButton:[self.viewModel isValidEmail:self.txtUsername.text
                                                         Password:self.txtPassword.text]];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performLocalLogin:(id)sender {
    
    @try {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidHide:)
                                                     name:UIKeyboardDidHideNotification
                                                   object:nil];
        
        [self.viewModel localLoginWithEmail:self.txtUsername.text
                                   Password:self.txtPassword.text];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performBack:(id)sender {
    
    @try {
        [self performSegueWithIdentifier:SEGUE_LOGIN_METHOD sender:self];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
