//
//  LoginMethodVC.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/11/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import "FontAwesomeKit.h"
#import "../../Common/Common-Util.h"
#import "../../ViewModel/iPad/LoginMethodVM.h"

@interface LoginMethodVC : UIViewController <GIDSignInUIDelegate>

@property (nonatomic, strong) LoginMethodVM *viewModel;

@property (nonatomic, strong) UIImageView *imvLogo;
@property (nonatomic, strong) UIButton *btnLocalLogin;
@property (nonatomic, strong) UIButton *btnCreateAccount;
@property (nonatomic, strong) UIView *viewSeparator;
@property (nonatomic, strong) UILabel *lblSeparator;
@property (nonatomic, strong) UIButton *btnFacebookLogin;
@property (nonatomic, strong) UIButton *btnGoogleLogin;
@property (nonatomic, strong) UIActivityIndicatorView *aivNetworking;

@end

@interface LoginMethodVC (LoginMethodVCEX)

- (IBAction)performLocalLogin:(id)sender;
- (IBAction)performFacebookLogin:(id)sender;
- (IBAction)performGoogleLogin:(id)sender;
- (IBAction)performSignup:(id)sender;

@end