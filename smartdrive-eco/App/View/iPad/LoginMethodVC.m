//
//  LoginMethodVC.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/11/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "LoginMethodVC.h"

static NSString * const SEGUE_LOCAL_LOGIN = @"segueLocalLogin";

@interface LoginMethodVC ()

@end

@implementation LoginMethodVC

- (void)viewDidLoad {
    
    @try {
        [super viewDidLoad];
        [self initViewModel];
        [self initSubview];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)didReceiveMemoryWarning {
    
    @try {
        [super didReceiveMemoryWarning];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initViewModel {
    
    @try {
        self.viewModel = [[LoginMethodVM alloc] init];
        
        [self.viewModel.coreVM addObserver:self
                                forKeyPath:@"networkStatus"
                                   options:(NSKeyValueObservingOptionNew)
                                   context:NULL];
        
        [self.viewModel.coreVM addObserver:self
                                forKeyPath:@"requestStatus"
                                   options:(NSKeyValueObservingOptionNew)
                                   context:NULL];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}


- (void)initSubview {
    
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    CGFloat imvLogoW = 0;
    CGFloat imvLogoH = 0;
    CGFloat imvLogoX = 0;
    CGFloat imvLogoY = 0;
    
    CGFloat btnLocalLoginW = 0;
    CGFloat btnLocalLoginH = 0;
    CGFloat btnLocalLoginX = 0;
    CGFloat btnLocalLoginY = 0;
    
    CGFloat btnCreateAccountW = 0;
    CGFloat btnCreateAccountH = 0;
    CGFloat btnCreateAccountX = 0;
    CGFloat btnCreateAccountY = 0;
    
    CGFloat viewSeparatorW = 0;
    CGFloat viewSeparatorH = 0;
    CGFloat viewSeparatorX = 0;
    CGFloat viewSeparatorY = 0;
    
    CGFloat lblSeparatorW = 0;
    CGFloat lblSeparatorH = 0;
    CGFloat lblSeparatorX = 0;
    CGFloat lblSeparatorY = 0;
    
    CGFloat btnFbLoginW = 0;
    CGFloat btnFbLoginH = 0;
    CGFloat btnFbLoginX = 0;
    CGFloat btnFbLoginY = 0;
    
    CGFloat btnGgLoginW = 0;
    CGFloat btnGgLoginH = 0;
    CGFloat btnGgLoginX = 0;
    CGFloat btnGgLoginY = 0;
    
    @try {
        /* Set background color */
        [self.view setBackgroundColor:[UIColor BG_LIGHT_GRAY]];
        
        /* Component calibration */
        imvLogoW = (screenW * 180) / 1024;
        imvLogoH = (screenH * 153) / 768;
        imvLogoX = (screenW - imvLogoW) / 2;
        imvLogoY = (screenH / 3) - imvLogoH;
        
        lblSeparatorW = (screenW * 50) / 1024;
        lblSeparatorH = (screenH * 50) / 768;
        
        btnLocalLoginW = btnCreateAccountW = viewSeparatorW = btnFbLoginW = btnGgLoginW = (screenW * 4) / 9;
        btnLocalLoginH = btnFbLoginH = btnGgLoginH = (screenH * 65) / 768;
        btnCreateAccountH = btnLocalLoginH / 2.5;
        viewSeparatorH = (screenH * 1.5) / 768;
        btnLocalLoginX = btnCreateAccountX = viewSeparatorX = btnFbLoginX = btnGgLoginX = (screenW - (screenW * 4) / 9) / 2;
        lblSeparatorX = (screenW / 2) - (lblSeparatorW / 2);
        btnLocalLoginY = imvLogoY + imvLogoH + (screenH * 50) / 768;
        btnCreateAccountY = btnLocalLoginY + btnLocalLoginH + (screenH * 10) / 768;
        viewSeparatorY = btnCreateAccountY + btnCreateAccountH + (screenH * 50) / 768;
        lblSeparatorY = viewSeparatorY - (screenH * 20) / 768;
        btnFbLoginY = viewSeparatorY + viewSeparatorH + (screenH * 50) / 768;
        btnGgLoginY = btnFbLoginY + btnFbLoginH + (screenH * 10) / 768;
        
        /* Initial UI component */
        self.imvLogo = [[UIImageView alloc] initWithFrame:CGRectMake(imvLogoX, imvLogoY, imvLogoW, imvLogoH)];
        self.btnLocalLogin = [[UIButton alloc] initWithFrame:CGRectMake(btnLocalLoginX, btnLocalLoginY, btnLocalLoginW, btnLocalLoginH)];
        self.btnCreateAccount = [[UIButton alloc] initWithFrame:CGRectMake(btnCreateAccountX, btnCreateAccountY, btnCreateAccountW, btnCreateAccountH)];
        self.viewSeparator = [[UIView alloc] initWithFrame:CGRectMake(viewSeparatorX, viewSeparatorY, viewSeparatorW, viewSeparatorH)];
        self.lblSeparator = [[UILabel alloc] initWithFrame:CGRectMake(lblSeparatorX, lblSeparatorY, lblSeparatorW, lblSeparatorH)];
        self.btnFacebookLogin = [[UIButton alloc] initWithFrame:CGRectMake(btnFbLoginX, btnFbLoginY, btnFbLoginW, btnFbLoginH)];
        [GIDSignIn sharedInstance].uiDelegate = self;
        self.btnGoogleLogin = [[UIButton alloc] initWithFrame:CGRectMake(btnGgLoginX, btnGgLoginY, btnGgLoginW, btnGgLoginH)];
        self.aivNetworking = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, screenW, screenH)];
        
        /* Initial UI property */
        [self.imvLogo setImage:[UIImage imageNamed:GUI_LOGIN_LOGO]];
        
        [self.btnLocalLogin setTitle:@"Login with existing account" forState:UIControlStateNormal];
        [self.btnLocalLogin.titleLabel setFont:[UIFont fontWithName:FONT_HELVETICA_REGULAR size:22]];
        [self.btnLocalLogin setBackgroundImage:[Common imageWithColor:[UIColor LIVINGTECH_RED]] forState:UIControlStateNormal];
        [self.btnLocalLogin setBackgroundImage:[Common imageWithColor:[UIColor LIVINGTECH_RED_HILIGHT]] forState:UIControlStateHighlighted];
        [self.btnLocalLogin addTarget:self action:@selector(performLocalLogin:) forControlEvents:UIControlEventTouchUpInside];
        
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:@"Need an account?"];
        [attrStr setAttributes:@{NSForegroundColorAttributeName:[UIColor TEXT_LIGHT_GRAY],
                                 NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]}
                         range:NSMakeRange(0, [attrStr length])];
        NSMutableAttributedString *attrStrHi = [[NSMutableAttributedString alloc] initWithString:@"Need an account?"];
        [attrStrHi setAttributes:@{NSForegroundColorAttributeName:[UIColor TEXT_DARK_GRAY],
                                   NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]}
                           range:NSMakeRange(0, [attrStr length])];
        [self.btnCreateAccount setAttributedTitle:attrStr forState:UIControlStateNormal];
        [self.btnCreateAccount setAttributedTitle:attrStrHi forState:UIControlStateHighlighted];
        [self.btnCreateAccount.titleLabel setFont:[UIFont fontWithName:FONT_DB_HELVETICA_LI_COND size:24]];
        [self.btnCreateAccount addTarget:self action:@selector(performSignup:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.viewSeparator setBackgroundColor:[UIColor SCREEN_SEPARATOR]];
        
        [self.lblSeparator setText:@"OR"];
        [self.lblSeparator setBackgroundColor:[UIColor BG_LIGHT_GRAY]];
        [self.lblSeparator setTextColor:[UIColor SCREEN_SEPARATOR]];
        [self.lblSeparator setTextAlignment:NSTextAlignmentCenter];
        [self.lblSeparator setFont:[UIFont fontWithName:FONT_HELVETICA_REGULAR size:18]];
        
        // Facebook login button
        FAKZocial *facebookIcon = [FAKZocial facebookIconWithSize:(btnFbLoginH / 2)];
        [facebookIcon addAttribute:NSBackgroundColorAttributeName value:[UIColor BUTTON_FACEBOOK]];
        [facebookIcon addAttribute:NSForegroundColorAttributeName value:[UIColor TEXT_WHITE]];
        NSMutableAttributedString *facebookAttr = [[facebookIcon attributedString] mutableCopy];
        [facebookAttr appendAttributedString:[[NSAttributedString alloc] initWithString:@"  Login with Facebook"
                                                                             attributes:@{NSForegroundColorAttributeName:[UIColor TEXT_WHITE],
                                                                                          NSFontAttributeName:[UIFont fontWithName:FONT_HELVETICA_REGULAR size:22]}]];
        [self.btnFacebookLogin setAttributedTitle:facebookAttr forState:UIControlStateNormal];
        [self.btnFacebookLogin setBackgroundImage:[Common imageWithColor:[UIColor BUTTON_FACEBOOK]] forState:UIControlStateNormal];
        
        FAKZocial *facebookIconHi = [FAKZocial facebookIconWithSize:(btnFbLoginH / 2)];
        [facebookIconHi addAttribute:NSBackgroundColorAttributeName value:[UIColor BUTTON_FACEBOOK_HILIGHT]];
        [facebookIconHi addAttribute:NSForegroundColorAttributeName value:[UIColor TEXT_WHITE]];
        NSMutableAttributedString *facebookAttrHi = [[facebookIconHi attributedString] mutableCopy];
        [facebookAttrHi appendAttributedString:[[NSAttributedString alloc] initWithString:@"  Login with Facebook"
                                                                               attributes:@{NSForegroundColorAttributeName:[UIColor TEXT_WHITE],
                                                                                            NSFontAttributeName:[UIFont fontWithName:FONT_HELVETICA_REGULAR size:22]}]];
        [self.btnFacebookLogin setAttributedTitle:facebookAttrHi forState:UIControlStateHighlighted];
        [self.btnFacebookLogin setBackgroundImage:[Common imageWithColor:[UIColor BUTTON_FACEBOOK_HILIGHT]] forState:UIControlStateHighlighted];
        
        [self.btnFacebookLogin addTarget:self action:@selector(performFacebookLogin:) forControlEvents:UIControlEventTouchUpInside];
        
        // Google login button
        FAKZocial *googleIcon = [FAKZocial googleIconWithSize:(btnGgLoginH / 2)];
        [googleIcon addAttribute:NSBackgroundColorAttributeName value:[UIColor BUTTON_GOOGLE]];
        [googleIcon addAttribute:NSForegroundColorAttributeName value:[UIColor TEXT_WHITE]];
        NSMutableAttributedString *googleAttr = [[googleIcon attributedString] mutableCopy];
        [googleAttr appendAttributedString:[[NSAttributedString alloc] initWithString:@"  Login with Google"
                                                                           attributes:@{NSForegroundColorAttributeName:[UIColor TEXT_WHITE],
                                                                                        NSFontAttributeName:[UIFont fontWithName:FONT_HELVETICA_REGULAR size:22]}]];
        [self.btnGoogleLogin setAttributedTitle:googleAttr forState:UIControlStateNormal];
        [self.btnGoogleLogin setBackgroundImage:[Common imageWithColor:[UIColor BUTTON_GOOGLE]] forState:UIControlStateNormal];
        
        FAKZocial *googleIconHi = [FAKZocial googleIconWithSize:(btnGgLoginH / 2)];
        [googleIconHi addAttribute:NSBackgroundColorAttributeName value:[UIColor BUTTON_GOOGLE_HILIGHT]];
        [googleIconHi addAttribute:NSForegroundColorAttributeName value:[UIColor TEXT_WHITE]];
        NSMutableAttributedString *googleAttrHi = [[googleIconHi attributedString] mutableCopy];
        [googleAttrHi appendAttributedString:[[NSAttributedString alloc] initWithString:@"  Login with Google"
                                                                             attributes:@{NSForegroundColorAttributeName:[UIColor TEXT_WHITE],
                                                                                          NSFontAttributeName:[UIFont fontWithName:FONT_HELVETICA_REGULAR size:22]}]];
        [self.btnGoogleLogin setAttributedTitle:googleAttrHi forState:UIControlStateHighlighted];
        [self.btnGoogleLogin setBackgroundImage:[Common imageWithColor:[UIColor BUTTON_GOOGLE_HILIGHT]] forState:UIControlStateHighlighted];
        
        [self.btnGoogleLogin addTarget:self action:@selector(performGoogleLogin:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.aivNetworking setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self.aivNetworking setCenter:[self.view center]];
        [self.aivNetworking setBackgroundColor:[UIColor INDICATOR_VIEW]];
        [self.aivNetworking setHidesWhenStopped:YES];
        [self.aivNetworking stopAnimating];
        
        /* Add UI component into the view */
        [self.view addSubview:self.imvLogo];
        [self.view addSubview:self.btnLocalLogin];
        [self.view addSubview:self.btnCreateAccount];
        [self.view addSubview:self.viewSeparator];
        [self.view addSubview:self.lblSeparator];
        [self.view addSubview:self.btnFacebookLogin];
        [self.view addSubview:self.btnGoogleLogin];
        [self.view addSubview:self.aivNetworking];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end

@implementation LoginMethodVC (LoginMethodVCEX)

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    @try {
        if ([keyPath isEqualToString:@"networkStatus"] && [self isEqual:[Common getTopController]]) {
            
            if ([[change objectForKey:@"new"] isEqual:[NSNumber numberWithInteger:NETWORK_PROCESS]]) {
                [self.aivNetworking startAnimating];
            } else {
                [self.aivNetworking stopAnimating];
            }
        }
        
        if ([keyPath isEqualToString:@"requestStatus"] && [self isEqual:[Common getTopController]]) {
            
            if ([[change objectForKey:@"new"] isEqual:[NSNumber numberWithInteger:REQUEST_SUCCESS]]) {
                
                if ([self.viewModel.coreVM.sessionMgr getCurrentSession]) {
                    [self performSegueWithIdentifier:@"segueHome" sender:self];
                }
            } else {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login failed"
                                                                               message:@"Please try again"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:okButton];
                [[Common getTopController] presentViewController:alert animated:YES completion:nil];
            }
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performLocalLogin:(id)sender {
    
    @try {
        [self performSegueWithIdentifier:SEGUE_LOCAL_LOGIN sender:self];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performFacebookLogin:(id)sender {
    
    @try {
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performGoogleLogin:(id)sender {
    
    @try {
        [self.viewModel googleLogin];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performSignup:(id)sender {
    
    @try {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[OAUTH2_DOMAIN stringByAppendingString:OAUTH2_SIGNUP]]];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
