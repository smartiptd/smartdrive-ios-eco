//
//  ScanSubsysVC.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/21/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QRCodeReaderDelegate.h"
#import "../../Common/Common-Util.h"
#import "../../ViewModel/iPad/ScanSubsysVM.h"

@interface ScanSubsysVC : UIViewController <QRCodeReaderDelegate>

@property (nonatomic, strong) ScanSubsysVM *viewModel;

@property (nonatomic, strong) UIImageView *imvScanning;
@property (nonatomic, strong) UIToolbar *tlbScannerTool;
@property (nonatomic, strong) UIBarButtonItem *bbiScan;
@property (nonatomic, strong) UIBarButtonItem *bbiCancel;

@end

@interface ScanSubsysVC (ScanSubsysVCEX) 

- (IBAction)performCancel:(id)sender;
- (IBAction)performScan:(id)sender;
- (void)scanningTapping:(UIGestureRecognizer *)recognizer;

@end