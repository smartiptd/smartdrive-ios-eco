//
//  ScanSubsysVC.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/21/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ScanSubsysVC.h"
#import "QRCodeReaderViewController.h"
#import "QRCodeReader.h"

static NSString * const SEGUE_HOME = @"segueHome";

static NSString * const PIC_QR_SCANNING = @"ipad-qr-scanning.png";

@interface ScanSubsysVC()

@property (nonatomic) BOOL isReading;

@end

@implementation ScanSubsysVC

- (void)viewDidLoad {
    
    @try {
        [super viewDidLoad];
        
        [self initViewModel];
        [self initSubview];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)didReceiveMemoryWarning {
    
    @try {
        [super didReceiveMemoryWarning];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initialize {
    
    @try {
        self.isReading = NO;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initViewModel {
    
    @try {
        self.viewModel = [[ScanSubsysVM alloc] init];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)initSubview {
    
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    CGFloat statusBarH = [UIApplication sharedApplication].statusBarFrame.size.height;
    
    CGFloat imvScanningX, imvScanningY, imvScanningW, imvScanningH = 0;
    CGFloat tlbScannerToolX, tlbScannerToolY, tlbScannerToolW, tlbScannerToolH = 0;
    
    @try {
        /* Set background color */
        [self.view setBackgroundColor:[UIColor BG_WHITE]];
        
        /* Component calibration */
        tlbScannerToolW = screenW;
        tlbScannerToolH = statusBarH * 2.3;
        tlbScannerToolY = screenH - tlbScannerToolH;
        
        imvScanningH = ((screenH - tlbScannerToolH - statusBarH) * 5) / 6;
        imvScanningW = imvScanningH * 1.385f;
        imvScanningX = (screenW - imvScanningW) / 2;
        imvScanningY = ((screenH - statusBarH - tlbScannerToolH - imvScanningH) / 2) + statusBarH;
        
        /* Initial UI component */
        self.imvScanning = [[UIImageView alloc] initWithFrame:CGRectMake(imvScanningX, imvScanningY, imvScanningW, imvScanningH)];
        [self.imvScanning setImage:[UIImage imageNamed:PIC_QR_SCANNING]];
        [self.imvScanning setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scanningTapping:)];
        [singleTap setNumberOfTapsRequired:1];
        [self.imvScanning addGestureRecognizer:singleTap];
        
        NSDictionary *titleAttr = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:FONT_HELVETICA_REGULAR size:16.0], NSFontAttributeName, nil];
        
        self.bbiScan = [[UIBarButtonItem alloc] initWithTitle:@"Scan Now"
                                                        style:UIBarButtonItemStylePlain
                                                       target:self
                                                       action:@selector(performScan:)];
        [self.bbiScan setTitleTextAttributes:titleAttr forState:UIControlStateNormal];
        
        self.bbiCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                          style:UIBarButtonItemStylePlain
                                                         target:self
                                                         action:@selector(performCancel:)];
        [self.bbiCancel setTitleTextAttributes:titleAttr forState:UIControlStateNormal];
        
        UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                  target:self
                                                                                  action:nil];

        
        self.tlbScannerTool = [[UIToolbar alloc] initWithFrame:CGRectMake(tlbScannerToolX, tlbScannerToolY, tlbScannerToolW, tlbScannerToolH)];
        self.tlbScannerTool.items = [NSArray arrayWithObjects:self.bbiCancel, flexible, self.bbiScan, nil];
        
        /* Add UI component into the view */
        [self.view addSubview:self.imvScanning];
        [self.view addSubview:self.tlbScannerTool];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - QRCodeReader Delegate
- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    @try {
        [reader stopScanning];
        [self dismissViewControllerAnimated:YES completion:^{

            if (result && ![result isEqualToString:BLANK]) {
                
                NSError *error;
                NSData *objectData = [result dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:objectData
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&error];
                
                if (resultDic && [resultDic objectForKey:@"clientID"]) {
                    
                    NSString *subsysID = [resultDic objectForKey:@"clientID"];
                    [self.viewModel subscribeSubsysWithSubsysID:subsysID];
                    
                } else {
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Subscription failed"
                                                                                   message:@"Invalid QR Code"
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:okButton];
                    [[Common getTopController] presentViewController:alert animated:YES completion:nil];
                }
            }
        }];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    @try {
        [self performSegueWithIdentifier:SEGUE_HOME sender:self];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end

@implementation ScanSubsysVC (ScanSubsysVCEX)

- (IBAction)performCancel:(id)sender {
    
    @try {
        [self performSegueWithIdentifier:SEGUE_HOME sender:self];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (IBAction)performScan:(id)sender {
    
    @try {
        [self displayScanner];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

-(void)scanningTapping:(UIGestureRecognizer *)recognizer {
    
    @try {
        [self displayScanner];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)displayScanner {
    
    @try {
        if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
            
            static QRCodeReaderViewController *readerVC = nil;
            static dispatch_once_t onceToken;
            
            dispatch_once(&onceToken, ^{
                QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
                readerVC = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel"
                                                                        codeReader:reader
                                                               startScanningAtLoad:YES
                                                            showSwitchCameraButton:NO
                                                                   showTorchButton:YES];
                readerVC.modalPresentationStyle = UIModalPresentationFormSheet;
            });
            
            readerVC.delegate = self;
            
            [self addChildViewController:readerVC];
            [self.view addSubview:readerVC.view];
            
        } else {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                           message:@"Reader not supported by the current device"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
