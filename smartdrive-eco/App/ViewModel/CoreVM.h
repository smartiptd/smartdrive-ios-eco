//
//  CoreVM.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/8/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JWT/JWT.h>
#import <JWT/JWTAlgorithmFactory.h>
#import "MQTTClient.h"
#import "../DataModel/AppSession.h"
#import "../DataModel/ActiveAirflowData.h"
#import "../Common/SessionManager.h"
#import "../Common/CommunicationManager.h"
#import "../Common/Common-Util.h"

@interface CoreVM : NSObject <SessionManagerDelegate, CommunicationManagerDelegate>

@property (nonatomic) NetworkStatus networkStatus;
@property (nonatomic) RequestStatus requestStatus;
@property (nonatomic, strong) AppSession *session;
@property (nonatomic, strong) MQTTSession *conn;
@property (nonatomic, strong) SessionManager *sessionMgr;
@property (nonatomic, strong) CommunicationManager *communicationMgr;
@property (nonatomic, strong) NSArray *subscriptionList;
@property (nonatomic, strong) NSMutableDictionary *subscriptionDataDic;

@property (nonatomic, strong) NSString *selectedSubsysID;

+ (instancetype)sharedInstance;
+ (void)resetSharedInstance;
- (void)requestSessionWithEmail:(NSString *)email Password:(NSString *)password;
- (void)requestSessionWithGoogleEmail:(NSString *)email TokenID:(NSString *)tokenID;
- (void)requestSubscriptionWithSubsysID:(NSString *)subsysID;
- (void)requestSubscriptionInfo;
- (void)destroySession;

@end
