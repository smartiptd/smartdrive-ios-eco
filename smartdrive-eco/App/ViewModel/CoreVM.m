//
//  CoreVM.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/8/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "CoreVM.h"

@implementation CoreVM

static CoreVM *instance = nil;

+ (instancetype)sharedInstance {
    
    @try {
        @synchronized (self) {
            if (instance == nil) {
                instance = [[CoreVM alloc] init];
            }
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return instance;
    }
}

+ (void)resetSharedInstance {
    
    @try {
        @synchronized(self) {
            instance = nil;
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (instancetype)init {
    
    @try {
        self = [super init];
        if (self) {
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
    
    @try {
        self.selectedSubsysID = BLANK;
        
        self.conn = nil;
        self.session = nil;
        
        self.subscriptionDataDic = [[NSMutableDictionary alloc] init];
        
        self.communicationMgr = [[CommunicationManager alloc] init];
        [self.communicationMgr setDelegate:self];

        self.sessionMgr = [[SessionManager alloc] init];
        [self.sessionMgr setDelegate:self];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)requestSessionWithEmail:(NSString *)email
                       Password:(NSString *)password {
    
    @try {
        [self.sessionMgr requestSessionWithEmail:email Password:password];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)requestSessionWithGoogleEmail:(NSString *)email
                              TokenID:(NSString *)tokenID {
    
    @try {
        [self.sessionMgr requestSessionWithGoogleEmail:email TokenID:tokenID];

    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)requestSubscriptionWithSubsysID:(NSString *)subsysID {
    
    @try {
        [self.sessionMgr requestSubscriptionWithSubsysID:subsysID];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)requestSubscriptionInfo {
    
    @try {
        [self.sessionMgr requestSubscriptionInfo];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)destroySession {
    
    @try {
        [self.sessionMgr destroySession];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - Session Manager Delegate
- (void)sessionDidUpdate:(AppSession *)session {
    
    @try {
        self.session = session;
        if (self.session) {
            
            /* Estiblish connection */
            NSString *token = [self.session valueForKey:@"accessToken"];
            if (token) {
                
                NSDictionary *decodeDic = [JWTBuilder decodeMessage:token].secret(OAUTH2_CLIENT_SECRET).algorithmName(@"HS256").decode;
                if (decodeDic) {
                    
                    NSDictionary *payloadDic = [decodeDic objectForKey:@"payload"];
                    if (payloadDic) {
                     
                        NSInteger challengeNum = [[payloadDic objectForKey:@"challengeNum"] integerValue];
                        if (challengeNum != 0) {
                            
                            NSInteger challengeCode = challengeNum * [Common randomNumberBetween:1000 maxNumber:9999];
                            
                            NSString *userID = [self.session valueForKey:@"userID"];
                            NSString *username = token;
                            NSString *password = [@(challengeCode) stringValue];
                            
                            /* Create communication channel */
                            [self.communicationMgr createMqttConnectionWithClientID:userID
                                                                           Username:username
                                                                           Password:password];
                        }
                    }
                }
            }
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)subscriptionDidUpdate:(NSArray *)subscriptionList {
    
    @try {
        if (self.conn && ![self.subscriptionList isEqual:subscriptionList]) {
            
            self.subscriptionList = subscriptionList;
            if (self.subscriptionList) {
                
                for (NSDictionary *subscriptionDic in self.subscriptionList) {
                    if (subscriptionDic) {
                        
                        NSString *subsysID = [subscriptionDic valueForKey:@"subsysID"];
                        NSString *subsysType = [subscriptionDic valueForKey:@"subsysType"];
                        if ([subsysType isEqualToString:SUBSYS_AAF]) {
                            
                            [self.communicationMgr subscribeToTopic:[NSString stringWithFormat:@"%@/status/#", subsysID]];
                        }
                    }
                }
            }
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)networkStatusDidUpdate:(NetworkStatus)status {
    
    @try {
        self.networkStatus = status;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)requestStatusDidUpdate:(RequestStatus)status {
    
    @try {
        self.requestStatus = status;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - Communication Manager Delegate 
- (void)connectionDidUpdate:(MQTTSession *)conn {
    
    @try {
        self.conn = conn;
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)connectionDidReceiveMessage:(NSDictionary *)messageDic
                           SubsysID:(NSString *)subsysID
                          Operation:(NSString *)operation
                               Path:(NSString *)path {
    
    @try {
        if (self.subscriptionList && self.subscriptionDataDic) {
            NSLog(@"%@", messageDic);
            NSMutableDictionary *tmpSubscriptionDataDic = [[NSMutableDictionary alloc] init];
            for (NSDictionary *subscription in self.subscriptionList) {
                
                if (subscription) {
                    
                    NSString *subscriptionSubsysID = [subscription objectForKey:@"subsysID"];
                    NSString *subscriptionSubsysType = [subscription objectForKey:@"subsysType"];
                    if (subscriptionSubsysID && subscriptionSubsysType && [subscriptionSubsysID isEqualToString:subsysID]) {
                        
                        ActiveAirflowData *aafData = (ActiveAirflowData *)[self.subscriptionDataDic objectForKey:subsysID];
                        if (!aafData) aafData = [[ActiveAirflowData alloc] initWithSubsysID:subsysID];
                        
                        if ([subscriptionSubsysType isEqualToString:SUBSYS_AAF]) {
                            
                            if ([operation isEqualToString:@"status"]) {
                             
                                if ([path isEqualToString:@"/heartbeat"]) {
                                    
                                    /* Connection status data */
                                    
                                } else if ([path isEqualToString:@"/system"]) {
                                    
                                    /* System status data */
                                    if ([messageDic objectForKey:@"firmware_version"])
                                        aafData.firmwareVersion = [messageDic objectForKey:@"firmware_version"];
                                    if ([messageDic objectForKey:@"system_clock"])
                                        aafData.systemClock = [messageDic objectForKey:@"system_clock"];
                                    if ([messageDic objectForKey:@"manual_switch"])
                                        aafData.powerStatus = [[messageDic objectForKey:@"manual_switch"] integerValue];
                                    if ([messageDic objectForKey:@"system_mode"])
                                        aafData.systemMode = [[messageDic objectForKey:@"system_mode"] integerValue];
                                    if ([messageDic objectForKey:@"source_info"])
                                        aafData.powerSource = [[messageDic objectForKey:@"source_info"] integerValue];
                                    
                                } else if ([path isEqualToString:@"/temperature"]) {
                                    
                                    /* Temperature data */
                                    for (NSString *name in messageDic) {
                                        
                                        id temp = [messageDic objectForKey:name];
                                        if (temp && [temp isKindOfClass:[NSNumber class]]) {
                                            [aafData.temperatureDic setObject:temp forKey:name];
                                        }
                                        if (temp && [temp isKindOfClass:[NSString class]]) {
                                            NSNumber *tempNum = [NSNumber numberWithLong:[temp integerValue]];
                                            [aafData.temperatureDic setObject:tempNum forKey:name];
                                        }
                                    }
                                    
                                } else if ([path isEqualToString:@"/solarcell"]) {
                                    
                                    /* Solar cell data */
                                    if ([messageDic objectForKey:@"level"])
                                        aafData.solarLevel = [[messageDic objectForKey:@"level"] integerValue];
                                    
                                } else if ([path isEqualToString:@"/sensor"]) {
                                    
                                    /* Sensor data */
                                    for (NSString *name in messageDic) {
                                        
                                        id status = [messageDic objectForKey:name];
                                        if (status && [status isKindOfClass:[NSNumber class]]) {
                                            [aafData.sensorStatusDic setObject:status forKey:name];
                                        }
                                    }
                                    
                                } else if ([path isEqualToString:@"/ventilator/srtv"]) {
                                    
                                    /* SRTV data */
                                    for (NSString *name in messageDic) {
                                        
                                        id status = [messageDic objectForKey:name];
                                        if (status && [status isKindOfClass:[NSNumber class]]) {
                                            [aafData.srtvStatusDic setObject:status forKey:name];
                                        }
                                    }
                
                                } else if ([path isEqualToString:@"/ventilator/cv"]) {
                                    
                                    /* CV data */
                                    for (NSString *name in messageDic) {
                                        
                                        id status = [messageDic objectForKey:name];
                                        if (status && [status isKindOfClass:[NSNumber class]]) {
                                            [aafData.cvStatusDic setObject:status forKey:name];
                                        }
                                    }
                                }
                            }
                        }
                        
                        [tmpSubscriptionDataDic setObject:aafData forKey:subsysID];
                    }
                }
            }
            
            self.subscriptionDataDic = tmpSubscriptionDataDic;
        }
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
