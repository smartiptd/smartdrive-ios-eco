//
//  ActiveAirflowMenuMainControlCellVM.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 8/3/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "../CoreVM.h"

@interface ActiveAirflowMenuMainControlCellVM : NSObject

@property (nonatomic, strong)  CoreVM *coreVM;

@end
