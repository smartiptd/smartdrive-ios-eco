//
//  ActiveAirflowMenuMainControlCellVM.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 8/3/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ActiveAirflowMenuMainControlCellVM.h"

@implementation ActiveAirflowMenuMainControlCellVM

- (instancetype)init {
    
    @try {
        self = [super init];
        if (self) {
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
    
    @try {
        self.coreVM = [CoreVM sharedInstance];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
