//
//  ActiveAirflowVM.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/27/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "../CoreVM.h"

@interface ActiveAirflowVM : NSObject

@property (nonatomic, strong)  CoreVM *coreVM;

@end
