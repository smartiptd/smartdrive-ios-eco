//
//  ActiveAirflowVM.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/27/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ActiveAirflowVM.h"

@implementation ActiveAirflowVM

- (instancetype)init {
    
    @try {
        self = [super init];
        if (self) {
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
    
    @try {
        self.coreVM = [CoreVM sharedInstance];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
