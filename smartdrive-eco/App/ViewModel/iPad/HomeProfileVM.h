//
//  HomeProfileVM.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/18/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "../CoreVM.h"

@interface HomeProfileVM : NSObject

@property (nonatomic, strong)  CoreVM *coreVM;

- (void)logout;

@end
