//
//  HomeProfileVM.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/18/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "HomeProfileVM.h"

@implementation HomeProfileVM

- (instancetype)init {
    
    @try {
        self = [super init];
        if (self) {
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
    
    @try {
        self.coreVM = [CoreVM sharedInstance];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)logout {
    
    @try {
        [self.coreVM destroySession];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
