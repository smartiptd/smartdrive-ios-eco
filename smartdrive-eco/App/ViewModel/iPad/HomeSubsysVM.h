//
//  HomeSubsysVM.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/26/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "../CoreVM.h"
#import "../../Common/Common-Util.h"

@interface HomeSubsysVM : NSObject

@property (nonatomic, strong)  CoreVM *coreVM;

- (NSArray *)getOldSubscriptionList;
- (NSArray *)updateSubscriptionWithSubscriptionList:(NSArray *)subscriptionList;

@end
