//
//  HomeSubsysVM.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/26/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "HomeSubsysVM.h"

@implementation HomeSubsysVM

- (instancetype)init {
    
    @try {
        self = [super init];
        if (self) {
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
    
    @try {
        self.coreVM = [CoreVM sharedInstance];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (NSArray *)getOldSubscriptionList {
    
    NSArray *subscriptionList;
    
    @try {
        subscriptionList = [self readSubscriptionFromDatabase];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return subscriptionList;
    }
}

- (NSArray *)updateSubscriptionWithSubscriptionList:(NSArray *)subscriptionList {
    
    NSArray *newSubscriptionList;
    
    @try {
        NSManagedObjectContext *context = [self managedObjectContext];
        
        NSMutableArray *currentSubsysList = [[NSMutableArray alloc] init];
        if (subscriptionList && [subscriptionList count] > 0) {
            
            /* Update & Save subscription */
            for (NSDictionary *subscription in subscriptionList) {
                
                if (subscription && [[subscription objectForKey:@"grantFlag"] isEqual:[NSNumber numberWithBool:YES]]) {
                    
                    [currentSubsysList addObject:[subscription objectForKey:@"subsysID"]];
                    
                    NSFetchRequest *fetchReq = [[NSFetchRequest alloc] initWithEntityName:ENTITY_SUBSCRIPTION];
                    [fetchReq setPredicate:[NSPredicate predicateWithFormat:@"subsysID == %@", [subscription objectForKey:@"subsysID"]]];
                    NSManagedObject *_subscription = [[context executeFetchRequest:fetchReq error:nil] firstObject];
                    if (_subscription) {
                        
                        [_subscription setValue:[subscription objectForKey:@"subscriptionID"] forKey:@"subscriptionID"];
                        [_subscription setValue:[subscription objectForKey:@"subsysID"] forKey:@"subsysID"];
                        [_subscription setValue:[subscription objectForKey:@"subscribeUser"] forKey:@"subscribeUser"];
                        [_subscription setValue:[subscription objectForKey:@"grantUser"] forKey:@"grantUser"];
                        [_subscription setValue:[subscription objectForKey:@"subsysType"] forKey:@"subsysType"];
                        [_subscription setValue:[subscription objectForKey:@"grantFlag"] forKey:@"grantFlag"];
                        
                    } else {
                        
                        NSManagedObject *newSubscription = [NSEntityDescription insertNewObjectForEntityForName:ENTITY_SUBSCRIPTION
                                                                                         inManagedObjectContext:context];
                        [newSubscription setValue:[subscription objectForKey:@"subscriptionID"] forKey:@"subscriptionID"];
                        [newSubscription setValue:[subscription objectForKey:@"subsysID"] forKey:@"subsysID"];
                        [newSubscription setValue:[subscription objectForKey:@"subscribeUser"] forKey:@"subscribeUser"];
                        [newSubscription setValue:[subscription objectForKey:@"grantUser"] forKey:@"grantUser"];
                        [newSubscription setValue:[subscription objectForKey:@"subsysType"] forKey:@"subsysType"];
                        [newSubscription setValue:BLANK forKey:@"refName"];
                        [newSubscription setValue:[subscription objectForKey:@"grantFlag"] forKey:@"grantFlag"];
                    }
                    
                    [context save:nil];
                }
            }
        }
        
        /* Delete subscription */
        if (currentSubsysList && [currentSubsysList count] > 0) {
            
            NSFetchRequest *fetchReq = [[NSFetchRequest alloc] initWithEntityName:ENTITY_SUBSCRIPTION];
            [fetchReq setPredicate:[NSPredicate predicateWithFormat:@"NOT (subsysID IN %@)", [currentSubsysList mutableCopy]]];
            NSArray *delObjList = [context executeFetchRequest:fetchReq error:nil];
            if (delObjList && [delObjList count] > 0) {
                
                for (NSManagedObject *obj in delObjList) {
                    [context deleteObject:obj];
                }
            }
        } else {
            
            NSFetchRequest *fetchReq = [[NSFetchRequest alloc] initWithEntityName:ENTITY_SUBSCRIPTION];
            NSArray *delObjList = [context executeFetchRequest:fetchReq error:nil];
            if (delObjList && [delObjList count] > 0) {
                
                for (NSManagedObject *obj in delObjList) {
                    [context deleteObject:obj];
                }
            }
        }
        
        newSubscriptionList = [self readSubscriptionFromDatabase];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return newSubscriptionList;
    }
}

#pragma mark - Private Methods
- (NSManagedObjectContext *)managedObjectContext {
    
    NSManagedObjectContext *context = nil;
    
    @try {
        id delegate = [[UIApplication sharedApplication] delegate];
        if ([delegate performSelector:@selector(managedObjectContext)]) {
            context = [delegate managedObjectContext];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return context;
    }
}

- (NSArray *)readSubscriptionFromDatabase {
    
    NSArray *subscriptionList;
    
    @try {
        NSManagedObjectContext *context = [self managedObjectContext];
        NSFetchRequest *fetchReq = [[NSFetchRequest alloc] initWithEntityName:ENTITY_SUBSCRIPTION];
        subscriptionList = [context executeFetchRequest:fetchReq error:nil];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return subscriptionList;
    }
}

@end
