//
//  HomeVM.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/10/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "../CoreVM.h"

@interface HomeVM : NSObject

@property (nonatomic, strong) CoreVM *coreVM;

- (void)requestSubscriptionInfo;

@end
