//
//  LocalLoginVM.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/8/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "../CoreVM.h"
#import "../../Common/Common-Util.h"

@interface LocalLoginVM : NSObject

@property (nonatomic, strong) CoreVM *coreVM;

- (BOOL)isValidEmail:(NSString *)email
            Password:(NSString *)password;
- (void)localLoginWithEmail:(NSString *)email
                   Password:(NSString *)password;

@end
