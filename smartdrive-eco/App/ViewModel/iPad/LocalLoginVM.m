//
//  LocalLoginVM.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/8/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "LocalLoginVM.h"

@implementation LocalLoginVM

- (instancetype)init {
    
    @try {
        self = [super init];
        if (self) {
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
 
    @try {
        self.coreVM = [CoreVM sharedInstance];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (BOOL)isValidEmail:(NSString *)email
            Password:(NSString *)password {
    
    BOOL isValid = NO;
    
    @try {
        isValid = [self isValidEmail:email] && [self isValidPassword:password];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return isValid;
    }
}

- (void)localLoginWithEmail:(NSString *)email
                   Password:(NSString *)password {
    
    @try {
        [self.coreVM requestSessionWithEmail:email Password:password];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - Private Method
- (BOOL)isValidEmail:(NSString *)email {
    
    BOOL isValid = NO;
    
    @try {
        if (![email isEqualToString:BLANK]) {
            NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
            NSPredicate *tester = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
            isValid = [tester evaluateWithObject:email];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return isValid;
    }
}

- (BOOL)isValidPassword:(NSString *)password {
    
    BOOL isValid = NO;
    
    @try {
        if (![password isEqualToString:BLANK] && [password length] >= 6) {
            isValid = YES;
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return isValid;
    }
}

@end
