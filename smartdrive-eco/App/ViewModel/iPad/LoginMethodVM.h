//
//  LoginMethodVM.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/11/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import <Google/SignIn.h>
#import "../CoreVM.h"

@interface LoginMethodVM : NSObject <GIDSignInDelegate>

@property (nonatomic, strong) CoreVM *coreVM;

- (void)googleLogin;
- (void)googleLogout;

@end
