//
//  LoginMethodVM.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/11/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "LoginMethodVM.h"

@implementation LoginMethodVM

- (instancetype)init {
    
    @try {
        self = [super init];
        if (self) {
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
    
    @try {
        self.coreVM = [CoreVM sharedInstance] ;
        
        /* Init Google Signin */
        NSError *configErr;
        [[GGLContext sharedInstance] configureWithError:&configErr];
        [GIDSignIn sharedInstance].delegate = self;
        
        [self googleLogout];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)googleLogin {
    
    @try {
        [[GIDSignIn sharedInstance] signIn];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)googleLogout {
    
    @try {
        [[GIDSignIn sharedInstance] signOut];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

#pragma mark - Google Signin Delegate
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    
    @try {
        NSString *email = user.profile.email;
        NSString *tokenID = user.authentication.idToken;
        if (tokenID) {
            [self.coreVM requestSessionWithGoogleEmail:email TokenID:tokenID];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
