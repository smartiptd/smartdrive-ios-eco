//
//  ScanSubsysVM.h
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/25/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "../CoreVM.h"

@interface ScanSubsysVM : NSObject

@property (nonatomic, strong) CoreVM *coreVM;

- (void)subscribeSubsysWithSubsysID:(NSString *)subsysID;

@end
