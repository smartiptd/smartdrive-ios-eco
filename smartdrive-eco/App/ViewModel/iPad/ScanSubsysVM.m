//
//  ScanSubsysVM.m
//  smartdrive-eco
//
//  Created by Sukrit  Chuen-im on 7/25/2559 BE.
//  Copyright © 2559 SCG. All rights reserved.
//

#import "ScanSubsysVM.h"

@implementation ScanSubsysVM

- (instancetype)init {
    
    @try {
        self = [super init];
        if (self) {
            [self initialize];
        }
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return self;
    }
}

- (void)initialize {
    
    @try {
        self.coreVM = [CoreVM sharedInstance];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

- (void)subscribeSubsysWithSubsysID:(NSString *)subsysID {
    
    @try {
        [self.coreVM requestSubscriptionWithSubsysID:subsysID];
        
    } @catch (NSException *exception) {
        @throw exception;
    } @finally {
        return;
    }
}

@end
